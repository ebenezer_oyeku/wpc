<?php
/**
 * Created by PhpStorm.
 * User: Unstinted
 * Date: 7/7/2015
 * Time: 4:44 PM
 */

Validator::extend('alpha_spaces', function($attribute, $value)
{
    return preg_match('/^[\pL\s]+$/u', $value);
});