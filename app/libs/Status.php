<?php

class Status {

    public static function display($status) {
        if ($status == 0) {
            echo "Confirmed";
        }else if ($status == 1) {
            echo "Started";
        }else if ($status == 2) {
            echo "InProgress";
        }else if($status == 3) {
            echo "Completed";
        }
    }

    public static function displayClass($status) {
        if ($status == 0) {
            echo "confirmed";
        }else if ($status == 1) {
            echo "started";
        }else if ($status == 2) {
            echo "inprogress";
        }else if($status == 3) {
            echo "completed";
        }
    }
}