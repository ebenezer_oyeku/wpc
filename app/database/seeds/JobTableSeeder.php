<?php

class JobTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('jobs')->delete();

        $jobs = array(
            array('name' => 'Administrator'),
            array('name' => 'Tutors'),
            array('name' => 'Cleaner'),
            array('name' => 'Driver'),
        );

        DB::table('jobs')->insert($jobs);
    }
}