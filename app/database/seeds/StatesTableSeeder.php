<?php

class StatesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('states')->delete();

        $states = array(
            array('name' => 'Abuja'),
            array('name' => 'Anambra'),
            array('name' => 'Enugu'),
            array('name' => 'Akwa Ibom'),
            array('name' => 'Adamawa'),
            array('name' => 'Abia'),
            array('name' => 'Bauchi'),
            array('name' => 'Bayelsa'),
            array('name' => 'Benue'),
            array('name' => 'Borno'),
            array('name' => 'Cross River'),
            array('name' => 'Delta'),
            array('name' => 'Ebonyi'),
            array('name' => 'Edo'),
            array('name' => 'Ekiti'),
            array('name' => 'Gombe'),
            array('name' => 'Imo'),
            array('name' => 'Jigawa'),
            array('name' => 'Kaduna'),
            array('name' => 'Kano'),
            array('name' => 'Katsina'),
            array('name' => 'Kebbi'),
            array('name' => 'Kogi'),
            array('name' => 'Kwara'),
            array('name' => 'Lagos'),
            array('name' => 'Nasarawa'),
            array('name' => 'Niger'),
            array('name' => 'Ogun'),
            array('name' => 'Ondo'),
            array('name' => 'Osun'),
            array('name' => 'Oyo'),
            array('name' => 'Plateau'),
            array('name' => 'Rivers'),
            array('name' => 'Sokoto'),
            array('name' => 'Taraba'),
            array('name' => 'Yobe'),
            array('name' => 'Zamfara'),
        );

		DB::table('states')->insert($states);
        }

}