<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Facebook
		 */
        'Facebook' => array(
            'client_id'     => '667517183362292',
            'client_secret' => '50c850aa035377fcdc83e2c40df35f8e',
            'scope'         => array('email','read_friendlists','user_online_presence'),
        ),

		'Google' => array(
			'client_id'     => '430932252209-3gpagrm0jbfscb2o3v65a8jtnj4rj47b.apps.googleusercontent.com',
			'client_secret' => 'XvaSjt4zztAaHqpDaUS8se3Q',
			'scope'         => array('userinfo_email', 'userinfo_profile'),
		),

		'Twitter' => array(
			'client_id'     => '1DREP4HMlurJiQOB89T82Z6Uj',
			'client_secret' => 'NdUgiAOk1d37xAL1sYQfsCP5og8reefvBnU2U8aiwP8EPXpaIM',
			// No scope - oauth1 doesn't need scope
		),

	)

);