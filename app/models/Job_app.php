<?php


Class Job_app extends Eloquent
{
    protected $fillable = array('img_up', 'job_list', 'title',
        'firstname', 'middlename','lastname', 'gender', 'dob',
        'phone_no', 'phone_no2', 'contact_add', 'state_lists', 'e_level',
        'i_name', 'c_study', 'email', 'cv_up');
    public function job()
    {
        return $this->belongsTo('Job');
    }

    public function state()
    {
        return $this->belongsTo('State');
    }

    public static $rules = array(
        'img_up'=>'required|image|mimes:jpeg,png,jpg,bmp,gif',
        'job_list'=>'required',
        'title'=>'required|alpha',
        'firstname'=>'required|alpha',
        'middlename'=>'required|alpha',
        'lastname'=>'required|alpha',
        'gender'=>'required',
        'dob'=>'required|date',
        'phone_no'=>'required|numeric',
        'phone_no2'=>'required|numeric',
        'contact_add'=>'required|alpha_spaces|min:2',
        'state_lists'=>'required',
        'e_level'=>'required|alpha_spaces|min:2',
        'i_name'=>'required|alpha_spaces|min:2',
        'c_study'=>'required|alpha_spaces|min:2',
        'email'=>'required|email|unique:job_app',
        'cv_up'=>'required|mimes:pdf,application/zip','application/vnd.openxmlfo‌​rmats-officedocument.spreadsheetml.sheet','application/vnd.ms-office','applicatio‌​n/vnd.ms-excel|max:1024',
    );

    protected $table = 'job_app';
}
