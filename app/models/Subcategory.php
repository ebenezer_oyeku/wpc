<?php

class Subcategory extends \Eloquent {
    protected $table = 'subcategories';
    protected $guarded = array('id');
	protected $fillable = ['name', 'cat_id'];

    public static $rules = ['name' => 'required|unique:subcategories|min:3',
    'cat_id' => 'integer'];

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function projects(){
        return $this->hasMany('Project');
    }
}