<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	protected $fillable = array('firstname', 'lastname', 'image', 'Username', 'password', 'email',
			'Address', 'phone','dob', 'Administrator');

	public static $rules = array(
			'firstname' => 'required|min:2|alpha',
			'lastname' => 'required|min:2|alpha',
			'image' => 'required|image|mimes:jpeg,png,jpg,bmp,gif',
			'Username' => 'required|alpha_spaces|alpha_dash|unique:users',
			'password' => 'required|alpha_num|between:8,12|confirmed',
			'password_confirmation'=>'required|alpha_num|between:8,12',
			'email' => 'required|email|unique:users',
			'Address' => 'required|min:20',
			'phone' => 'required|numeric',
			'dob' => 'required|date',
			'Administrator' => 'integer',
	);

	use UserTrait, RemindableTrait;

	public function projects()
	{
		return $this->hasMany('Project');
	}

	protected $table = 'users';

	protected $hidden = array('password', 'remember_token');
}
