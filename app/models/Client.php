<?php

class Client extends \Eloquent {
	protected $fillable = array('name', 'weblink','image', 'address', 'contact1', 'contact2',
	'email', 'twitterid', 'facebookid', 'skypeid', 'city', 'state', 'country');

	public function projects()
	{
		return $this->hasMany('Project');
	}

	public static $rules = array(
		'name'=>'required|alpha_spaces',
		'weblink'=>'min:3',
		'image'=>'required|image|mimes:jpeg,png,jpg,bmp,gif',
		'address'=>'required|min:3',
		'contact1'=>'required|numeric',
		'contact2'=>'numeric',
		'email'=>'email|unique:client',
		'twitterid'=>'min:3|unique:client',
		'facebookid'=>'min:3|unique:client',
		'skypeid'=>'min:3|unique:client',
		'city'=>'required|alpha_spaces',
		'state'=>'required|alpha_spaces',
		'country'=>'required',

	);

	protected $table = 'client';
}