<?php

class Category extends \Eloquent {
	protected $table = 'categories';
	public $timestamps = false;
	protected $guarded = array('id');
	protected $fillable = array('name');

	public static $rules = ['name' => 'required|min:3'];


	public function projects(){
		return $this->hasMany('Project');
	}


}