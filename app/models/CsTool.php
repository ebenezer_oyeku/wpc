<?php

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class CsTool extends \Eloquent implements SluggableInterface {
	protected $fillable = ['name', 'weblink', 'slug','tool_function', 'details', 'image'];

	use SluggableTrait;

	protected $sluggable = [
			'build_from' => 'name',
			'save_to'    => 'slug',
	];

	public static $rules = array(
			'name'=>'required|alpha_spaces',
			'slug'=>'alpha-spaces',
			'weblink'=>'required|min:1',
			'image'=>'image|mimes:jpeg,png,jpg,bmp,gif',
			'details'=>'required|min:10',
			'tool_function'=>'required|min:3'
	);

	protected $table = 'cs_tools';
}