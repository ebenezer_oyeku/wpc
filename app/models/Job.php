<?php

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
    class Job extends Eloquent implements SluggableInterface{
        protected $fillable = array('name', 'slug', 'summary', 'openings', 'image','description', 'end_date');

        use SluggableTrait;

        protected $sluggable = [
            'build_from' => 'name',
            'save_to'    => 'slug',
        ];

        public static $rules = array('name'=>'required|min:3',
            'name'=>'required|alpha_spaces',
            'slug'=>'min:1',
            'summary'=>'required|min:1',
            'openings'=>'required|integer',
            'image'=>'image|mimes:jpeg,png,jpg,bmp,gif',
            'description'=>'required|min:10',
            'end_date'=>'required|date'
            );

        public function jobs_app() {
            return $this->hasMany('Job_app');
        }


    }

?>