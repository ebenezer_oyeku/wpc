<?php
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Project extends \Eloquent implements SluggableInterface{
	protected $fillable = array('image','pname', 'purl', 'details', 'slug', 'ExpStartDate', 'ExpEndDate',
		'ActStartDate', 'ActEndDate', 'projState', 'cat_id', 'subcat_id', 'client_id', 'AdministratorID' );

    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'pname',
        'save_to'    => 'slug',
    ];


    public static $rules = array(
		'pname'=>'required|alpha_spaces',
		'purl'=>'min:3',
		'image'=>'image|mimes:jpeg,png,jpg,bmp,gif',
		'details'=>'required|min:3',
        'slug'=>'alpha-spaces',
		'ExpStartDate'=>'required|date',
		'ExpEndDate'=>'required|date',
		'ActStartDate'=>'date',
		'ActEndDate'=>'date',
		'projState'=>'integer',
		'cat_id'=>'required|integer',
		'subcat_id'=>'required|integer',
		'client_id'=>'required|integer',
		'AdministratorID'=>'required|integer',
	);


	public function category(){
		return $this->belongsTo('Category', 'cat_id');
	}

	public function subcategory(){
		return $this->belongsTo('Subcategory', 'subcat_id');
	}
	public function client(){
		return $this->belongsTo('Client');
	}

	public function administrator()
	{
		return $this->belongsTo('User', 'AdministratorID');
	}

	protected $table = 'projects';
}