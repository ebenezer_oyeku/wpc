<?php

class ServicesController extends BaseController
{

    public function showServices()
    {
        return View::make('services')
        ->with('clients', Client::orderBy('created_at', 'DESC')->get());
    }
}