<?php

class ContactusController extends BaseController
{

    public function showContactus()
    {
        return View::make('contactus');
    }

    public function getContactUsForm(){

        //Get all the data and store it inside Store Variable
        $data = Input::all();

        //Validation rules
        $rules = array (
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha',
            'phone_number'=>'numeric|min:8',
            'email' => 'required|email',
            'message' => 'required|min:25'
        );

        //Validate data
        $validator = Validator::make ($data, $rules);

        //If everything is correct than run passes.
        if ($validator -> passes()){

            Mail::send('emails.feedback', $data,  function($message) use ($data)
            {
                $message->from($data['email'] , $data['first_name'], $data['last_name']);
                $message->to('info@webplanetcon.com', 'WPC')->cc('ebenezeroyeku@webplanetcon.com')->subject('Clients Feedback');

            });
            // Redirect to page
            return Redirect::to('contactus')->with('message', 'Your Message was Sent');
        }else{
//return contact form with errors
            return Redirect::to('contactus')->with('message', 'The following errors occurred')->withErrors($validator)
                ->withInput();
        }
    }
}