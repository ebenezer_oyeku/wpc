<?php

class AboutController extends BaseController
{

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on'=>'post'));
    }

    public function showAbout()
    {
        return View::make('about');
    }

}