<?php

class SignupController extends BaseController
{

    public function showSignup()
    {
        return View::make('account.signup');
    }
    public function __construct() {
        $this->beforeFilter('csrf', array('on'=>'post'));
    }

    public function postCreate() {
        $validator = Validator::make(Input::all(), User::$rules);

        $confirmation_code = str_random(30);

        if ($validator->passes()) {
            $user = new User;
            $user->f_name = Input::get('f_name');
            $user->l_name = Input::get('l_name');
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->confirmation_code = $confirmation_code;
            $user->save();
            $theEmail = Input::get('email');
            $theName = Input::get('f_name');

            Mail::send('emails.mailers',  array('user'=>$user, 'confirmation_code'=> $confirmation_code), function($message){
                $message->to(Input::get('email'), Input::get('f_name').' '.Input::get('l_name'))->subject('Welcome to Webplanet Consulting and Services!');
            });

            return View::make('account.thanks')->with('theEmail', $theEmail)->with('theName', $theName);
        } else {
            return Redirect::to('signup')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();
        }
    }

    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return Redirect::to('login')->with('message', 'You have successfully verified your account. You can Sign In');
    }

   /* public function doSignup()
    {
        $user = new User;
        $user->f_name = Input::get('f_name');
        $user->l_name = Input::get('l_name');
        $user->email = Input::get('email');
        $user->username = Input::get('username');
        $user->password = Hash::make(Input::get('password'));
        $user->save();
        $theEmail = Input::get('email');
        return View::make('thanks')->with('theEmail', $theEmail);
    }*/
}