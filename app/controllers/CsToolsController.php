<?php

class CsToolsController extends \BaseController {

	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('csrf', array('on'=>'post'));
	}



	public function index()
	{
		return View::make('admin/cstool.index')
				->with('cstools', CsTool::orderBy('created_at', 'DESC')->paginate(4));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /cstools/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /cstools
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), CsTool::$rules);

		if ($validator->passes()) {
			$cstool = new CsTool;
			$cstool->name =Input::get('name');
			$cstool->Weblink =Input::get('weblink');
			$image = Input::file('image');
			$filename = time()."-".$image->getClientOriginalName();
			Image::make($image->getRealPath())->resize(750,340)->save(public_path().'/images/cstool/'.$filename);
			$cstool->image = '/images/cstool/'.$filename;
			$cstool->details = Input::get('details');
			$cstool->tool_function = Input::get('tool_function');
			$cstool->save();

			return Redirect::to('admin/cstool')
					->with('message', 'Tool Details Created');
		}

		return Redirect::to('admin/cstool')
				->with('message', 'Something went wrong')
				->withErrors($validator)
				->withInput();
	}

	/**
	 * Display the specified resource.
	 * GET /cstools/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// get the nerd
		$cstool = CsTool::find($id);

		// show the view and pass the nerd to it
		return View::make('admin/cstool.show')
				->with('cstool', $cstool);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /cstools/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// get the client
		$cstool = CsTool::find($id);

		// show the edit form and pass the nerd
		return View::make('admin/cstool.edit')
				->with('cstool', $cstool);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /cstools/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$validator = Validator::make(Input::all(), Client::$rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('admin/cstool/' . $id . '/edit')
					->withErrors($validator)
					->withInput(Input::except('password'));
		} else {
			// store
			$client = Client::find($id);
			$client->name =Input::get('name');
			$image = Input::file('image');
			$filename = time()."-".$image->getClientOriginalName();
			Image::make($image->getRealPath())->resize(468,249)->save(public_path().'/images/clients/'.$filename);
			$client->image = '/images/clients/'.$filename;
			$client->address = Input::get('address');
			$client->contact1 = Input::get('contact1');
			$client->contact2 = Input::get('contact2');
			$client->email = Input::get('email');
			$client->twitterid = Input::get('twitterid');
			$client->facebookid = Input::get('facebookid');
			$client->skypeid = Input::get('skypeid');
			$client->city = Input::get('city');
			$client->state = Input::get('state');
			$client->country = Input::get('country');
			$client->save();

			// redirect
			Session::flash('message', 'Successfully updated CS Programming Tool!');
			return Redirect::to('admin/cstool');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /cstools/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		$cstool = CsTool::find($id);

		if ($cstool) {
			File::delete('public/images/cstool'.$cstool->image);
			$cstool->delete();
			return Redirect::to('admin/cstool')
					->with('message', 'Tools Detail Deleted');
		}

		return Redirect::to('admin/cstool')
				->with('message', 'Something went wrong, please try again');
	}

}