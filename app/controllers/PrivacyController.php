<?php

class PrivacyController extends BaseController
{

    public function showPrivacy()
    {
        return View::make('privacy');
    }
}