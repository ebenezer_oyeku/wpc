<?php

class CareersController extends BaseController
{

    public function index()
    {
        return View::make('careers')
            ->with('jobs', Job::orderBy('created_at', 'DESC')->get());
    }

    public function show($slug)
    {
        $job = Job::where('slug',$slug)->first();
        if ($job) {
            return View::make('jobview')->with('job', $job);
        }
    }
}