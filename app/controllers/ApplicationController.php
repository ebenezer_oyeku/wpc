<?php


class ApplicationController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->beforeFilter('csrf', array('on'=>'post'));
    }

    public function getIndex()
    {
        $job_lists = array();
        foreach (Job::all() as $job_list) {
            $job_lists[$job_list -> id] = $job_list -> name;
        }
        $state_lists = array();
        foreach (State::all() as $state_list) {
            $state_lists[$state_list -> id] = $state_list -> name;
        }

        return View::make('application')
            ->with('job_lists', $job_lists)
            ->with('state_lists', $state_lists);
    }


    public function postJobApp()
    {
        $validator = Validator::make(Input::all(), Job_app::$rules);

        if ($validator->passes()) {
            $job_app = new Job_app;
                $image = Input::file('img_up');
                $filename = time()."-".$image->getClientOriginalName();
                Image::make($image->getRealPath())->resize(468,249)->save(public_path().'/applicant_img/'.$filename);
            $job_app->img_up = '/applicant_img/'.$filename;
            $job_app->job_list = Input::get('job_list');
            $job_app->title = Input::get('title');
            $job_app->firstname = Input::get('firstname');
            $job_app->middlename = Input::get('middlename');
            $job_app->lastname = Input::get('lastname');
            $job_app->gender = Input::get('gender');
            $job_app->dob = Input::get('dob');
            $job_app->phone_no = Input::get('phone_no');
            $job_app->phone_no2 = Input::get('phone_no2');
            $job_app->contact_add = Input::get('contact_add');
            $job_app->state_lists = Input::get('state_lists');
            $job_app->e_level = Input::get('e-level');
            $job_app->i_name = Input::get('i_name');
            $job_app->c_study = Input::get('c_study');
            $job_app->email = Input::get('email');
            $resume = Input::file('cv_up');
            $filename = time()."-".$resume->getClientOriginalName();
            $resume->move(public_path().'/applicant_resume/'.$filename);
            $job_app->cv_up = '/applicant_resume/'.$filename;
            $job_app->save();

            Mail::send('emails.jobapp', array('job_app'=>$job_app), function($message){
                $message->to(Input::get('email'), Input::get('firstname').' '.Input::get('lastname'))->subject('Welcome to Webplanet Consulting and Services!');
            });
            return Redirect::to('/application')->with('message', 'Job Application has been Received');
        } else {
            return Redirect::to('/application')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();
        }


    }

}