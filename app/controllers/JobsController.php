<?php

class JobsController extends \BaseController {

    public function __construct()
    {
        parent::__construct();
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('Administrator');
    }

    /**
     * Display a listing of the resource.
     * GET /cstools
     *
     * @return Response
     */
    public function index()
    {
        return View::make('admin/jobs.index')
            ->with('jobs', Job::all());
    }

    /**
     * Show the form for creating a new resource.
     * GET /cstools/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /cstools
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make(Input::all(), Job::$rules);

        if ($validator->passes()) {
            $job = new Job;
            $job->name =Input::get('name');
            $job->openings =Input::get('openings');
            $job->summary =Input::get('summary');
            $image = Input::file('image');
            $filename = time()."-".$image->getClientOriginalName();
            Image::make($image->getRealPath())->resize(750,340)->save(public_path().'/images/jobs/'.$filename);
            $job->image = '/images/jobs/'.$filename;
            $job->description = Input::get('description');
            $job->end_date = Input::get('end_date');
            $job->save();

            return Redirect::to('admin/jobs')
                ->with('message', 'Tool Details Created');
        }

        return Redirect::to('admin/jobs')
            ->with('message', 'Something went wrong')
            ->withErrors($validator)
            ->withInput();
    }

    /**
     * Display the specified resource.
     * GET /cstools/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the client
        $job = Job::find($id);

        // show the edit form and pass the nerd
        return View::make('admin/jobs.show')
            ->with('job', $job);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /cstools/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the job
        $job = Job::find($id);

        // show the edit form and pass the job
        return View::make('admin/jobs.edit')
            ->with('job', $job);
    }

    /**
     * Update the specified resource in storage.
     * PUT /cstools/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = Validator::make(Input::all(), Client::$rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/jobs/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $job = Job::find($id);
            $job->name =Input::get('name');
            $job->openings =Input::get('openings');
            $job->summary =Input::get('summary');
            $image = Input::file('image');
            $filename = time()."-".$image->getClientOriginalName();
            Image::make($image->getRealPath())->resize(750,340)->save(public_path().'/images/jobs/'.$filename);
            $job->image = '/images/jobs/'.$filename;
            $job->description = Input::get('description');
            $job->end_date = Input::get('end_date');
            $job->save();

            // redirect
            Session::flash('message', 'Successfully updated Job Opening!');
            return Redirect::to('admin/jobs');
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /cstools/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $job = Job::find($id);

        if ($job) {
            File::delete('public/images/jobs'.$job->image);
            $job->delete();
            return Redirect::to('admin/jobs')
                ->with('message', 'Tools Detail Deleted');
        }

        return Redirect::to('admin/jobs')
            ->with('message', 'Something went wrong, please try again');
    }

}