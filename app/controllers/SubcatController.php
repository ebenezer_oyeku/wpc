<?php

class SubcatController extends \BaseController {

    public function __construct()
    {
        parent::__construct();
        $this->beforeFilter('csrf', array('on'=>'post'));
		$this->beforeFilter('Administrator');
    }
	/**
	 * Display a listing of the resource.
	 * GET /subcat
	 *
	 * @return Response
	 */
	public function index()
	{
        $categories = array();
        foreach(Category::all() as $category) {
            $categories[$category->id] = $category->name;
        }

        return View::make('admin/subcategories.index')
            ->with('subcategories', Subcategory::orderBy('created_at', 'DESC')->paginate(6))
            ->with('categories', $categories);
	}

	public function store()
	{
		$validator = Validator::make(Input::all(), Subcategory::$rules);

		if ($validator->passes()) {
			$subcategory = new Subcategory;
			$subcategory->name = Input::get('name');
			$subcategory->cat_id = Input::get('cat_id');
			$subcategory->save();

			return Redirect::to('admin/subcategories')
					->with('message', 'SubCategory Created');
		}

		return Redirect::to('admin/subcategories')
				->with('message', 'Something went wrong')
				->withErrors($validator)
				->withInput();
	}

	/**
	 * Display the specified resource.
	 * GET /subcat/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// get the nerd
		$subcategory = Subcategory::find($id);

		// show the view and pass the nerd to it
		return View::make('admin/subcategories.show')
				->with('subcategory', $subcategory);
	}


	public function edit($id)
	{
		// get the client
		$subcategory = Subcategory::find($id);
		$categories = Category::lists('name','id');

		// show the edit form and pass the nerd
		return View::make('admin/subcategories.edit')
				->with('subcategory', $subcategory)
				->with('categories', $categories);
	}

	public function update($id)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$validator = Validator::make(Input::all(), Client::$rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('admin/subcategories/' . $id . '/edit')
					->withErrors($validator)
					->withInput(Input::except('password'));
		} else {
			// store
			$subcategory = Category::find($id);
			$subcategory->name =Input::get('name');
			$subcategory->cat_id = Input::get('cat_id');
			$subcategory->save();

			// redirect
			Session::flash('message', 'Successfully updated nerd!');
			return Redirect::to('admin/subcategories');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /subcat/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $subcategory = Subcategory::find($id);

        if ($subcategory) {
            $subcategory->delete();
            return Redirect::to('admin/subcategories')
                ->with('message', 'Category Deleted');
        }

        return Redirect::to('admin/subcategories')
            ->with('message', 'Something went wrong, please try again');
	}

}