<?php

class UsersController extends BaseController {


	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('csrf', array('on'=>'post'));
	}

	public function getIndex()
	{
		return View::make('users.index')
				->with('users', User::orderBy('created_at', 'DESC')->paginate(8));
	}

	public function postCreate()
	{
		$validator = Validator::make(Input::all(), User::$rules);

		if ($validator->passes()) {
			$user = new User;
			$user->firstname =Input::get('firstname');
			$user->lastname =Input::get('lastname');
			$user->Username =Input::get('Username');
			$image = Input::file('image');
			$filename = time()."-".$image->getClientOriginalName();
			Image::make($image->getRealPath())->resize(468,249)->save(public_path().'/images/users/'.$filename);
			$user->image = '/images/users/'.$filename;
			$user->password = Hash::make(Input::get('password'));
			$user->email = Input::get('email');
			$user->Address = Input::get('Address');
			$user->phone = Input::get('phone');
			$user->dob = Input::get('dob');
			$user->Administrator = Input::get('Administrator');
			$user->save();

			return Redirect::to('admin/users.index')
					->with('message', 'Client Created');
		}

		return Redirect::to('admin/users.index')
				->with('message', 'Something went wrong')
				->withErrors($validator)
				->withInput();
	}


	public function postDestroy()
	{
		$user = User::find(Input::get('id'));

		if ($user) {
			File::delete('public/images/users'.$user->image);
			$user->delete();
			return Redirect::to('admin/users.index')
					->with('message', 'Client Deleted');
		}

		return Redirect::to('admin/users.index')
				->with('message', 'Something went wrong, please try again');
	}



	public function getTeamlogin(){
		return View::make('users.teamlogin');
	}

	public function postTeamlogin() {
		if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
			return Redirect::to('admin/dashboard')->with('message', 'Thanks for signing in');
		}

		return Redirect::to('admin/users/teamlogin')->with('message', 'Your email/password combo was incorrect');
	}

	public function getSignout() {
		Auth::logout();
		return Redirect::to('admin/users/teamlogin')->with('message', 'You have been successfully logged out');
	}
}
