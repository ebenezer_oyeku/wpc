<?php

class DashboardController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->beforeFilter('csrf', array('on'=>'post'));
    }

    public function index()
    {
        return View::make('admin/dashboard.profile');
    }

}