<?php

class ChildictController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->beforeFilter('csrf', array('on'=>'post'));
    }

    public function index()
    {
        return View::make('childict.childict')
        ->with('cstools', CsTool::orderBy('created_at', 'DESC')->get());
    }

    public function show($slug)
    {
        $cstool = CsTool::where('slug',$slug)->first();
        if ($cstool) {
            return View::make('childict.softview')->with('cstool', $cstool);
        }
    }
}