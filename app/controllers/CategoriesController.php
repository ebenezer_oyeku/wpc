<?php

class CategoriesController extends \BaseController {

	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->beforeFilter('Administrator');
	}

	public function index(){
		return View::make('admin/categories.index')
			->with('categories', Category::orderBy('created_at', 'DESC')->paginate(6));
	}

	public function store(){
		$validator = Validator::make(Input::all(), Category::$rules);

		if ($validator->passes()) {
			$category = new Category;
			$category->name = Input::get('name');
			$category->save();

			return Redirect::to('admin/categories')
				->with('message', 'Category Created');
		}

		return Redirect::to('admin/categories')
			->with('message', 'Something went wrong')
			->withErrors($validator)
			->withInput();
	}

	public function show($id)
	{
		// get the nerd
		$category = Category::find($id);

		// show the view and pass the nerd to it
		return View::make('admin/categories.show')
				->with('category', $category);
	}


	public function edit($id)
	{
		// get the client
		$category = Category::find($id);

		// show the edit form and pass the nerd
		return View::make('admin/categories.edit')
				->with('category', $category);
	}

	public function update($id)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$validator = Validator::make(Input::all(), Client::$rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('admin/categories/' . $id . '/edit')
					->withErrors($validator)
					->withInput(Input::except('password'));
		} else {
			// store
			$category = Category::find($id);
			$category->name =Input::get('name');
			$category->save();

			// redirect
			Session::flash('message', 'Successfully updated nerd!');
			return Redirect::to('admin/categories');
		}
	}

	public function destroy($id){

		$category = Category::find($id);

		if ($category) {
			$category->delete();
			return Redirect::to('admin/categories')
				->with('message', 'Category Deleted');
		}

		return Redirect::to('admin/categories')
			->with('message', 'Something went wrong, please try again');
	}
}