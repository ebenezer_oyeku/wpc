<?php

class StoreController extends \BaseController {

	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('csrf', array('on'=>'post'));
	}

	public function getProjects()
	{
		$categories = Category::all();
		return View::make('project.project', compact($categories))
			->with('projects', Project::orderBy('created_at', 'DESC')->paginate(6));
	}

   public function getView($slug) {
        $project = Project::where('slug',$slug)->first();
        if ($project) {
            return View::make('project.view')->with('project', $project);
        }
    }


	public function getCategory($category_id) {
		return View::make('project.category')
			->with('projects', Project::where('cat_id', '=', $category_id)->paginate(6))
			->with('category', Category::find($category_id));
	}

	public  function getSearch() {
		$keyword = Input::get('keyword');

		return View::make('project.search')
				->with('projects', Project::where('pname', 'LIKE', '%'.$keyword.'%')->get())
				->with('keyword', $keyword);
	}

}