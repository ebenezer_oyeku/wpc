<?php

class ProjectsController extends \BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('Administrator');
    }


    public function index(){

        $categories = array();
        foreach(Category::all() as $category) {
        $categories[$category->id] = $category->name;
        }

        $clients = array();
        foreach(Client::all() as $client) {
        $clients[$client->id] = $client->name;
        }

        $users = array();
        foreach(User::all() as $user) {
            $users[$user->id] = $user->lastname;
        }

        return View::make('admin/projects.index')
            ->with('projects', Project::orderBy('created_at', 'DESC')->paginate(4))
            ->with('categories', $categories)
            ->with('clients', $clients)
            ->with('employees', $users);
    }

    public function store(){
        $validator = Validator::make(Input::all(), Project::$rules);

        if ($validator->passes()) {
            $project = new Project;
            $project->pname = Input::get('pname');
            $project->purl = Input::get('purl');
            $project->details = Input::get('details');
            $image = Input::file('image');
            $filename = time()."-".$image->getClientOriginalName();
            Image::make($image->getRealPath())->resize(550,400)->save(public_path().'/images/projects/'.$filename);
            $project->image ='/images/projects/'.$filename;
            $project->ExpStartDate = Input::get('ExpStartDate');
            $project->ExpEndDate = Input::get('ExpEndDate');
            $project->ActStartDate = Input::get('ActStartDate');
            $project->ActEndDate = Input::get('ActEndDate');
            $project->cat_id = Input::get('cat_id');
            $project->subcat_id = Input::get('subcat_id');
            $project->client_id = Input::get('client_id');
            $project->AdministratorID = Input::get('AdministratorID');
            $project->save();

            return Redirect::to('admin/projects')
                ->with('message', 'Project Created');
        }

        return Redirect::to('admin/projects')
            ->with('message', 'Something went wrong, Check out the Form Field')
            ->withErrors($validator)
            ->withInput();
    }

    public function postToggleState(){
        $project = Project::find(Input::get('id'));

        if ($project) {
            $project->projState = Input::get('projState');
            $project->save();
            return Redirect::to('admin/projects')->with('message', 'Project Updated');
        }

        return Redirect::to('admin/projects')->with('message', 'Invalid Project');

    }

    public function show($id)
    {
        // get the client
        $project = Project::find($id);

        // show the edit form and pass the nerd
        return View::make('admin/projects.show')
            ->with('project', $project);
    }

    public function edit($id)
    {
        // get the client
        $project = Project::find($id);
        $subcategories = Subcategory::lists('name','id');
        $categories = Category::lists('name','id');
        $clients = Client::lists('name', 'id');
        $users = User::lists('lastname', 'id');

        // show the edit form and pass the nerd
        return View::make('admin/projects.edit')
            ->with('project', $project)
            ->with('categories', $categories)
            ->with('subcategories', $subcategories)
            ->with('clients', $clients)
            ->with('employees', $users);
    }

    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validator = Validator::make(Input::all(), Client::$rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/projects/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $project = Project::find($id);
            $project->pname = Input::get('pname');
            $project->purl = Input::get('purl');
            $project->details = Input::get('details');
            $image = Input::file('image');
            $filename = time()."-".$image->getClientOriginalName();
            Image::make($image->getRealPath())->resize(550,400)->save(public_path().'/images/projects/'.$filename);
            $project->image ='public/images/projects/'.$filename;
            $project->ExpStartDate = Input::get('ExpStartDate');
            $project->ExpEndDate = Input::get('ExpEndDate');
            $project->ActStartDate = Input::get('ActStartDate');
            $project->ActEndDate = Input::get('ActEndDate');
            $project->cat_id = Input::get('cat_id');
            $project->subcat_id = Input::get('subcat_id');
            $project->client_id = Input::get('client_id');
            $project->AdministratorID = Input::get('AdministratorID');
            $project->save();

            // redirect
            Session::flash('message', 'Successfully updated CS Programming Tool!');
            return Redirect::to('admin/projects');
        }
    }

    public function destroy($id){

        $project = Project::find($id);

        if ($project) {
            File::delete('public/images/projects/'.$project->img);
            $project->delete();
            return Redirect::to('admin/projects')
                ->with('message', 'Project Deleted');
        }

        return Redirect::to('admin/projects')
            ->with('message', 'Something went wrong, please try again');
    }



}