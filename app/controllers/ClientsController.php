<?php

class ClientsController extends \BaseController {

	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->beforeFilter('Administrator');
	}

	public function index(){
		return View::make('admin/clients.index')
			->with('clients', Client::all());
	}

	public function store(){
		$validator = Validator::make(Input::all(), Client::$rules);

		if ($validator->passes()) {
			$client = new Client;
			$client->name =Input::get('name');
			$client->weblink = Input::get('weblink');
			$image = Input::file('image');
			$filename = time()."-".$image->getClientOriginalName();
			Image::make($image->getRealPath())->resize(180,64)->save(public_path().'/images/clients/'.$filename);
			$client->image = '/images/clients/'.$filename;
			$client->address = Input::get('address');
			$client->contact1 = Input::get('contact1');
			$client->contact2 = Input::get('contact2');
			$client->email = Input::get('email');
			$client->twitterid = Input::get('twitterid');
			$client->facebookid = Input::get('facebookid');
			$client->skypeid = Input::get('skypeid');
			$client->city = Input::get('city');
			$client->state = Input::get('state');
			$client->country = Input::get('country');
			$client->save();

			return Redirect::to('admin/clients')
				->with('message', 'Client Successfully Created');
		}

		return Redirect::to('admin/clients')
			->with('message', 'Something went wrong, Check Form Fields for Errors.')
			->withErrors($validator)
			->withInput();
	}
	public function show($id)
	{
		// get the nerd
		$client = Client::find($id);

		// show the view and pass the nerd to it
		return View::make('admin/clients.show')
				->with('client', $client);
	}


	public function edit($id)
	{
		// get the client
		$client = Client::find($id);

		// show the edit form and pass the nerd
		return View::make('admin/clients.edit')
				->with('client', $client);
	}

	public function update($id)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$validator = Validator::make(Input::all(), Client::$rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('admin/clients/' . $id . '/edit')
					->withErrors($validator)
					->withInput(Input::except('password'));
		} else {
			// store
			$cstool = Client::find($id);
			$cstool->name =Input::get('name');
			$cstool->Weblink =Input::get('weblink');
			$image = Input::file('image');
			$filename = time()."-".$image->getClientOriginalName();
			Image::make($image->getRealPath())->resize(180,64)->save(public_path().'/images/cstool/'.$filename);
			$cstool->image = '/images/cstool/'.$filename;
			$cstool->details = Input::get('details');
			$cstool->tool_function = Input::get('tool_function');
			$cstool->save();

			// redirect
			Session::flash('message', 'Successfully updated nerd!');
			return Redirect::to('admin/clients');
		}
	}

	public function destroy($id){
		$client = Client::find($id);

		if ($client) {
			File::delete('public/images/clients'.$client->image);
			$client->delete();
			return Redirect::to('admin/clients')
				->with('message', 'Client Deleted');
		}

		return Redirect::to('admin/clients')
			->with('message', 'Something went wrong, please try again');
	}

}