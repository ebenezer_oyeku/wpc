<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::resource('admin/dashboard', 'DashboardController');
Route::get('/about', 'AboutController@showAbout');
Route::get('/index', 'IndexController@showIndex');
Route::get('/services', 'ServicesController@showServices');
Route::resource('/careers', 'CareersController');
Route::resource('careers/jobview', 'CareersController');
Route::get('/privacy', 'PrivacyController@showPrivacy');
Route::get('/Faq', 'FaqController@showFaq');
Route::get('/project', 'StoreController@getProjects');
Route::get('/contactus', 'ContactusController@showContactus');
Route::get('login', 'LoginController@showLogin');
Route::post('contactus','ContactusController@getContactUsForm');
Route::get('/remind', 'RemindersController@getRemind');
Route::post('/remind', 'RemindersController@postRemind');
Route::post('/application', 'ApplicationController@postJobApp');

Route::get('password/reset/{token}', array(
    'uses' => 'RemindersController@getReset',
    'as' => 'password.reset'
));

Route::post('password/reset/{token}', array(
    'uses' => 'RemindersController@postReset',
    'as' => 'password.update'
));

Route::get('/thanks', function()
{
    return View::make('account.thanks');
});

Route::get('/', function()
{
    return View::make('index');
});


Route::get('signup/mailers/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'SignupController@confirm'
]);
Route::resource('/admin/categories', 'CategoriesController');
Route::resource('/admin/clients', 'ClientsController');
Route::resource('/admin/projects', 'ProjectsController');
Route::controller('admin/users', 'UsersController');
Route::controller('project', 'StoreController');
Route::get('/application', array('as' => 'careers/application', 'uses' => 'ApplicationController@getIndex'));
Route::get('/childict', 'ChildictController@index');
Route::resource('/childict/softview', 'ChildictController');
Route::resource('/admin/subcategories', 'SubcatController');
Route::resource('/admin/cstool', 'CsToolsController');
Route::resource('/admin/jobs', 'JobsController');
//Route::get('admin/clients/edit', 'ClientsController@edit');





//Route::post('admin/projects', 'ProjectsController@postToggleState');

Route::get('/ajax-subcat',function(){
    $cat_id = Input::get('cat_id');
    $subcategories = Subcategory::where('cat_id', '=', $cat_id)->get();
    return Response::json($subcategories);
});





