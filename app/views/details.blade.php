@extends('layouts.default')

@section('title')
    Project Page.
@stop


@section('banner')
    <div class="page-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Lorem Ipsum</h2>
                    <p>Single Project Subtitle</p>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="#">ChildCS</a></li>
                        <li><a href="#">Software Detail</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div id="content">
        <div class="container">
            <div class="project-page row">

                <!-- Start Single Project Slider -->
                <div class="project-media col-md-8">
                    <div class="touch-slider project-slider">
                        <div class="item">
                            <a class="lightbox" title="This is an image title" href="images/project-big-01.jpg" data-lightbox-gallery="gallery2">
                                <div class="thumb-overlay"><i class="icon-resize-full"></i></div>
                                <img alt="" src="images/project-01.jpg">
                            </a>
                        </div>
                        <div class="item">
                            <a class="lightbox" title="This is an image title" href="images/project-big-02.jpg" data-lightbox-gallery="gallery2">
                                <div class="thumb-overlay"><i class="icon-resize-full"></i></div>
                                <img alt="" src="images/project-02.jpg">
                            </a>
                        </div>
                        <div class="item">
                            <a class="lightbox" title="This is an image title" href="images/project-big-03.jpg" data-lightbox-gallery="gallery2">
                                <div class="thumb-overlay"><i class="icon-resize-full"></i></div>
                                <img alt="" src="images/project-03.jpg">
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End Single Project Slider -->

                <!-- Start Project Content -->
                <div class="project-content col-md-4">
                    <h4><span>Project Description</span></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sed facilisis purus. Donec interdum massa at ipsum vehicula tristique. Maecenas bibendum dictum tincidunt. Sed nec justo ac libero consequat tincidunt. Cras eget molestie justo.</p>
                    <h4><span>Project Details</span></h4>
                    <ul>
                        <li><strong>Client:</strong> Envato</li>
                        <li><strong>Status:</strong> Finish on 30 Dec, 2013</li>
                        <li><strong>Skills:</strong> creative, web design</li>
                    </ul>
                    <div class="post-share">
                        <span>Share This:</span>
                        <a class="facebook" href="#"><i class="icon-facebook"></i></a>
                        <a class="twitter" href="#"><i class="icon-twitter"></i></a>
                        <a class="gplus" href="#"><i class="icon-gplus"></i></a>
                        <a class="linkedin" href="#"><i class="icon-linkedin-1"></i></a>
                        <a class="mail" href="#"><i class="icon-mail-4"></i></a>
                    </div>
                </div>
                <!-- End Project Content -->

            </div>


        </div>
    </div>
@stop
	<!-- Container -->
