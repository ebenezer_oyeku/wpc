<?php
//get the first name
$first_name = Input::get('first_name');
$last_name = Input::get ('last_name');
$phone_number = Input::get('phone_number');
$email = Input::get ('email');
$subject = Input::get ('subject');
$message = Input::get ('message');
$date_time = date("F j, Y, g:i a");
$userIpAddress = Request::getClientIp();
?>

<h1>We have been contacted by.... </h1>

<p>
    <ul>
    <li>First name: <?php echo ($first_name); ?></li>
    <li>Last name: <?php echo($last_name);?></li>
    <li>Phone number: <?php echo($phone_number);?></li>
    <li>Email address: <?php echo ($email);?></li>
    <li>Subject: <?php echo ($subject); ?></li>
    <li>Message: <?php echo ($message);?></li>
    <li>Date: <?php echo($date_time);?></li>
    <li>User IP address: <?php echo($userIpAddress);?></li>
    </ul>
</p>