@extends('layouts.default')

@section('title')
    Software Details
@stop

@section('banner')
    <div class="page-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Start A Career with Us</h2>
                    <p>{{ $job->name }}</p>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="/careers">Careers</a></li>
                        <li><a href="#">Job Details</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
        <!-- Start Content -->
    <div id="content">
        <div class="container">
            <div class="row blog-post-page">
                <div class="col-md-9 blog-box">

                    <!-- Start Single Post Area -->
                    <div class="blog-post gallery-post">

                        <!-- Start Single Post Content -->
                        <div class="post-content">
                            <div class="post-type"><i class=" icon-picture-1"></i></div>
                            <h2>{{$job->name}}</h2>
                            <ul class="post-meta">
                                <li>Created <a href="#">at</a></li>
                                <li>{{$job->created_at}}</li>
                                <li><a href="#">WordPress</a>, <a href="#">Graphic</a></li>
                                <li><a href="#">4 Comments</a></li>
                            </ul>
                            <h3>Job Description</h3>
                            <p>{{$job->description}}</p>

                            <p>Closing Date: {{$job->end_date}}</p>
                        </div>
                        <!-- End Single Post Content -->

                    </div>
                    <!-- End Single Post Area -->

                </div>



                <!-- Sidebar -->
                <div class="col-md-3 sidebar right-sidebar">

                    <!-- Search Widget -->
                    <div class="widget widget-search">
                        <form action="#">
                            <input type="search" placeholder="Enter Keywords..." />
                            <button class="search-btn" type="submit"><i class="icon-search-1"></i></button>
                        </form>
                    </div>

                    <!-- Categories Widget -->
                    <div class="widget widget-categories">
                        <h4>Categories <span class="head-line"></span></h4>
                        <ul>
                            <li>
                                <a href="#">{{$job->summary}}</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!--End sidebar-->


            </div>

        </div>
    </div>
    <!-- End content -->
    @endsection