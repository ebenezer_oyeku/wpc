@extends('layouts.default')

@section('title')
    Kids Hub and Innovation Center
@stop


@section('banner')
    <div class="page-banner" style="padding:40px 0; background: url(images/slide-03-bg.jpg) center #f9f9f9;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Kids/Youths Computing Education</h2>
                    <p>Creating Future Innovators and Inventors</p>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="{{URL::to('index')}}">Home</a></li>
                        <li>Kids CS</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div id="content">
    <div class="container">
        <div class="page-content">


            <div class="row">

                <div class="col-md-7">

                    <!-- Classic Heading -->
                    <h4 class="classic-title"><span>Computing Science Education for Kids in School and Out of School</span></h4>

                    <!-- Some Text -->
                    <p>Computing Science Education and Programming for children/youth in schools and out of schools in Nigeria. Changing the phase of Computing Science acceptability among children/youths in Nigeria.
                        The program is design to help children/youths from primary and secondary schools and out of school children/youths to be entrepreneur with the Art Computing Science Education with use of collaboration and teamwork.</p>
                    <p>Building young Entrepreneur from a tender age, children/youth Empowerment and to reduce the rate of Unemployment. Public Primary and Secondary schools, Private Primary and Secondary schools, Out of School children/youth in the society, Rural Communities.</p>

                </div>

                <div class="col-md-5">

                    <!-- Start Touch Slider -->
                    <div class="touch-slider" data-slider-navigation="true" data-slider-pagination="true">
                        <div class="item"><img alt="" src="images/1.jpg"></div>
                        <div class="item"><img alt="" src="images/2.jpg"></div>
                        <div class="item"><img alt="" src="images/3.jpg"></div>
                        <div class="item"><img alt="" src="images/4.jpg"></div>
                    </div>
                    <!-- End Touch Slider -->

                </div>

            </div>

            <!-- Divider -->
            <div class="hr1" style="margin-bottom:50px;"></div>

            <div class="row">

                <div class="row">
                    <div class="col-md-6">

                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>Why we Invest in this Innovation</span></h4>

                        <!-- Accordion -->
                        <div class="panel-group" id="accordion">

                            <!-- Start Accordion 1 -->
                            <div class="panel panel-default">
                                <!-- Toggle Heading -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-one">
                                            <i class="icon-down-open-1 control-icon"></i>
                                            <i class="icon-laptop-1"></i> Showcasing African Talents
                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id="collapse-one" class="panel-collapse collapse in">
                                    <div class="panel-body">We are here to discover Kids/youths in Africa Communities and showcase them to the world with skills in Computing Science Innovations.</div>
                                </div>
                            </div>
                            <!-- End Accordion 1 -->

                            <!-- Start Accordion 2 -->
                            <div class="panel panel-default">
                                <!-- Toggle Heading -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-tow" class="collapsed">
                                            <i class="icon-down-open-1 control-icon"></i>
                                            <i class="icon-gift-1"></i> Creating Expert Hubs in Kids/Youths
                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id="collapse-tow" class="panel-collapse collapse">
                                    <div class="panel-body">Creating An Expert Hub of kids and youth in Africa, where teamwork with collaborative ideas would invent a future technology for Economy Sectors in Africa and the rest of the World.</div>
                                </div>
                            </div>
                            <!-- End Accordion 2 -->

                            <!-- Start Accordion 3 -->
                            <div class="panel panel-default">
                                <!-- Toggle Heading -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-three" class="collapsed">
                                            <i class="icon-down-open-1 control-icon"></i>
                                            <i class="icon-tint"></i> World class Experience in the IT Education for Teens.
                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id="collapse-three" class="panel-collapse collapse">
                                    <div class="panel-body">We have been able to partner and embed different IT Educational Curriculum in other to meet up with parents/Teens expectation in offering a World call Computing Education. And this has increase our success and build up the commitment spirit with our Clients.</div>
                                </div>
                            </div>
                            <!-- End Accordion 3 -->

                        </div>
                        <!-- End Accordion -->

                    </div>

                    <div class="col-md-6">

                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>Mission of Program</span></h4>

                        <!-- Start Progress Bar 1 -->
                        <div class="progress-label">Collaborate Ideas</div>
                        <div class="progress progress-striped">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="100%">
                                <span class="percentage">100%</span>
                            </div>
                        </div>

                        <!-- Start Progress Bar 2 -->
                        <div class="progress-label">Team-Work</div>
                        <div class="progress progress-striped">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="100%" data-appear-animation-delay="400">
                                <span class="percentage">100%</span>
                            </div>
                        </div>

                        <!-- Start Progress Bar 3 -->
                        <div class="progress-label">Groom Innovators</div>
                        <div class="progress progress-striped">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="100%" data-appear-animation-delay="800">
                                <span class="percentage">100%</span>
                            </div>
                        </div>

                        <!-- Start Progress Bar 4 -->
                        <div class="progress-label">Discover Inventors</div>
                        <div class="progress progress-striped">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="100%" data-appear-animation-delay="1200">
                                <span class="percentage">100%</span>
                            </div>
                        </div>

                    </div>
                </div>



            </div>
            <!-- Start Full Width Section 5 -->
            <div class="section" style="padding-top:60px; padding-bottom:60px; border-top:0; border-bottom:0; background:#fff;">

                <!-- Start Big Heading -->
                <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                    <p class="title-desc">Our Learn tools for Work</p>
                    <h1>This is Our Learning <strong>Tools for Kids & Youths</strong></h1>
                </div>
                <!-- End Big Heading -->

                <p class="text-center">We are agent of Change or Changemaker, changing the phase of Computing Science Acceptability among Kids, Parents and Schools.<br/>
                    Discovering Hidden Talent with Information Technology Sciences in Many Kids and Youths.</p>

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:25px;"></div>

                <!-- Start Recent Projects Carousel -->
                <div class="full-width-recent-projects">
                    <div class="projects-carousel touch-carousel navigation-3">
                        @foreach($cstools as $cstool)
                        <!-- Start Project Item -->
                        <div class="portfolio-item item">
                            <div class="portfolio-border">
                                <!-- Start Project Thumb -->
                                <div class="portfolio-thumb">
                                    <a href="/childict/softview/{{$cstool->slug}}">
                                        <div class="thumb-overlay"><i class="icon-link-1"></i></div>
                                        <img alt="" src="{{$cstool->image}}" />
                                    </a>
                                </div>
                                <!-- End Project Thumb -->
                            </div>
                        </div>
                        @endforeach
                        <!-- End Project Item -->
                    </div>
                </div>
                <!-- End Recent Projects Carousel -->

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:35px;"></div>

            </div>
            <!-- End Full Width Section 5 -->

            <div class="row">
                <img alt="" src="images/Code.org.png" />
            </div>
        </div>
    </div>
    </div>
@stop


    
