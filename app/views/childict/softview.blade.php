@extends('layouts.default')

@section('title')
    Software Details
@stop

@section('banner')
    <div class="page-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Kids Computing Programming Software Details</h2>
                    <p>{{ $cstool->name }}</p>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="/childict">CS Child</a></li>
                        <li><a href="#">CS Detail</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div id="content">
        <div class="container">
            <div class="project-page row">

                <!-- Start Single Project Slider -->
                <div class="project-media col-md-8">
                    <div class="touch-slider project-slider">
                        <div class="item">
                            <a class="lightbox" title="This is an image title" href="{{ $cstool->image }}" data-lightbox-gallery="gallery2">
                                <div class="thumb-overlay"><i class="icon-resize-full"></i></div>
                                <img alt="" src="{{ $cstool->image }}">
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End Single Project Slider -->

                <!-- Start Project Content -->
                <div class="project-content col-md-4">
                    <h4><span>CS Tool Description</span></h4>
                    <p>{{ nl2br(e($cstool->details)) }}</p>
                    <h4><span>{{ $cstool->slug }}</span></h4>
                    <ul>
                        <li><strong>Weblink:</strong><a href="{{$cstool->weblink}}">{{$cstool->weblink}}</a></li>
                        <li><strong>Function:</strong> {{$cstool->tool_function}}</li>
                    </ul>

                    <div class="post-share">
                        <span class='st_sharethis_large' displayText='ShareThis'></span>
                        <span class='st_facebook_large' displayText='Facebook'></span>
                        <span class='st_twitter_large' displayText='Tweet'></span>
                        <span class='st_linkedin_large' displayText='LinkedIn'></span>
                        <span class='st_pinterest_large' displayText='Pinterest'></span>
                        <span class='st_email_large' displayText='Email'></span>
                    </div>

                </div>
                <!-- End Project Content -->
                <br>


            </div>


        </div>
    </div>
@stop
