@extends('layouts.auth')

@section('title')
    WPC | Login
@endsection

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="index">{{HTML::image('img/wpc.png')}}</a>
        </div><!-- /.login-logo -->
<div class="login-box-body">

    <div class="social-auth-links text-center">
        <a href="https://www.webplanetcon.com/Freelance_Office/" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-ticket"></i> Sign as a Client</a>
        <a href="https://www.webplanetcon.com/kid_techhub/" class="btn btn-block btn-social btn-twitter btn-flat"><i class="fa fa-hand-peace-o"></i> Signin to KidsTechHub</a>
        {{--<a href="/sign-in-with-google" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>--}}
    </div><!-- /.social-auth-links -->

    {{--<a href="{{URL::to('remind')}}" class="pull-right help">Forgot Password? </a><span class="clearfix"></span><br>--}}
    <a href="https://www.webplanetcon.com/Freelance_Office/auth/register" class="text-center">Register a new membership </a>

</div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
@endsection

