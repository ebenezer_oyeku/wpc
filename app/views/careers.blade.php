@extends('layouts.default')

@section('title')
    Our Career and Vacancy
@stop


@section('banner')
    <div class="page-banner" style="padding:40px 0; background: url(images/slide-02-bg.jpg) center #f9f9f9;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Careers</h2>
                    <p>We Are Professional</p>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="{{URL::to('index')}}">Home</a></li>
                        <li>Careers</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div id="content">
    <div class="container">
        <div class="page-content">


            <div class="row">
                @if (count($jobs))
                @foreach($jobs as $job)
                <!-- Start Job Box -->
                <div class="col-md-4 image-service-box">
                    <img class="img-thumbnail" src="{{$job->image}}" alt="" />
                    <h4>{{$job->name}}</h4>
                    <p>{{$job->summary}}</p>
                    <a href="/careers/jobview/{{$job->slug}}" class="btn-system border-btn btn-medium">Job Description & Requirement</a>
                </div>
                @endforeach
                @else
                    <div class="alert alert-danger">
                        <strong>Oh snap!</strong> No Job Openings this time, Check back again.
                    </div>
                @endif
                <!-- End Job Box -->
            </div>
            <br>
            <p><a href="{{URL::to('careers/application')}}" class="btn-system border-btn btn-medium">Apply Online</a></p>
            <p>Please kindly send your Resume to <strong class="accent-color">careers@weplanetcon.com</strong> as mail specifying the Position as A Subject of your mail.</p>
        </div>

        <div class="hr1" style="margin-bottom:45px;"></div>

        <div class="row">

            <div class="col-md-4">

                <!-- Classic Heading -->
                <h4 class="classic-title"><span>We Aare Awesome</span></h4>

                <!-- Accordion -->
                <div class="panel-group" id="accordion">

                    <!-- Start Accordion 1 -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-one">
                                    <i class="icon-down-open-1 control-icon"></i>
                                    <i class="icon-laptop-1"></i> Great Customer Service
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-one" class="panel-collapse collapse in">
                            <div class="panel-body">Our Customer/Clients are very important to us at WPC. Tha is why we put their Satisfaction on Our priority List in Our Firm. Our goal is to always make our customers smile due to the fact that "Their Success id Our Success too!"</div>
                        </div>
                    </div>
                    <!-- End Accordion 1 -->

                    <!-- Start Accordion 2 -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-tow" class="collapsed">
                                    <i class="icon-down-open-1 control-icon"></i>
                                    <i class="icon-gift-1"></i> Dedicated Team of Experts
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-tow" class="panel-collapse collapse">
                            <div class="panel-body">At WPC we believe in what our employees are good at doing, not what they have an idea about. These has helped our productivity by employing Professionals that has strong commitment in ICT Industry and are always ready to learn New Technology to enhance our Customer/Clients Productivties.</div>
                        </div>
                    </div>
                    <!-- End Accordion 2 -->

                </div>
                <!-- End Accordion -->

            </div>

            <div class="col-md-8">

                <!-- Classic Heading -->
                <h4 class="classic-title"><span>Our Features</span></h4>

                <div class="row">

                    <!-- Start Service Icon 1 -->
                    <div class="col-md-6 service-box service-icon-left-more">
                        <div class="service-icon">
                            <i class="icon-paper-plane icon-medium"></i>
                        </div>
                        <div class="service-content">
                            <h4>Childs ICT Education</h4>
                            <p>We are passionate about giving <strong class="accent-color">values</strong> to every child with the knowlegde f Computer Programming.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 1 -->

                    <!-- Start Service Icon 2 -->
                    <div class="col-md-6 service-box service-icon-left-more">
                        <div class="service-icon">
                            <i class="icon-basket-1 icon-medium"></i>
                        </div>
                        <div class="service-content">
                            <h4>Business Application</h4>
                            <p>We Provide Business application <strong class="accent-color">softwares</strong> for businesses to run effectively as desired.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 2 -->

                    <!-- Start Service Icon 3 -->
                    <div class="col-md-6 service-box service-icon-left-more">
                        <div class="service-icon">
                            <i class="icon-globe-1 icon-medium"></i>
                        </div>
                        <div class="service-content">
                            <h4>Web Development</h4>
                            <p>We develop your <strong class="accent-color">ideas, businesses and inovations</strong> on the world wide web.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 3 -->

                    <!-- Start Service Icon 4 -->
                    <div class="col-md-6 service-box service-icon-left-more">
                        <div class="service-icon">
                            <i class="icon-camera-alt icon-medium"></i>
                        </div>
                        <div class="service-content">
                            <h4>Graphic Design</h4>
                            <p>We design your <strong class="accent-color">Wholesome Graphics</strong> for your needs.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 4 -->

                    <!-- Start Service Icon 5 -->
                    <div class="col-md-6 service-box service-icon-left-more">
                        <div class="service-icon">
                            <i class="icon-feather icon-medium"></i>
                        </div>
                        <div class="service-content">
                            <h4>Software Design</h4>
                            <p>We design your <strong class="accent-color">Dream Application</strong> with our understanding of your ideas.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 5 -->

                    <!-- Start Service Icon 6 -->
                    <div class="col-md-6 service-box service-icon-left-more">
                        <div class="service-icon">
                            <i class="icon-tools icon-medium"></i>
                        </div>
                        <div class="service-content">
                            <h4>Supporting</h4>
                            <p>We are here <strong class="accent-color">24/7</strong> for your business support at everytime you call on Us.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 6 -->

                </div>
            </div>

        </div>

        <!-- Divider -->
        <div class="hr1" style="margin-bottom:25px;"></div>

        <div class="row">

            <div class="col-md-8">

            </div>

        </div>


    </div>
    </div>
    @stop
