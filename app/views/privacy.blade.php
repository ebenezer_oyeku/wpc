@extends('layouts.default')

@section('title')
    Privacy Policy
@stop



@section('banner')
    <div class="page-banner no-subtitle">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Privacy Policy</h2>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="#">Home</a></li>
                        <li>Privacy Policy</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @stop

@section('content')
    <div id="content">
        <div class="container">
            <div class="row sidebar-page">


                <!-- Page Content -->
                <div class="col-md-9 page-content">

                    <!-- Classic Heading -->
                    <h4 class="classic-title"><span>PRIVACY STATEMENT</span></h4>

                    <!-- Some Text -->
                    <p>
                        Your privacy is important to WebPlanet Consulting and Services.  This privacy statement provides information about the personal information that WebPlanet Consulting and Services collects, and the ways in which WPC uses that personal information
                    </p>

                    <!-- Divider -->
                    <div class="hr5" style="margin-top:30px; margin-bottom:45px;"></div>

                    <!-- Accordion -->
                    <div class="panel-group" id="accordion">

                        <!-- Start Accordion 1 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-4">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-laptop-1"></i> Personal Information Collection
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-4" class="panel-collapse collapse in">
                                <div class="panel-body"><img class="img-thumbnail image-text" style="float:left; width:180px;" alt="" src="images/bussniss-pic.jpg" /> <strong class="accent-color">WPC may collect and use the following kinds of personal information:</strong><br>
                                    <ul class="icons-list">
                                        <li><i class="icon-attention-circled"></i>Register as a user either as Student, Parent, Educators and Schools.</li>
                                        <li><i class="icon-attention-circled"></i>Information for Registration include Personal Data has stated on the registration form or Social Network Logins.</li>
                                        <li><i class="icon-attention-circled"></i>Transaction on our website include Donation and Parent Payment for kids Education.</li>
                                        <li><i class="icon-attention-circled"></i>For Subscribing to our Newsletter we need your email Address Only</li>
                                        <li><i class="icon-attention-circled"></i>Use our feeback form on Our <strong>Contact Page</strong> to tell us our improvement in service and comments.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- End Accordion 3 -->

                        <!-- Start Accordion 2 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-5" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-gift-1"></i> Using Personal Information
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-5" class="panel-collapse collapse">
                                <div class="panel-body">WPC may use your personal information to:
                                    <ul class="icons-list">
                                        <li><i class="icon-ok-squared"></i>administer this website;</li>
                                        <li><i class="icon-ok-squared"></i>personalize the website for you;</li>
                                        <li><i class="icon-ok-squared"></i>enable your access to and use of the website services;</li>
                                        <li><i class="icon-ok-squared"></i>publish information about you on the website;</li>
                                        <li><i class="icon-ok-squared"></i>send to you products that you purchase;</li>
                                        <li><i class="icon-ok-squared"></i>supply to you services that you purchase;</li>
                                        <li><i class="icon-ok-squared"></i>send to you statements and invoices;</li>
                                        <li><i class="icon-ok-squared"></i>collect payments/donation from you;</li>
                                        <li><i class="icon-ok-squared"></i>send you marketing newsletters</li>
                                    </ul>
                                    <p>Where WPC discloses your personal information to its agents or sub-contractors for these purposes, the agent or sub-contractor in question will be obligated to use that personal information in accordance with the terms of this privacy statement.</p>
                                    <p>In addition to the disclosures reasonably necessary for the purposes identified elsewhere above, WPC] may disclose your personal information to the extent that it is required to do so by law, in connection with any legal proceedings or prospective legal proceedings, and in order to establish, exercise or defend its legal rights.</p>

                                </div>
                            </div>
                        </div>
                        <!-- End Accordion 3 -->

                        <!-- Start Accordion 3 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-6" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-tint"></i> Securing Your Data
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-6" class="panel-collapse collapse">
                                <div class="panel-body"><strong>WPC</strong> will take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information.
                                    <p>WPC will store all the personal information you provide on our secure servers.</p>
                                    <ul class="icons-list">
                                        <li><i class="icon-attention-2"></i>Note: Information relating to electronic transactions entered into via this website will be protected by encryption technology.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Start Accordion 4 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-7" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-tasks"></i> CrossBorder Data Transfer
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-7" class="panel-collapse collapse">
                                <div class="panel-body"><p>Information that WPC collects may be stored and processed in and transferred between any of the countries in which WPC operates to enable the use of the information in accordance with this privacy policy.</p>
                                    <p>In addition, personal information that you submit for publication on the website will be published on the internet and may be available around the world.</p>
                                    <p>You agree to such cross-border transfers of personal information.</p>

                                </div>
                            </div>
                        </div>

                        <!-- Start Accordion 5 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-8" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-chart-bar-3"></i> Updating This statement
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-8" class="panel-collapse collapse">
                                <div class="panel-body"><strong>WPC</strong> may update this privacy policy by posting a new version on this website.
                                    <ul class="icons-list">
                                        <li><i class="icon-attention-2"></i>Note: You should check this page occasionally to ensure you are familiar with any changes.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Start Accordion 6 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-9" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-globe-3"></i> Other Websites
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-9" class="panel-collapse collapse">
                                <div class="panel-body"><strong>WPC</strong> website contains links to other websites..
                                    <ul class="icons-list">
                                        <li><i class="icon-attention-2"></i>Note: WebPlanet Consulting and Services is not responsible for the privacy policies or practices of any third party..</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Start Accordion 7 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-10" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-globe-3"></i> Contact WebPlanet Consulting and Services
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-10" class="panel-collapse collapse">
                                <div class="panel-body">If you have any questions about this privacy policy or WPC treatment of your personal information, please write:
                                    <ul class="icons-list">
                                        <li><i class="icon-briefcase-2"></i>by email to info@webplanetcon.com</li>
                                        <li><i class="icon-truck"></i>by post-mail to 6 Sanni Aro Street, Onireke, Ilaje, Ajah, Lagos State, Nigeria</li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- End Accordion -->

                </div>
                <!-- End Page Content-->


                <!--Sidebar-->
                <div class="col-md-3 sidebar right-sidebar">

                    <div class="widget">
                        <h4>Certifates <span class="head-line"></span></h4>
                        <div>
                            <a href="#"><img src="images/ssl.png" alt="" /></a>
                        </div>
                        <div>
                            <a href="#"><img src="images/comodo.png" alt="" /></a>
                        </div>
                    </div>


                </div>
                <!--End sidebar-->


            </div>
        </div>
    </div>
    @stop


      

