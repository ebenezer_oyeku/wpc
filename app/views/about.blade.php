@extends('layouts.default')

@section('title')
    About WPC
@stop


@section('banner')
    <div class="page-banner" style="padding:40px 0; background: url(images/slide-02-bg.jpg) center #f9f9f9;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>About Us</h2>
                    <p>We Are Professional</p>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="{{URL::to('index')}}">Home</a></li>
                        <li>About Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')

    <div id="content">
    <div class="container">
        <div class="page-content">


            <div class="row">

                <div class="col-md-7">

                    <!-- Classic Heading -->
                    <h4 class="classic-title"><span>Why Webplanet Consulting</span></h4>

                    <!-- Some Text -->
                    <p>We believe that our clients success, is our success too. We believe in professionalism, we constantly and consistently maintain high standards for services and consultancy in other to be able to have the best team of consultants on every project we are working on.
                        We understand our client’s business needs, we understand their aim and we always want the best for our clients. We help small scales businesses, medium and large scales businesses to grow and maximize their revenues.</p>
                    <p>We have been able to work in different Industry Sectors in other to meet up with our Clients needs. And this has increase our success and build up the commitment spirit with our Clients.</p>

                </div>

                <div class="col-md-5">

                    <!-- Start Touch Slider -->
                    <div class="touch-slider" data-slider-navigation="true" data-slider-pagination="true">
                        <div class="item"><img alt="" src="images/about-01.jpg"></div>
                        <div class="item"><img alt="" src="images/about-02.jpg"></div>
                        <div class="item"><img alt="" src="images/about-03.jpg"></div>
                    </div>
                    <!-- End Touch Slider -->

                </div>

            </div>

            <!-- Divider -->
            <div class="hr1" style="margin-bottom:50px;"></div>

            <div class="row">

                <div class="row">
                    <div class="col-md-6">

                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>Why Choose Us</span></h4>

                        <!-- Accordion -->
                        <div class="panel-group" id="accordion">

                            <!-- Start Accordion 1 -->
                            <div class="panel panel-default">
                                <!-- Toggle Heading -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-one">
                                            <i class="icon-down-open-1 control-icon"></i>
                                            <i class="icon-laptop-1"></i> World class Experience in the IT Divisions
                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id="collapse-one" class="panel-collapse collapse in">
                                    <div class="panel-body">Our Management Team are professionals that have acquired experience in different Sectors of Business Industry in relative to Information Technology Engineering and Managements. As a Team with world class Exposure we are committed to serve you.</div>
                                </div>
                            </div>
                            <!-- End Accordion 1 -->

                            <!-- Start Accordion 2 -->
                            <div class="panel panel-default">
                                <!-- Toggle Heading -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-tow" class="collapsed">
                                            <i class="icon-down-open-1 control-icon"></i>
                                            <i class="icon-gift-1"></i> South Asia Experience (Vietnam)
                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id="collapse-tow" class="panel-collapse collapse">
                                    <div class="panel-body">With the fast growth of information Technology creation and digest in Asia continent, we have consultant in the Asian Countries that work to meet up the needs of our clients in Asian countries like: Vietnam, Philippines, Laos, Cambodia, China and so on. We use this to minimize the gap that mostly occur due to Language and cultural barrier in the Western Countries and Asia countries.</div>
                                </div>
                            </div>
                            <!-- End Accordion 2 -->

                            <!-- Start Accordion 3 -->
                            <div class="panel panel-default">
                                <!-- Toggle Heading -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-three" class="collapsed">
                                            <i class="icon-down-open-1 control-icon"></i>
                                            <i class="icon-tint"></i> Add World-Class Values to Businesses Around the World
                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id="collapse-three" class="panel-collapse collapse">
                                    <div class="panel-body">We are passionate on the Growth & Success of every Clients and Prospective Clients Businesses. We get involved by adding Business Management Tools in other improve business processes and revenues.</div>
                                </div>
                            </div>
                            <!-- End Accordion 3 -->

                        </div>
                        <!-- End Accordion -->

                    </div>

                    <div class="col-md-6">

                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>Our Skills</span></h4>

                        <!-- Start Progress Bar 1 -->
                        <div class="progress-label">Software Engineering</div>
                        <div class="progress progress-striped">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="98%">
                                <span class="percentage">98%</span>
                            </div>
                        </div>

                        <!-- Start Progress Bar 2 -->
                        <div class="progress-label">Application Developing</div>
                        <div class="progress progress-striped">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="96%" data-appear-animation-delay="400">
                                <span class="percentage">96%</span>
                            </div>
                        </div>

                        <!-- Start Progress Bar 3 -->
                        <div class="progress-label">ICT Education</div>
                        <div class="progress progress-striped">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="94%" data-appear-animation-delay="800">
                                <span class="percentage">94%</span>
                            </div>
                        </div>

                        <!-- Start Progress Bar 4 -->
                        <div class="progress-label">ICT Support</div>
                        <div class="progress progress-striped">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="100%" data-appear-animation-delay="1200">
                                <span class="percentage">100%</span>
                            </div>
                        </div>

                    </div>
                </div>



            </div>

            <!-- Divider -->
            <div class="hr1" style="margin-bottom:50px;"></div>

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Our Creative Team</span></h4>

            <!-- Start Team Members -->
            <div class="row">

                <!-- Start Memebr 1 -->
                <div class="col-md-3">
                    <div class="team-member">
                        <!-- Memebr Photo, Name & Position -->
                        <div class="member-photo">
                            <img alt="" src="images/member-01.png" />
                            <div class="member-name">Ebenezer Oyeku <span>Team Leader</span></div>
                        </div>
                        <!-- Memebr Words -->
                        <div class="member-info">
                            <p>Database Administrator, Developer and Software Analyst.</p>
                        </div>
                        <!-- Memebr Social Links -->
                        <div class="member-socail">
                            <a class="twitter" href="http://twitter.com/Unstinted"><i class="icon-twitter-2"></i></a>
                            <a class="gplus" href="https://plus.google.com/u/0/113737353160487799790/posts"><i class="icon-gplus"></i></a>
                            <a class="linkedin" href="https://www.linkedin.com/profile/view?id=87591725"><i class="icon-linkedin"></i></a>
                            <a class="instagramm" href="https://www.instagram.com/unstinted_usd/"><i class="icon-instagramm"></i></a>
                            <a class="mail" href="mailto:ebenezeroyeku@webplanetcon.com"><i class="icon-mail-2"></i></a>
                        </div>
                    </div>
                </div>
                <!-- End Memebr 1 -->

                <!-- Start Memebr 2 -->
                <div class="col-md-3">
                    <div class="team-member">
                        <!-- Memebr Photo, Name & Position -->
                        <div class="member-photo">
                            <img alt="" src="images/member-02.png" />
                            <div class="member-name">Tran Bach <span>Developer</span></div>
                        </div>
                        <!-- Memebr Words -->
                        <div class="member-info">
                            <p>Application Developer and Software Engineer</p>
                        </div>
                        <!-- Memebr Social Links -->
                        <div class="member-socail">
                            <a class="facebook" href="https://www.facebook.com/tranbach93?fref=ts"><i class="icon-facebook"></i></a>
                            <a class="gplus" href="https://plus.google.com/u/0/+B%C3%A1chTr%E1%BA%A7n93/posts"><i class="icon-gplus"></i></a>
                        </div>
                    </div>
                </div>
                <!-- End Memebr 2 -->

                <!-- Start Memebr 3 -->
                <div class="col-md-3">
                    <div class="team-member">
                        <!-- Memebr Photo, Name & Position -->
                        <div class="member-photo">
                            <img alt="" src="images/member-03.png" />
                            <div class="member-name">Opeyemi Aderibigbe <span>Company Secretary </span></div>
                        </div>
                        <!-- Memebr Words -->
                        <div class="member-info">
                            <p>Head Human Resource</p>
                        </div>
                        <!-- Memebr Social Links -->
                        <div class="member-socail">
                            <a class="facebook" href="https://www.facebook.com/opeyemi.oyeku.7?fref=ts"><i class="icon-facebook"></i></a>
                            <a class="twitter" href="https://twitter.com/opnaty2009"><i class="icon-twitter-2"></i></a>
                            <a class="linkedin" href="https://www.linkedin.com/in/opeyemi-oyeku-31b07194"><i class="icon-linkedin"></i></a>
                            <a class="mail" href="mailto:opeyemi.aderibigbe@webplanetcon.com"><i class="icon-mail-2"></i></a>
                        </div>
                    </div>
                </div>
                <!-- End Memebr 3 -->

                <!-- Start Memebr 4 -->
                <div class="col-md-3">
                    <div class="team-member">
                        <!-- Memebr Photo, Name & Position -->
                        <div class="member-photo">
                            <img alt="" src="images/member-04.png" />
                            <div class="member-name">Nguyen Duc Ahn <span>Project Manager</span></div>
                        </div>
                        <!-- Memebr Words -->
                        <div class="member-info">
                            <p>Application Developer and System Management.</p>
                        </div>
                        <!-- Memebr Social Links -->
                        <div class="member-socail">
                            <a class="facebook" href="https://www.facebook.com/nguyenduc.anh.906?fref=ts"><i class="icon-facebook"></i></a>
                            <a class="gplus" href="https://plus.google.com/u/0/118353886690704267032/posts"><i class="icon-gplus"></i></a>
                        </div>
                    </div>
                </div>
                <!-- End Memebr 4 -->

            </div>
            <!-- End Team Members -->
            <div class="row">
                <img alt="" src="images/Code.org.png" />
            </div>
        </div>
    </div>
    </div>
@stop




