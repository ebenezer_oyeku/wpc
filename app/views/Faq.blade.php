@extends('layouts.default')

@section('title')
    Frequent Asked Questions.
@stop


@section('banner')
    <div class="page-banner no-subtitle">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Frequent Ask Questions</h2>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="#">Home</a></li>
                        <li>FAQ</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @stop

@section('content')
    <div id="content">
        <div class="container">
            <div class="row sidebar-page">


                <!-- Page Content -->
                <div class="col-md-9 page-content">

                    <!-- Classic Heading -->
                    <h4 class="classic-title"><span>FREQUENT ASKED QUESTIONS</span></h4>

                    <!-- Some Text -->
                    <p>
                        Questions Parents, School Administrators, Students and CLients ask about WebPlanet Consulting and Services innovation on ICT Education for All and Services.
                    </p>

                    <!-- Divider -->
                    <div class="hr5" style="margin-top:30px; margin-bottom:45px;"></div>

                    <!-- Accordion -->
                    <div class="panel-group" id="accordion">

                        <!-- Start Accordion 1 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-4">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-globe-3"></i> Who is Webplanet Consulting and Services?
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-4" class="panel-collapse collapse in">
                                <div class="panel-body"><br>
                                    <ul class="icons-list">
                                        <li><i class="icon-attention-circled"></i>WebPlanet Consulting and Services is an Information Technology business Consulting, Child Computing Programming Consultant and Legal Advisory service based in Asia and Africa. We are bridging gaps between businesses managements in Information Technology and the Ends users of the services.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- End Accordion 3 -->

                        <!-- Start Accordion 2 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-5" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-globe-3"></i> What is the main goal of Child Computing Programming?
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="icons-list">
                                        <li><i class="icon-ok-squared"></i>Our Goal is to bring the information technology to your doorstep and make it relevant for all stages of Life both Adults and Kids</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <!-- End Accordion 3 -->

                        <!-- Start Accordion 3 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-6" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-globe-3"></i> How does my Child benefits from this program and what Future prospect does it offer them?
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-6" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="icons-list">
                                        <li><i class="icon-attention-2"></i>Scholarships and Grants for Outstanding and Talented Kids both locally and overseas.</li>
                                        <li><i class="icon-attention-2"></i>Bringing Technology to their Finger Tips.</li>
                                        <li><i class="icon-attention-2"></i>Show Casing the Kids to the world as young innovators, inventors and Scientists.</li>
                                        <li><i class="icon-attention-2"></i>Helping the Kids with Logical Thinking and problem solving in a real world scenarios.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Start Accordion 4 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-7" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-globe-3"></i> Is it possible that my child school embed this program in their Curriculum?
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-7" class="panel-collapse collapse">
                                <div class="panel-body"><p>Yes, We have arrange to put this in place by visiting schools ones per week and spending 2 to 3 hours to lecture and practical’s on  what we have taught the student at each lesson learnt. With this learning steps it will be easy for the student to understand and use Information Technology learnt into practice on their own in their respective homes and schools.</p>

                                </div>
                            </div>
                        </div>

                        <!-- Start Accordion 5 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-8" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-globe-3"></i> What is the cost of this program for the Kids that are ready to be involved?
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-8" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="icons-list">
                                        <li><i class="icon-attention-2"></i>Pricing for Schools Kids: 10,000 per Term, 1 Study/Week.</li>
                                        <li><i class="icon-attention-2"></i>Pricing for Individual Kids at their respective Homes: 10,000 per Month, 3 study/week.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Start Accordion 6 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-9" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-globe-3"></i> How does this Program differ from ICT Education Offered in Schools in the Nigeria Curriculum.
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-9" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="icons-list">
                                        <li><i class="icon-attention-2"></i>The program is design to help children/youths from primary and secondary schools and out of school children/youths to be entrepreneur with the Art Computing Science Education with use of collaboration and teamwork.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Start Accordion 7 -->
                        <div class="panel panel-default">
                            <!-- Toggle Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-10" class="collapsed">
                                        <i class="icon-down-open-1 control-icon"></i>
                                        <i class="icon-globe-3"></i> Contact WebPlanet Consulting and Services
                                    </a>
                                </h4>
                            </div>
                            <!-- Toggle Content -->
                            <div id="collapse-10" class="panel-collapse collapse">
                                <div class="panel-body">If you have any questions about this privacy policy or WPC treatment of your personal information, please write:
                                    <ul class="icons-list">
                                        <li><i class="icon-briefcase-2"></i>by email to info@webplanetcon.com</li>
                                        <li><i class="icon-truck"></i>by post-mail to 6 Sanni Aro Street, Onireke, Ilaje, Ajah, Lagos State, Nigeria</li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- End Accordion -->

                </div>
                <!-- End Page Content-->


                <!--Sidebar-->
                <div class="col-md-3 sidebar right-sidebar">

                    <div class="widget">
                        <h4>Certifates <span class="head-line"></span></h4>
                        <div>
                            <a href="#"><img src="images/ssl.png" alt="ssl Security" /></a>
                        </div>
                        <div>
                            <a href="#"><img src="images/comodo.png" alt="Comodo logo" /></a>
                        </div>
                    </div>


                </div>
                <!--End sidebar-->


            </div>
        </div>
    </div>
    @stop

      
      


