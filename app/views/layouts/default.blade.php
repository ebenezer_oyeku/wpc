<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="keywords"  name="Webplanet Consulting" content="Webplanet, WPC, WPC + Code, WPC + Code.org, Webplanet Consulting, Webplanet Consulting and Services, Child ICT, Kids Programming, African young minds, ChangeMaker, Innovator, Webplanet Consulting and Services provide supports, services, management and developments in ICT, Administrative  supports and Graphics Design. We also offer Transcribing services for audio and videos">
    <meta name="Webplanet Consulting - Child ICT and Progrmming " content="Creating a better future for the young minds">
    <meta name="description" name="Ebenezer Oyeku" content="ChangeMaker!"/>
    <meta name="viewport" content="initial-scale-1.0, width=device-width, height=device-height, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <meta property="place:location:latitude" content="13.062616"/>
    <meta property="place:location:longitude" content="80.229508"/>
    <meta property="business:contact_data:street_address" content="6 Sanni Aro street, Onireke, Ilaje, Ajah"/>
    <meta property="business:contact_data:locality" content="Lagos State"/>
    <meta property="business:contact_data:postal_code" content="23401"/>
    <meta property="business:contact_data:country_name" content="Nigeria"/>
    <meta property="business:contact_data:email" content="info@webplanetcon.com"/>
    <meta property="business:contact_data:phone_number" content="+23408168694999"/>
    <meta property="business:contact_data:website" content="http://www.webplanetcon.com"/>

    <meta name="twitter:card" content="summary"/>  <!-- Card type -->
    <meta name="twitter:site" content="WPC"/>
    <meta name="twitter:title" content="Webplanet Consulting and Services">
    <meta name="twitter:description" content="Innovator of Child ICT and Programming for African Kids"/>
    <meta name="twitter:creator" content="WPC"/>
    <meta name="twitter:image:src" content="https://webplanetcon.com/images/vella.png"/>
    <meta name="twitter:domain" content="webplanetcon.com"/>

    <meta itemprop="name" content="Webplanet Consulting and Services"/>
    <meta itemprop="description" content="ICT for all"/>
    <meta itemprop="image" content="https://www.webplanetcon.com/images/vella.png"/>

    <meta property="og:type" content="odammy"/>
    <meta property="profile:first_name" content="Ebenezer"/>
    <meta property="profile:last_name" content="Oyeku"/>
    <meta property="profile:username" content="odammy"/>
    <meta property="og:title" content="Webplanet Consulting and Services"/>
    <meta property="og:description" content="Creating a better future for the young minds"/>
    <meta property="og:image" content="https://www.webplanetcon.com/images/vella.png"/>
    <meta property="og:url" content="http://www.webplanetcon.com"/>
    <meta property="og:site_name" content="Webplanet Consulting and Services"/>
    <meta property="og:see_also" content="http://www.webplanetcon.com"/>
    <meta property="fb:admins" content="https://www.facebook.com/WebplanetCon"/>
    <!-- Basic -->
    <title>@yield('title')</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

    {{HTML::style('css/bootstrap.css', ['media' => 'screen'])}}


            <!-- Revolution Banner CSS -->
    {{HTML::style('css/settings.css', ['media' => 'screen'])}}

            <!-- Vella CSS Styles  -->
    {{HTML::style('css/style.css', ['media' => 'screen'])}}

            <!-- Responsive CSS Styles  -->
    {{HTML::style('css/responsive.css', ['media' => 'screen'])}}

            <!-- Css3 Transitions Styles  -->
    {{HTML::style('css/animate.css', ['media' => 'screen'])}}

    {{HTML::style('css/datepicker.css', ['media' => 'screen'])}}

            <!-- Color CSS Styles  -->
    {{HTML::style('css/colors/blue.css', ['media' => 'screen', 'title' => 'blue'])}}
    {{HTML::style('css/colors/cyan.css', ['media' => 'screen', 'title' => 'cyan'])}}
    {{HTML::style('css/colors/green.css', ['media' => 'screen', 'title' => 'green'])}}
    {{HTML::style('css/colors/jade.css', ['media' => 'screen', 'title' => 'jade'])}}
    {{HTML::style('css/colors/orange.css', ['media' => 'screen', 'title' => 'orange'])}}
    {{HTML::style('css/colors/peach.css', ['media' => 'screen', 'title' => 'peach'])}}
    {{HTML::style('css/colors/pink.css', ['media' => 'screen', 'title' => 'pink'])}}
    {{HTML::style('css/colors/purple.css', ['media' => 'screen', 'title' => 'purple'])}}
    {{HTML::style('css/colors/red.css', ['media' => 'screen', 'title' => 'red'])}}
    {{HTML::style('css/colors/sky-blue.css', ['media' => 'screen', 'title' => 'sky-blue'])}}
    {{HTML::style('css/colors/yellow.css', ['media' => 'screen', 'title' => 'yellow'])}}


            <!-- Fontello Icons CSS Styles  -->
    {{HTML::style('css/fontello.css', ['media' => 'screen'])}}
    {{HTML::style('css/fontello-ie7.css', ['media' => 'screen'])}}

            <!-- Vella JS  -->
    {{HTML::script('js/jquery.min.js')}}
    {{HTML::script('js/jquery.migrate.js')}}
    {{HTML::script('js/modernizrr.js')}}
    {{HTML::script('js/bootstrap.js')}}
    {{HTML::script('js/jquery.fitvids.js')}}
    {{HTML::script('js/owl.carousel.min.js')}}
    {{HTML::script('js/nivo-lightbox.min.js')}}
    {{HTML::script('js/jquery.isotope.min.js')}}
    {{HTML::script('js/jquery.appear.js')}}
    {{HTML::script('js/count-to.js')}}
    {{HTML::script('js/jquery.textillate.js')}}
    {{HTML::script('js/jquery.lettering.js')}}
    {{HTML::script('js/jquery.easypiechart.min.js')}}
    {{HTML::script('js/jquery.nicescroll.min.js')}}
    {{HTML::script('js/jquery.parallax.js')}}
    {{HTML::script('js/mediaelement-and-player.js')}}
    {{HTML::script('js/jquery.themepunch.plugins.min.js')}}
    {{HTML::script('js/jquery.themepunch.revolution.min.js')}}
    {{HTML::script('js/script.js')}}
    {{HTML::script('js/jquery.parallax.js')}}

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="https://apis.google.com/js/platform.js" async defer></script>



    <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->


</head>

<body>

<!-- Container -->
<div id="container">
    <!-- Start Header -->
    <div class="hidden-header"></div>
    <header class="clearfix">

        <!-- Start Top Bar -->
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <!-- Start Contact Info -->
                        <ul class="contact-details">
                            <li><a href="#"><i class="icon-mobile-2"></i> +234 815 747 8047</a></li>
                            <li><a href="#"><i class="icon-mail-2"></i> info@webplanetcon.com</a></li>
                            <a href="{{URL::to('login')}}" class="btn-system border-btn btn-medium">Login/Register</a>
                        </ul>
                        <!-- End Contact Info -->
                    </div>
                    <div class="col-md-6">
                        <!-- Start Social Links -->
                        <ul class="social-list">
                            <li>
                                <a class="facebook sh-tooltip" data-placement="bottom" title="Facebook" href="https://www.facebook.com/WebplanetCon"><i class="icon-facebook-2"></i></a>
                            </li>
                            <li>
                                <a class="twitter sh-tooltip" data-placement="bottom" title="Twitter" href="https://twitter.com/WebplanetC"><i class="icon-twitter-2"></i></a>
                            </li>
                            <li>
                                <a class="google sh-tooltip" data-placement="bottom" title="Google Plus" href="https://plus.google.com/u/0/111669135886597931127"><i class="icon-gplus"></i></a>
                            </li>
                            <li>
                                <a class="linkdin sh-tooltip" data-placement="bottom" title="Linkedin" href="https://www.linkedin.com/company/webplanet-consulting-and-services"><i class="icon-linkedin"></i></a>
                            </li>
                            <li>
                                <a class="instgram sh-tooltip" data-placement="bottom" title="Instagram" href="http://instagram.com/webplanetconsulting?ref=badge"><i class="icon-instagramm"></i></a>
                            </li>
                            <li>
                                <a class="skype sh-tooltip" data-placement="bottom" title="Skype" href="skype:webplanet.consulting?call"><i class="icon-skype-2"></i></a>
                            </li>
                        </ul>
                        <!-- End Social Links -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Top Bar -->

        <!-- Start Header ( Logo & Naviagtion ) -->
        <div class="navbar navbar-default navbar-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- Stat Toggle Nav Link For Mobiles -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <i class="icon-menu-1"></i>
                    </button>
                    <!-- End Toggle Nav Link For Mobiles -->
                    <a class="navbar-brand" href="{{URL::to('/index')}}">{{HTML::image('images/vella.png', 'WPC Logo')}}</a>
                </div>
                <div class="navbar-collapse collapse">
                <!-- Stat Search -->
                <div class="search-side">
                    <a href="#" class="show-search"><i class="icon-search-1"></i></a>
                    <div class="search-form">
                        <form autocomplete="off" role="search" method="get" class="searchform" action="#">
                            <input type="text" value="" name="s" id="s" placeholder="Search the site...">
                        </form>
                    </div>
                </div>
                <!-- End Search -->
                <!-- Start Navigation List -->
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="active" href="{{URL::to('index')}}">Home</a>
                    </li>
                    <li>
                        <a href="{{URL::to('about')}}">Why US</a>
                    </li>
                    <li>
                        <a href="{{URL::to('services')}}">Services</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('project') }}">Projects</a>
                    </li>
                    <li>
                        <a href="{{URL::to('childict')}}">Child CS</a>
                    </li>
                    <li>
                        <a href="http://www.blog.webplanetcon.com" target="_blank">Blog</a>
                    </li>
                    <li><a href="{{URL::to('contactus')}}">Contact</a></li>
                </ul>
                <!-- End Navigation List -->
                </div>
            </div>
        </div>
        <!-- End Header ( Logo & Naviagtion ) -->
</header>


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-55339003-1', 'auto');
    ga('send', 'pageview');

</script>
    <!-- End Header -->

    @yield('banner')

    @yield('map')
    <!-- Start Home Slider -->
    <div id="slider">
        @yield('slider')
    </div>

    <!-- THE SCRIPT INITIALISATION -->
    <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
    <script type="text/javascript">
        var revapi;
        jQuery(document).ready(function() {
            revapi = jQuery('.fullwidthbanner').revolution({

                delay:9000,
                startwidth:1140,
                startheight:450,
                hideThumbs:200,

                thumbWidth:100,
                thumbHeight:50,
                thumbAmount:3,

                navigationType:"none",
                navigationArrows:"solo",
                navigationStyle:"round",

                touchenabled:"on",
                onHoverStop:"on",

                navigationHAlign:"center",
                navigationVAlign:"bottom",
                navigationHOffset:0,
                navigationVOffset:20,

                soloArrowLeftHalign:"left",
                soloArrowLeftValign:"center",
                soloArrowLeftHOffset:20,
                soloArrowLeftVOffset:0,

                soloArrowRightHalign:"right",
                soloArrowRightValign:"center",
                soloArrowRightHOffset:20,
                soloArrowRightVOffset:0,

                shadow:0,
                fullWidth:"on",
                fullScreen:"off",
                lazyLoad:"on",

                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,

                shuffle:"off",

                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                startWithSlide:0
            });
        });
    </script>

</div>
<!-- End Home Slider -->




<!-- Start Full Width Sections Content -->
<div id="content" class="full-sections">
    @yield('content')
</div>

<!-- End Full Width Sections Content -->


<!-- Start Footer -->
<?php
$startYear = 2012;
$thisYear = date('Y');
if ($thisYear > $startYear) {
    $thisYear - date('y');
    $copyright = "$startYear&ndash;$thisYear";
} else {
    $copyright = $startYear;
}
?>
<footer>
    <div class="container">
        <div class="row footer-widgets">

            <!-- Start Contact Widget -->
            <div class="col-md-3">
                <div class="footer-widget contact-widget">
                    <h4>Contact info<span class="head-line"></span></h4>
                    <ul>
                        <li><span>Lagos Address:</span> F89 Ikota Shopping Complex, VGC, Ajah, Lagos State, Nigeria.</li>
                        <li><span>Ibadan Address:</span> 15 Ike-Oluwa Plaza, Elewure Junction, Akala Express Road New Garage, Ibadan, Oyo State, Nigeria</li>
                        <li><span>Phone Number:</span> +234 081 6869 4999</li>
                        <li><span>Fax Number:</span> +234 081 6869 4999</li>
                        <li><span>Email:</span> info@webplanetcon.com</li>
                        <li><span>Website:</span> www.webplanetcon.com</li>
                    </ul>
                </div>
            </div>
            <!-- End Contact Widget -->

            <!-- Start Facebook Widget -->
            <div class="col-md-3">
                <div class="footer-widget flickr-widget">
                    <h4>Facebook Feed<span class="head-line"></span></h4>
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=1428285717423998&version=v2.0";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-like-box" data-href="https://www.facebook.com/WebplanetCon" data-width="250" data-height="250" data-colorscheme="dark" data-show-faces="true" data-header="true"></div>
                </div>
            </div>
            <!-- End Flickr Widget -->

            <!-- Start Twitter Widget -->
            <div class="col-md-3">
                <div class="footer-widget twitter-widget">
                    <h4>Twitter Feed<span class="head-line"></span></h4>
                    <a class="twitter-timeline"  href="https://twitter.com/WebplanetC"  data-widget-id="502742942566461440">Tweets by @WebplanetC</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
            </div>
            <!-- End Twitter Widget -->

            <!-- Start Subscribe & Social Links Widget -->
            <div class="col-md-3">
                <div class="footer-widget mail-subscribe-widget">
                    <h4>Get in touch<span class="head-line"></span></h4>
                    <p>Join our mailing list to stay up to date and get notices about our new releases!</p>
                    <form class="subscribe" form action="//webplanetcon.us9.list-manage.com/subscribe/post?u=e063d9e43456481f799d43d9a&amp;id=956a2a40e3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <input type="text"id="mce_Email" placeholder="email address">
                        <input type="submit" class="main-button" value="Send" name="subscribe" id="mc-embedded-subscribe" >
                    </form>


                </div>
                <div class="footer-widget social-widget">
                    <h4>Follow Us<span class="head-line"></span></h4>
                    <ul class="social-icons">
                        <li>
                            <a class="facebook" href="https://www.facebook.com/WebplanetCon"><i class="icon-facebook-2"></i></a>
                        </li>
                        <li>
                            <a class="twitter" href="https://twitter.com/WebplanetC"><i class="icon-twitter-2"></i></a>
                        </li>
                        <li>
                            <a class="google" href="https://plus.google.com/u/0/111669135886597931127"><i class="icon-gplus-1"></i></a>
                        </li>
                        <li>
                            <a class="linkdin" href="https://www.linkedin.com/company/webplanet-consulting-and-services"><i class="icon-linkedin-1"></i></a>
                        </li>
                        <li>
                            <a class="instgram" href="http://instagram.com/webplanetcon?ref=badge"><i class="icon-instagramm"></i></a>
                        </li>
                        <li>
                            <a class="skype" href="skype:webplanet.consulting?call"><i class="icon-skype-2"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Subscribe & Social Links Widget -->

        <!-- Start Copyright -->
        <div class="copyright-section">
            <div class="row">
                <div class="col-md-6">
                    <p><?php echo $copyright; ?> - All Rights Reserved</p>
                </div>
                <div class="col-md-6">
                    <ul class="footer-nav">
                        <li><a href="{{URL::to('careers')}}">Career</a></li>
                        <li><a href="{{URL::to('Faq')}}">FAQ</a></li>
                        <li><a href="{{URL::to('privacy')}}">Policy</a></li>
                        <li>{{ HTML::link('admin/users/teamlogin', 'Sign In') }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Copyright -->

    </div>
</footer>
<!-- End Footer -->

</div>
<!-- End Container -->

<!-- Go To Top Link -->
<a href="#" class="back-to-top"><i class="icon-up-open-1"></i></a>

<div id="loader">
    @yield('loader')
</div>

<!-- Style Switcher -->
<div class="switcher-box">
    <a href="#" class="open-switcher show-switcher"><i class="icon-cog-1"></i></a>
    <h4>Style Switcher</h4>
    <span>12 Predefined Color Skins</span>
    <ul class="colors-list">
        <li><a onClick="setActiveStyleSheet('blue'); return false;" title="Blue" class="blue"></a></li>
        <li><a onClick="setActiveStyleSheet('sky-blue'); return false;" title="Sky Blue" class="sky-blue"></a></li>
        <li><a onClick="setActiveStyleSheet('cyan'); return false;" title="Cyan" class="cyan"></a></li>
        <li><a onClick="setActiveStyleSheet('jade'); return false;" title="Jade" class="jade"></a></li>
        <li><a onClick="setActiveStyleSheet('green'); return false;" title="Green" class="green"></a></li>
        <li><a onClick="setActiveStyleSheet('purple'); return false;" title="Purple" class="purple"></a></li>
        <li><a onClick="setActiveStyleSheet('pink'); return false;" title="Pink" class="pink"></a></li>
        <li><a onClick="setActiveStyleSheet('red'); return false;" title="Red" class="red"></a></li>
        <li><a onClick="setActiveStyleSheet('orange'); return false;" title="Orange" class="orange"></a></li>
        <li><a onClick="setActiveStyleSheet('yellow'); return false;" title="Yellow" class="yellow"></a></li>
        <li><a onClick="setActiveStyleSheet('peach'); return false;" title="Peach" class="peach"></a></li>
        <li><a onClick="setActiveStyleSheet('beige'); return false;" title="Biege" class="beige"></a></li>
    </ul>
    <span>Top Bar Color</span>
    <select id="topbar-style" class="topbar-style">
        <option value="1">Light (Default)</option>
        <option value="2">Dark</option>
        <option value="3">Color</option>
    </select>
    <span>Layout Style</span>
    <select id="layout-style" class="layout-style">
        <option value="1">Wide</option>
        <option value="2">Boxed</option>
    </select>
    <span>Patterns for Boxed Version</span>
    <ul class="bg-list">
        <li><a href="#" class="bg1"></a></li>
        <li><a href="#" class="bg2"></a></li>
        <li><a href="#" class="bg3"></a></li>
        <li><a href="#" class="bg4"></a></li>
        <li><a href="#" class="bg5"></a></li>
        <li><a href="#" class="bg6"></a></li>
        <li><a href="#" class="bg7"></a></li>
        <li><a href="#" class="bg8"></a></li>
        <li><a href="#" class="bg9"></a></li>
        <li><a href="#" class="bg10"></a></li>
        <li><a href="#" class="bg11"></a></li>
        <li><a href="#" class="bg12"></a></li>
        <li><a href="#" class="bg13"></a></li>
        <li><a href="#" class="bg14"></a></li>
    </ul>
</div>

@yield('script')



{{HTML::script('js/bootstrap-datepicker.js')}}

<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>

<!-- Histats.com  START (hidden counter)-->
<script type="text/javascript">document.write(unescape("%3Cscript src=%27http://s10.histats.com/js15.js%27 type=%27text/javascript%27%3E%3C/script%3E"));</script>
<a href="http://www.histats.com/" target="_blank" title="" ><script  type="text/javascript" >
        try {Histats.start(1,2633271,4,0,0,0,"");
            Histats.track_hits();} catch(err){};
    </script></a>
<noscript><a href="http://www.histats.com/" target="_blank"><img  src="../../../sstatic1.histats.com/0266b.gif?2633271&amp;101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->


<!-- Pure Chat Snippet -->
<script type="text/javascript" data-cfasync="false">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: 'edd100ff-af15-46cf-8b03-0f1b6811a7d6', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->


</body>


</html>
