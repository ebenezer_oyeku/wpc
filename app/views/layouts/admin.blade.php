<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WPC-Admin | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    {{HTML::style('bower_resources/AdminLTE/bootstrap/css/bootstrap.min.css')}}
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    {{HTML::style('bower_resources/AdminLTE/dist/css/AdminLTE.min.css')}}
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    {{HTML::style('bower_resources/AdminLTE/dist/css/skins/_all-skins.min.css')}}
    <!-- iCheck -->
    {{HTML::style('bower_resources/AdminLTE/plugins/iCheck/flat/blue.css')}}
    <!-- Morris chart -->
    {{HTML::style('bower_resources/AdminLTE/plugins/morris/morris.css')}}
    <!-- jvectormap -->
    {{HTML::style('bower_resources/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}
    <!-- Date Picker -->
    {{HTML::style('bower_resources/AdminLTE/plugins/datepicker/datepicker3.css')}}
    <!-- Daterange picker -->
    {{HTML::style('bower_resources/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css')}}
    <!-- bootstrap wysihtml5 - text editor -->
    {{HTML::style('bower_resources/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('style')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="{{URL::to('index')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>W</b>PC</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b> WPC</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="{{URL::to('admin/users/signout')}}">
                            {{HTML::image(Auth::User()->image, 'User Image', array('class' => 'user-image'))}}
                            <span class="hidden-xs">Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    {{HTML::image(Auth::User()->image, 'User Image', array('class' => 'img-circle'))}}
                </div>
                <div class="pull-left info">
                    <p>{{Auth::User()->firstname, Auth::User()->lastname}}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active treeview">
                    <a href="/admin/dashboard">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/projects"><i class="fa fa-desktop"></i>Project</a></li>
                        <li><a href="/admin/clients"><i class="fa fa-globe"></i> Clients</a></li>
                        <li><a href="/admin/jobs"><i class="fa fa-group (alias)"></i>Jobs</a></li>
                        <li><a href="/admin/cstool"><i class="fa fa-money"></i>CS Tools</a></li>
                        <li><a href="/admin/users"><i class="fa fa-male"></i>Employees</a></li>
                        <li><a href="/admin/categories"><i class="fa fa-user"></i>Category</a></li>
                        <li><a href="/admin/subcategories"><i class="fa fa-users"></i>SubCategory</a></li>

                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    @yield('content')
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://webplanetcon.com">Webplanet Consulting & Services</a>.</strong> All rights reserved.
    </footer>

</div><!-- ./wrapper -->

@yield('script')
{{HTML::script('js/jquery.min.js')}}
<!-- jQuery 2.1.4 -->
{{HTML::script('bower_resources/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
{{HTML::script('bower_resources/AdminLTE/bootstrap/js/bootstrap.min.js')}}
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
{{HTML::script('bower_resources/AdminLTE/plugins/morris/morris.min.js')}}
<!-- Sparkline -->
{{HTML::script('bower_resources/AdminLTE/plugins/sparkline/jquery.sparkline.min.js')}}
<!-- jvectormap -->
{{HTML::script('bower_resources/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}
{{HTML::script('bower_resources/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}
<!-- jQuery Knob Chart -->
{{HTML::script('bower_resources/AdminLTE/plugins/knob/jquery.knob.js')}}
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
{{HTML::script('bower_resources/AdminLTE/plugins/daterangepicker/daterangepicker.js')}}
<!-- datepicker -->
{{HTML::script('bower_resources/AdminLTE/plugins/datepicker/bootstrap-datepicker.js')}}
<!-- Bootstrap WYSIHTML5 -->
{{HTML::script('bower_resources/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}
<!-- Slimscroll -->
{{HTML::script('bower_resources/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js')}}
<!-- FastClick -->
{{HTML::script('bower_resources/AdminLTE/plugins/fastclick/fastclick.min.js')}}
<!-- AdminLTE App -->
{{HTML::script('bower_resources/AdminLTE/dist/js/app.min.js')}}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{HTML::script('bower_resources/AdminLTE/dist/js/pages/dashboard.js')}}
<!-- AdminLTE for demo purposes -->
{{HTML::script('bower_resources/AdminLTE/dist/js/demo.js')}}
</body>
</html>
