@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                SubCategory
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">SubCategory</li>
            </ol>
            @if($errors->has())
                <div id="form-errors">
                    <p>The following errors have occurred:</p>

                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </section>

        <!-- Main content -->
        <section class="content">


            <!-- =========================================================== -->

            <div class="row">
                @foreach($subcategories as $subcategory)
                    <div class="col-md-4">
                        <div class="box box-success box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">{{$subcategory->name}}</h3>
                                <a href="{{ URL::to('admin/subcategories/' . $subcategory->id . '/edit') }}" class="btn btn-block btn-primary btn-xs">Edit Category</a>
                                <a href="{{ URL::to('admin/subcategories/' . $subcategory->id) }}" class="btn btn-block btn-success btn-xs">Show Category Details</a>
                                <br>
                                {{Form::open(array('url' =>'admin/subcategories/'.$subcategory->id, 'class'=>'form-inline')) }}
                                {{Form::hidden('_method', 'DELETE') }}
                                {{Form::submit('delete', array('class' => 'class="btn btn-block btn-danger btn-xs')) }}
                                {{Form::close() }}
                            </div><!-- /.box-header -->
                            {{--<div class="box-body">
                                The body of the box
                            </div><!-- /.box-body -->--}}
                        </div><!-- /.box -->
                    </div><!-- /.col -->
                @endforeach
            </div><!-- /.row -->
            {{$subcategories->links()}}

            <!-- =========================================================== -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create SubCategories Form</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        {{Form::open(array('url'=>'admin/subcategories', 'class'=>'form-horizontal')) }}
                        <div class="box-body">
                            <div class="form-group">
                                {{Form::label('name', 'SubCategory Name', array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Sub-Category Name')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('cat_id', 'Category Name', array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{Form::select('cat_id', $categories = array('default' => 'Please Select a Category') + $categories,'default', array('class' => 'form-control', 'placeholder' => 'Category Name')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            {{Form::reset('Clear Form', array('class'=>'btn btn-default'))}}
                            {{Form::submit('Create Category', array('class'=>'btn btn-info pull-right'))}}
                        </div><!-- /.box-footer -->
                        {{Form::close() }}
                    </div><!-- /.box -->
                </div>   <!-- /.row -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection