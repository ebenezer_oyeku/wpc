@extends('layouts.admin')

@section('style')
    {{--{{HTML::style('ccs/bootstrap.min.css')}}--}}
    {{HTML::style('css/fileinput.css', ['media' => 'all'])}}
    {{HTML::script('js/jquery.min.js')}}
    {{HTML::script('js/fileinput.js')}}
    {{--{{HTML::script('js/bootstrap.min.js')}}--}}



    <style>
        #imagePreview {
            width: 180px;
            height: 180px;
            background-position: center center;
            background-size: cover;
            -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
            display: inline-block;
        }
    </style>
@endsection

@section('content')
        <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Projects
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Create Project CP</a></li>
            </ol>
        </section>

        <section class="content">
            @if(Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <!-- USERS LIST -->
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Latest Projects</h3>
                            <div class="box-tools pull-right">
                                <span class="label label-danger">Project Lists</span>
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body no-padding">
                            <ul class="users-list clearfix">
                                @foreach($projects as $project)
                                    <li>
                                        <a href="{{ URL::to('admin/projects/' . $project->id . '/edit') }}" class="btn btn-block btn-primary btn-xs">Edit Client</a>
                                        <a href="{{ URL::to('admin/projects/' . $project->id) }}" class="btn btn-block btn-success btn-xs">Show Client Details</a>
                                        {{HTML::image($project->image, 'User Image', array('width'=>'50'))}}
                                        <a class="users-list-name" href="#">{{$project->pname}}</a>
                                        <span class="users-list-date">{{$project->created_at}}</span>
                                        {{Form::open(array('url' =>'admin/projects/destroy', 'class'=>'form-inline')) }}
                                        {{Form::hidden('id', $project->id) }}
                                        {{Form::submit('Delete Client', array('class' => 'btn btn-block btn-danger btn-xs')) }}
                                        {{Form::close() }}

                                        {{Form::open(array('url'=>'admin/projects/toggle-state', 'class'=>'form-inline'))}}
                                        {{Form::hidden('id', $project->id)}}
                                        {{Form::select('projState', array('0'=>'Confirmed', '1'=>'Started', '2'=>'InProgress', '3'=>'Completed'), $project->projState)}}
                                        {{Form::submit('Update STATUS', array('class' => 'btn btn-block btn-danger btn-xs'))}}
                                        {{Form::close() }}
                                    </li>
                                @endforeach
                            </ul><!-- /.users-list -->
                            {{$projects->links()}}
                        </div><!-- /.box-body -->
                        <div class="box-footer text-center">
                            <a href="javascript::" class="uppercase">View All Users</a>
                        </div><!-- /.box-footer -->
                    </div><!--/.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- Input addon -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create New Project</h3>
                        </div>

                        {{Form::open(array('url'=>'admin/projects', 'files' => true))}}
                        <div class="box-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <div id="imagePreview" class="img-thumbnail"></div>
                                        <div>
                                            <input id="uploadFile" type="file" name="image" class="img" />
                                            <label for="img">Post Image Upload</label>
                                            <p>"Include Snapshot of Your Project Here"</p>
                                            @if ($errors->has('image')) <p class="help-block">{{ $errors->first('image') }}</p> @endif
                                        </div>
                                    </div>
                                </div><!-- /.col-lg-6 -->

                            </div><!-- /.row -->
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Project Name</span>
                                {{Form::text('pname', null, array('class' => 'form-control', 'placeholder' => 'Your Project Name'))}}
                            </div>
                            @if ($errors->has('pname')) <p class="help-block">{{ $errors->first('pname') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Project Weblink</span>
                                {{Form::text('purl', null, array('class' => 'form-control', 'placeholder' => 'Your Project Link here'))}}
                            </div>
                            @if ($errors->has('purl')) <p class="help-block">{{ $errors->first('purl') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Project Details</span>
                                {{Form::textarea('details', null, array('class' => 'form-control', 'placeholder' => 'Your Project Details or Description Here'))}}
                            </div>
                            @if ($errors->has('details')) <p class="help-block">{{ $errors->first('details') }}</p> @endif
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                        {{Form::text('ExpStartDate', null, array('class' => 'form-control', 'id' => 'expstartdate','placeholder' => 'Expected Start Date'))}}
                                    </div><!-- /input-group -->
                                    @if ($errors->has('ExpStartDate')) <p class="help-block">{{ $errors->first('ExpStartDate') }}</p> @endif
                                </div><!-- /.col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        {{Form::text('ExpEndDate', null, array('class' => 'form-control', 'id' => 'expenddate','placeholder' => 'Expected End Date'))}}
                                    </div><!-- /input-group -->
                                    @if ($errors->has('ExpEndDate')) <p class="help-block">{{ $errors->first('ExpEndDate') }}</p> @endif
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                            <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </span>
                                        {{Form::text('ActStartDate', null, array('class' => 'form-control', 'id' => 'actstartdate','placeholder' => 'Actual Start Date'))}}
                                    </div><!-- /input-group -->
                                    @if ($errors->has('ActStartDate')) <p class="help-block">{{ $errors->first('ActStartDate') }}</p> @endif
                                </div><!-- /.col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        {{Form::text('ActEndDate', null, array('class' => 'form-control', 'id' => 'actenddate','placeholder' => 'Actual End Date'))}}
                                    </div><!-- /input-group -->
                                    @if ($errors->has('ActEndDate')) <p class="help-block">{{ $errors->first('ActEndDate') }}</p> @endif
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                            <span class="input-group-addon">
                              <i class="fa fa-check-square-o"></i>
                            </span>
                                        {{Form::select('cat_id', $categories = array('default' => 'Please Select a Category') + $categories,'default', array('id' => 'category','class' => 'form-control', 'placeholder'=>'Choose a Category'))}}
                                    </div><!-- /input-group -->
                                    @if ($errors->has('cat_id')) <p class="help-block">{{ $errors->first('cat_id') }}</p> @endif
                                </div><!-- /.col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
                                        {{Form::select('subcat_id', array( 'default' => 'Please Select a SubCategory'),'default', array( 'id'=>'subcategory', 'class' => 'form-control'))}}
                                    </div><!-- /input-group -->
                                    @if ($errors->has('subcat_id')) <p class="help-block">{{ $errors->first('subcat_id') }}</p> @endif
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->
                            <br>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                            <span class="input-group-addon">
                              <i class="fa fa-check-square-o"></i>
                            </span>
                                        {{Form::select('client_id', $clients = array('default' => 'Please Select a Client') + $clients,'default', array('id' => 'category','class' => 'form-control', 'placeholder'=>'Choose a Category'))}}
                                    </div><!-- /input-group -->
                                    @if ($errors->has('client_id')) <p class="help-block">{{ $errors->first('client_id') }}</p> @endif
                                </div><!-- /.col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
                                        {{Form::select('AdministratorID', $employees = array( 'default' => 'Please Select a Project Leader') + $employees, 'default', array( 'id'=>'subcategory', 'class' => 'form-control'))}}
                                    </div><!-- /input-group -->
                                    @if ($errors->has('AdministratorID')) <p class="help-block">{{ $errors->first('AdministratorID') }}</p> @endif
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->
                            <br>
                            {{Form::submit('Create Project', array('class'=>'btn btn-block btn-success btn-flat'))}}
                            {{Form::close() }}
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!--/.col (left) -->
            </div>
        </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('script')


    <script>
        $("#input-ficons-2").fileinput({
            uploadUrl: "/file-upload-batch/2",
            uploadAsync: false,
            previewFileIcon: '<i class="fa fa-file"></i>',
            allowedPreviewTypes: null, // set to empty, null or false to disable preview for all types
            previewFileIconSettings: {
                'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'htm': '<i class="fa fa-file-code-o text-info"></i>',
                'txt': '<i class="fa fa-file-text-o text-info"></i>',
                'mov': '<i class="fa fa-file-movie-o text-warning"></i>',
                'mp3': '<i class="fa fa-file-audio-o text-warning"></i>',
            },
            previewFileExtSettings: {
                'doc': function(ext) {
                    return ext.match(/(doc|docx)$/i);
                },
                'xls': function(ext) {
                    return ext.match(/(xls|xlsx)$/i);
                },
                'ppt': function(ext) {
                    return ext.match(/(ppt|pptx)$/i);
                },
                'zip': function(ext) {
                    return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                },
                'htm': function(ext) {
                    return ext.match(/(php|js|css|htm|html)$/i);
                },
                'txt': function(ext) {
                    return ext.match(/(txt|ini|md)$/i);
                },
                'mov': function(ext) {
                    return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
                },
                'mp3': function(ext) {
                    return ext.match(/(mp3|wav)$/i);
                },
            }
        });
    </script>

    <script type="text/javascript">
        $(function() {
            $("#uploadFile").on("change", function()
            {
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file

                    reader.onloadend = function(){ // set image data as background of div
                        $("#imagePreview").css("background-image", "url("+this.result+")");
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $('#expstartdate').datepicker({
                format: "yyyy/mm/dd/"
            });
            $('#expenddate').datepicker({
                format: "yyyy/mm/dd/"
            });
            $('#actstartdate').datepicker({
                format: "yyyy/mm/dd/"
            });
            $('#actenddate').datepicker({
                format: "yyyy/mm/dd/"
            });

        });
    </script>

    <script>

        $('#category').on('change',function(e){
            console.log(e);

            var cat_id = e.target.value;

            //ajax
            $.get('/ajax-subcat?cat_id=' + cat_id, function(data){
                //success data
                $('#subcategory').empty();
                $.each(data, function(index, subcatObj){

                    $('#subcategory').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
                });
            });
        });

    </script>
@endsection
