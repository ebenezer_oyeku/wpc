@extends('layouts.admin')

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Show Clients
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">{{$project->name}}</a></li>
            {{--<li class="active">{{$client->name}}</li>--}}
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        {{HTML::image($project->image, 'User profile picture', array('class'=>'profile-user-img img-responsive img-circle'))}}
                        <h3 class="profile-username text-center">{{$project->pname}}</h3>
                        <p class="text-muted text-center">{{$project->created_at}}</p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Expected Start Date</b> <a class="pull-right">{{$project->ExpStartDate}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Expected End Date</b> <a class="pull-right">{{$project->ExpEndDate}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Actual Start Date</b> <a class="pull-right">{{$project->ActStartDate}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Actual End Date</b> <a class="pull-right">{{$project->ActEndDate}}</a>
                            </li>
                        </ul>

                        <a href="{{ URL::to('admin/projects/' . $project->id . '/edit') }}" class="btn btn-primary btn-block"><b>Edit Client</b></a>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->


            </div><!-- /.col -->
            <div class="col-md-9">
                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">About {{$project->pname}}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <strong><i class="fa fa-book margin-r-5"></i>Project Details</strong>
                        <p class="text-muted">
                            {{$project->details}}
                        </p>

                        <hr>

                        <strong><i class="fa fa-map-marker margin-r-5"></i>Client</strong>
                        <p class="text-muted">{{$project->client->name}}</p>

                        <hr>

                        <strong><i class="fa fa-pencil margin-r-5"></i> Project Category And Sub-Category</strong>
                        <p>
                            <span class="label label-danger">{{$project->category->name}}</span>
                            <span class="label label-success">{{$project->subcategory->name}}</span>
                            {{--<span class="label label-info">{{$client->country}}</span>--}}
                        </p>

                        <hr>

                        <strong><i class="fa fa-file-text-o margin-r-5"></i>Assign Project Admin</strong>
                        <p>{{$project->administrator->lastname}}.</p>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection