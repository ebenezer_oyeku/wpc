@extends('layouts.admin')

@section('style')
    <style>
        #imagePreview {
            width: 180px;
            height: 180px;
            background-position: center center;
            background-size: cover;
            -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
            display: inline-block;
        }
    </style>
@endsection


@section('content')
        <div class="content-wrapper">
            @if(Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Clients Form
                    <small>Control Panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Clients Form</a></li>
                </ol>
            </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- USERS LIST -->
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Latest Clients</h3>
                            <div class="box-tools pull-right">
                                <span class="label label-danger">Clients</span>
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body no-padding">
                            <ul class="users-list clearfix">
                                @foreach($clients as $client)
                                    <li>
                                        <a href="{{ URL::to('admin/clients/' . $client->id . '/edit') }}" class="btn btn-block btn-primary btn-xs">Edit Client</a>
                                    <a href="{{ URL::to('admin/clients/' . $client->id) }}" class="btn btn-block btn-success btn-xs">Show Client Details</a>
                                        {{HTML::image($client->image, 'User Image', array('width'=>'50'))}}
                                        <a class="users-list-name" href="#">{{$client->pname}}</a>
                                        <span class="users-list-date">{{$client->created_at}}</span>
                                        {{Form::open(array('url' =>'admin/clients/'.$client->id, 'class'=>'form-inline')) }}
                                        {{Form::hidden('_method', 'DELETE') }}
                                        {{Form::submit('Delete Client', array('class' => 'btn btn-block btn-danger btn-xs')) }}
                                        {{Form::close() }}
                                    </li>
                                @endforeach
                            </ul><!-- /.users-list -->
                        </div><!-- /.box-body -->
                        <div class="box-footer text-center">
                            <a href="javascript::" class="uppercase">View All Users</a>
                        </div><!-- /.box-footer -->
                    </div><!--/.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- Input addon -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create New Client</h3>
                        </div>
                        {{Form::open(array('url'=>'admin/clients', 'files' => true))}}
                        <div class="box-body">
                            <div class="input-group">
                                <span class="input-group-addon">Name/Company Name</span>
                                {{Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Your Name or Company Name'))}}
                            </div>
                            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Clients URL</span>
                                {{Form::text('weblink', null, array('class' => 'form-control', 'placeholder' => 'Clients URL'))}}
                            </div>
                            @if ($errors->has('weblink')) <p class="help-block">{{ $errors->first('weblink') }}</p> @endif
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <div id="imagePreview" class="img-thumbnail"></div>
                                        <div>
                                            <input id="uploadFile" type="file" name="image" class="img" />
                                            <label for="img">Post Image Upload</label>
                                            <p>"Include Snapshot of Your Project Here"</p>
                                            @if ($errors->has('image')) <p class="help-block">{{ $errors->first('image') }}</p> @endif
                                        </div>
                                    </div>
                                </div><!-- /.col-lg-6 -->

                            </div><!-- /.row -->
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Contact Address</span>
                                {{Form::text('address', null, array('class' => 'form-control', 'placeholder' => 'Your Contact Address or Company Address'))}}
                            </div>
                            @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                {{Form::text('contact1', null, array('class' => 'form-control', 'placeholder' => 'Your Contact Number'))}}
                            </div>
                            @if ($errors->has('contact1')) <p class="help-block">{{ $errors->first('contact1') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                {{Form::text('contact2', null, array('class' => 'form-control', 'placeholder' => 'Alternative Contact Number'))}}
                            </div>
                            @if ($errors->has('contact2')) <p class="help-block">{{ $errors->first('contact2') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                {{Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Your Email Address'))}}
                            </div>
                            @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                {{Form::text('facebookid', null, array('class' => 'form-control', 'placeholder' => 'Your Facebook link/Page or Username'))}}
                            </div>
                            @if ($errors->has('facebookid')) <p class="help-block">{{ $errors->first('facebookid') }}</p> @endif
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                            <span class="input-group-addon">
                              <i class="fa fa-twitter"></i>
                            </span>
                                        {{Form::text('twitterid', null, array('class' => 'form-control', 'placeholder' => 'Your Twitter Handle'))}}
                                        @if ($errors->has('twitterid')) <p class="help-block">{{ $errors->first('twitterid') }}</p> @endif
                                    </div><!-- /input-group -->
                                </div><!-- /.col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-skype"></i></span>
                                        {{Form::text('skypeid', null, array('class' => 'form-control', 'placeholder' => 'Your Skype-ID'))}}
                                        @if ($errors->has('skypeid')) <p class="help-block">{{ $errors->first('skypeid') }}</p> @endif
                                    </div><!-- /input-group -->
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->
                            <br>
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-danger">Country</button>
                                </div><!-- /btn-group -->
                                {{Form::select('country', array('key' => 'value' ), 'default', array('id' => 'country','class' => 'form-control'))}}
                                @if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
                            </div><!-- /input-group -->
                            <br>
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-danger">State</button>
                                </div><!-- /btn-group -->
                                {{Form::select('state', array('placeholder' => 'Select State' ), 'default', array('id' => 'state','class' => 'form-control'))}}
                                @if ($errors->has('state')) <p class="help-block">{{ $errors->first('state') }}</p> @endif
                            </div>
                            <br>
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-danger">City</button>
                                </div><!-- /btn-group -->
                                {{Form::text('city', null, [ 'class' => 'form-control', 'placeholder'=>'Enter Your City'])}}
                                @if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
                            </div>
                            <br>
                            {{Form::submit('Create Clients', array('class'=>'btn btn-block btn-success btn-flat'))}}
                            {{Form::close() }}
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!--/.col (left) -->
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('script')
    {{HTML::script('js/jquery-1.8.0.min.js')}}
    {{HTML::script('js/countries.js')}}

    <script type="text/javascript">
        $(function() {
            $("#uploadFile").on("change", function()
            {
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file

                    reader.onloadend = function(){ // set image data as background of div
                        $("#imagePreview").css("background-image", "url("+this.result+")");
                    }
                }
            });
        });
    </script>
    <script language="javascript">
        populateCountries("country", "state"); // first parameter is id of country drop-down and second parameter is id of state drop-down
        populateCountries("country2");
        populateCountries("country2");
    </script>
    @endsection
