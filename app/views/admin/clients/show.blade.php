@extends('layouts.admin')

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Show Clients
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            {{--<li class="active">{{$client->name}}</li>--}}
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        {{--{{HTML::image($client->image, 'User profile picture', array('class'=>'profile-user-img img-responsive img-circle'))}}--}}
                        {{--<img class="profile-user-img img-responsive img-circle" src="{{HTML::image($client->image)}}" alt="User profile picture">--}}
                        <h3 class="profile-username text-center">{{$client->name}}</h3>
                        <p class="text-muted text-center">{{$client->email}}</p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Skype ID</b> <a class="pull-right">{{$client->skypeid}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Facebook ID</b> <a class="pull-right">{{$client->facebookid}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Twitter ID</b> <a class="pull-right">{{$client->twitterid}}</a>
                            </li>
                        </ul>

                        <a href="{{ URL::to('admin/clients/' . $client->id . '/edit')}}" class="btn btn-primary btn-block"><b>Edit Client</b></a>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->


            </div><!-- /.col -->
            <div class="col-md-9">
                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">About {{$client->name}}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <strong><i class="fa fa-book margin-r-5"></i>  Address</strong>
                        <p class="text-muted">
                            {{$client->Address}}
                        </p>

                        <hr>

                        <strong><i class="fa fa-map-marker margin-r-5"></i> Contact</strong>
                        <p class="text-muted">{{$client->contact1}}, {{$client->contact2}}</p>

                        <hr>

                        <strong><i class="fa fa-pencil margin-r-5"></i> Location</strong>
                        <p>
                            <span class="label label-danger">{{$client->city}}</span>
                            <span class="label label-success">{{$client->state}}</span>
                            <span class="label label-info">{{$client->country}}</span>
                        </p>

                        <hr>

                        <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection