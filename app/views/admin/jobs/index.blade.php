@extends('layouts.admin')

@section('style')
{{HTML::script('js/jquery-1.8.0.min.js')}}
{{HTML::style('css/datepicker.css')}}
@endsection

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Job Opening
            <small>Control Panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Create Job Openings</a></li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('message'))
            <div class="alert alert-success">{{ Session::get('message') }}</div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <!-- USERS LIST -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Jobs</h3>
                        <div class="box-tools pull-right">
                            <span class="label label-danger">Jobs Available</span>
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                        <ul class="users-list clearfix">
                            @foreach($jobs as $job)
                                <li>
                                    <a href="{{ URL::to('admin/jobs/' . $job->id . '/edit') }}" class="btn btn-block btn-primary btn-xs">Edit Client</a>
                                    <a href="{{ URL::to('admin/jobs/' . $job->id) }}" class="btn btn-block btn-success btn-xs">Show Client Details</a>
                                    {{HTML::image($job->image, 'User Image', array('width'=>'50'))}}
                                    <a class="users-list-name" href="#">{{$job->name}}</a>
                                    <span class="users-list-date">{{$job->created_at}}</span>
                                    {{Form::open(array('url' =>'admin/jobs/'.$job->id, 'class'=>'form-inline')) }}
                                    {{Form::hidden('_method', 'DELETE') }}
                                    {{Form::submit('Delete Job Opening', array('class' => 'btn btn-block btn-danger btn-xs')) }}
                                    {{Form::close() }}
                                </li>
                            @endforeach
                        </ul><!-- /.users-list -->
                    </div><!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="javascript::" class="uppercase">View All Opening</a>
                    </div><!-- /.box-footer -->
                </div><!--/.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- Input addon -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create New Job Opening Details</h3>
                    </div>
                    {{Form::open(array('url'=>'admin/jobs', 'files' => true))}}
                    <div class="box-body">
                        <div class="input-group">
                            <span class="input-group-addon">Name</span>
                            {{Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Job Name'))}}
                        </div>
                        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">Openings</span>
                            {{Form::text('openings', null, array('class' => 'form-control', 'placeholder' => 'Number of Opening'))}}
                        </div>
                        @if ($errors->has('openings')) <p class="help-block">{{ $errors->first('openings') }}</p> @endif
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <div id="imagePreview" class="img-thumbnail"></div>
                                    <div>
                                        <input id="uploadFile" type="file" name="image" class="img" />
                                        <label for="img">Post Image Upload</label>
                                        <p>"Include Snapshot of Your Project Here"</p>
                                        @if ($errors->has('image')) <p class="help-block">{{ $errors->first('image') }}</p> @endif
                                    </div>
                                </div>
                            </div><!-- /.col-lg-6 -->

                        </div><!-- /.row -->
                        <br>
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Job Summary <small>Meta Description Of Job</small></h3>
                                <!-- tools box -->
                                <div class="pull-right box-tools">
                                    <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div><!-- /. tools -->
                            </div><!-- /.box-header -->
                            <div class="box-body pad">
                                {{Form::textarea('summary', null, array('class' => 'textarea', 'placeholder' => 'Description of Jobs', 'style'=>'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;'))}}
                            </div>
                            @if ($errors->has('summary')) <p class="help-block">{{ $errors->first('summary') }}</p> @endif
                        </div>
                        <br>
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Job Description <small>Full Job Details</small></h3>
                                <!-- tools box -->
                                <div class="pull-right box-tools">
                                    <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div><!-- /. tools -->
                            </div><!-- /.box-header -->
                            <div class="box-body pad">
                                    {{Form::textarea('description', null, array('class' => 'textarea', 'placeholder' => 'Description of Jobs', 'style'=>'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;'))}}
                            </div>
                            @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">Job Opening End-Date</span>
                            {{Form::text('end_date', null, array('class' => 'form-control', 'id' => 'enddate', 'placeholder' => 'Uses of Tools'))}}
                        </div>
                        @if ($errors->has('end_date')) <p class="help-block">{{ $errors->first('end_date') }}</p> @endif
                        <br>
                        {{Form::submit('Create Job Openings', array('class'=>'btn btn-block btn-success btn-flat'))}}
                        {{Form::close() }}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!--/.col (left) -->
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('script')
    <style>
        #imagePreview {
            width: 180px;
            height: 180px;
            background-position: center center;
            background-size: cover;
            -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
            display: inline-block;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $("#uploadFile").on("change", function()
            {
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file

                    reader.onloadend = function(){ // set image data as background of div
                        $("#imagePreview").css("background-image", "url("+this.result+")");
                    }
                }
            });
        });
    </script>
    {{HTML::script('js/jquery-1.9.1.min.js')}}
    {{HTML::script('js/bootstrap-datepicker.js')}}

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            $('#enddate').datepicker({
                format: "mm/dd/yyyy"
            });

        });
    </script>
@endsection
