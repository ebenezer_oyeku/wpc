@extends('layouts.admin')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Category
                <small>Edit Panel page</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Category</li>
            </ol>
            @if($errors->has())
                <div id="form-errors">
                    <p>The following errors have occurred:</p>

                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </section>

        <!-- Main content -->
        <section class="content">

                    <!-- =========================================================== -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create Categories Form</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        {{Form::model($category, array('action'=> array('CategoriesController@update', $category->id), 'method'=> 'PUT'))}}
                        <div class="box-body">
                            <div class="form-group">
                                {{Form::label('name', 'Category Name', array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Category Name')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            {{Form::reset('Clear Form', array('class'=>'btn btn-default'))}}
                            {{Form::submit('Create Category', array('class'=>'btn btn-info pull-right'))}}
                        </div><!-- /.box-footer -->
                        {{Form::close() }}
                    </div><!-- /.box -->
                </div>   <!-- /.row -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection