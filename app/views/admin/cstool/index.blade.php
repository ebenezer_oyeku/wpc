@extends('layouts.admin')

@section('style')
{{HTML::script('js/jquery-1.8.0.min.js')}}
{{HTML::style('css/datepicker.css')}}
@endsection

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            CS Tool
            <small>Control Panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Create CS Tools</a></li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('message'))
            <div class="alert alert-success">{{ Session::get('message') }}</div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <!-- USERS LIST -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Tools</h3>
                        <div class="box-tools pull-right">
                            <span class="label label-danger">Tools Available</span>
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                        <ul class="users-list clearfix">
                            @foreach($cstools as $cstool)
                                <li>
                                    <a href="{{ URL::to('admin/cstool/' . $cstool->id . '/edit') }}" class="btn btn-block btn-primary btn-xs">Edit Client</a>
                                    <a href="{{ URL::to('admin/cstool/' . $cstool->id) }}" class="btn btn-block btn-success btn-xs">Show Client Details</a>
                                    {{HTML::image($cstool->image, 'User Image', array('width'=>'50'))}}
                                    <a class="users-list-name" href="#">{{$cstool->name}}</a>
                                    <span class="users-list-date">{{$cstool->created_at}}</span>
                                    {{Form::open(array('url' =>'admin/cstool/'.$cstool->id, 'class'=>'form-inline')) }}
                                    {{Form::hidden('_method', 'DELETE') }}
                                    {{Form::submit('Delete CS-Tool', array('class' => 'btn btn-block btn-danger btn-xs')) }}
                                    {{Form::close() }}
                                </li>
                            @endforeach
                        </ul><!-- /.users-list -->
                        {{$cstools->links()}}
                    </div><!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="javascript::" class="uppercase">View All Users</a>
                    </div><!-- /.box-footer -->
                </div><!--/.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- Input addon -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create New CS Tool Details</h3>
                    </div>
                    {{Form::open(array('url'=>'admin/cstool', 'files' => true))}}
                    <div class="box-body">
                        <div class="input-group">
                            <span class="input-group-addon">Name</span>
                            {{Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Tool Name'))}}
                        </div>
                        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">Weblink</span>
                            {{Form::text('weblink', null, array('class' => 'form-control', 'placeholder' => 'Weblink Address'))}}
                        </div>
                        @if ($errors->has('weblink')) <p class="help-block">{{ $errors->first('weblink') }}</p> @endif
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <div id="imagePreview" class="img-thumbnail"></div>
                                    <div>
                                        <input id="uploadFile" type="file" name="image" class="img" />
                                        <label for="img">Post Image Upload</label>
                                        <p>"Include Snapshot of Your Project Here"</p>
                                        @if ($errors->has('image')) <p class="help-block">{{ $errors->first('image') }}</p> @endif
                                    </div>
                                </div>
                            </div><!-- /.col-lg-6 -->

                        </div><!-- /.row -->
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">Details</span>
                            {{Form::textarea('details', null, array('class' => 'form-control', 'placeholder' => 'Description of Tool'))}}
                        </div>
                        @if ($errors->has('details')) <p class="help-block">{{ $errors->first('details') }}</p> @endif
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">Tool Functions</span>
                            {{Form::text('tool_function', null, array('class' => 'form-control', 'placeholder' => 'Uses of Tools'))}}
                        </div>
                        @if ($errors->has('tool_function')) <p class="help-block">{{ $errors->first('tool_function') }}</p> @endif
                        <br>
                        {{Form::submit('Create CS Tools', array('class'=>'btn btn-block btn-success btn-flat'))}}
                        {{Form::close() }}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!--/.col (left) -->
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('script')
    <style>
        #imagePreview {
            width: 180px;
            height: 180px;
            background-position: center center;
            background-size: cover;
            -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
            display: inline-block;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $("#uploadFile").on("change", function()
            {
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file

                    reader.onloadend = function(){ // set image data as background of div
                        $("#imagePreview").css("background-image", "url("+this.result+")");
                    }
                }
            });
        });
    </script>
    {{HTML::script('js/jquery-1.9.1.min.js')}}
    {{HTML::script('js/bootstrap-datepicker.js')}}

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            $('#example1').datepicker({
                format: "mm/dd/yyyy"
            });

        });
    </script>
@endsection
