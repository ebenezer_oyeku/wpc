@extends('layouts.admin')

@section('style')
    <style>
        #imagePreview {
            width: 180px;
            height: 180px;
            background-position: center center;
            background-size: cover;
            -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
            display: inline-block;
        }
    </style>
@endsection


@section('content')
    <div class="content-wrapper">
        @if(Session::has('message'))
            <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
                    <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Edit CS-Programming Tool
                    <small>Control Panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">{{$cstool->name}}</a></li>
                </ol>
            </section>

            <section class="content">

                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- Input addon -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Edit Client</h3>
                            </div>
                            {{--{{ Form::model($nerd, array('action' => array('NerdController@update', $nerd->id), 'method' => 'PUT')) }}--}}
                            {{Form::model($cstool, array('action'=> array('CsToolsController@update', $cstool->id), 'method'=> 'PUT'))}}
                            <div class="box-body">
                                <div class="input-group">
                                    <span class="input-group-addon">Name</span>
                                    {{Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Tool Name'))}}
                                </div>
                                @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon">Weblink</span>
                                    {{Form::text('weblink', null, array('class' => 'form-control', 'placeholder' => 'Weblink Address'))}}
                                </div>
                                @if ($errors->has('weblink')) <p class="help-block">{{ $errors->first('weblink') }}</p> @endif
                                <br>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <div id="imagePreview" class="img-thumbnail"></div>
                                            <div>
                                                <input id="uploadFile" type="file" name="image" class="img" />
                                                <label for="img">Post Image Upload</label>
                                                <p>"Include Snapshot of Your Project Here"</p>
                                                @if ($errors->has('image')) <p class="help-block">{{ $errors->first('image') }}</p> @endif
                                            </div>
                                        </div>
                                    </div><!-- /.col-lg-6 -->

                                </div><!-- /.row -->
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon">Details</span>
                                    {{Form::textarea('details', null, array('class' => 'form-control', 'placeholder' => 'Description of Tool'))}}
                                </div>
                                @if ($errors->has('details')) <p class="help-block">{{ $errors->first('details') }}</p> @endif
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon">Tool Functions</span>
                                    {{Form::text('tool_function', null, array('class' => 'form-control', 'placeholder' => 'Uses of Tools'))}}
                                </div>
                                @if ($errors->has('tool_function')) <p class="help-block">{{ $errors->first('tool_function') }}</p> @endif
                                <br>
                                {{Form::submit('Edit Clients', array('class'=>'btn btn-block btn-success btn-flat'))}}
                                {{Form::close() }}
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!--/.col (left) -->
                </div>
            </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('script')
    {{HTML::script('js/jquery-1.8.0.min.js')}}
    {{HTML::script('js/countries.js')}}

    <script type="text/javascript">
        $(function() {
            $("#uploadFile").on("change", function()
            {
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file

                    reader.onloadend = function(){ // set image data as background of div
                        $("#imagePreview").css("background-image", "url("+this.result+")");
                    }
                }
            });
        });
    </script>
    <script language="javascript">
        populateCountries("country", "state"); // first parameter is id of country drop-down and second parameter is id of state drop-down
        populateCountries("country2");
        populateCountries("country2");
    </script>
@endsection
