@extends('layouts.admin')

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Show Clients
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">{{$cstool->name}}</a></li>
            {{--<li class="active">{{$client->name}}</li>--}}
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        {{HTML::image($cstool->image, 'User profile picture', array('class'=>'profile-user-img img-responsive img-circle'))}}
                        <h3 class="profile-username text-center">{{$cstool->name}}</h3>
                        <p class="text-muted text-center">{{$cstool->email}}</p>

                        {{--<ul class="list-group list-group-unbordered">--}}
                            {{--<li class="list-group-item">--}}
                                {{--<b>Skype ID</b> <a class="pull-right">{{$cstool->skypeid}}</a>--}}
                            {{--</li>--}}
                            {{--<li class="list-group-item">--}}
                                {{--<b>Facebook ID</b> <a class="pull-right">{{$cstool->facebookid}}</a>--}}
                            {{--</li>--}}
                            {{--<li class="list-group-item">--}}
                                {{--<b>Twitter ID</b> <a class="pull-right">{{$cstool->twitterid}}</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}

                        <a href="{{ URL::to('admin/cstool/' . $cstool->id . '/edit') }}" class="btn btn-primary btn-block"><b>Edit Client</b></a>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->


            </div><!-- /.col -->
            <div class="col-md-9">
                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">About {{$cstool->name}}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <strong><i class="fa fa-book margin-r-5"></i> Tool Description</strong>
                        <p class="text-muted">
                            {{$cstool->details}}
                        </p>

                        <hr>

                        {{--<strong><i class="fa fa-map-marker margin-r-5"></i> Contact</strong>--}}
                        {{--<p class="text-muted">{{$client->contact1}}, {{$client->contact2}}</p>--}}

                        {{--<hr>--}}

                        {{--<strong><i class="fa fa-pencil margin-r-5"></i> Location</strong>--}}
                        {{--<p>--}}
                            {{--<span class="label label-danger">{{$client->city}}</span>--}}
                            {{--<span class="label label-success">{{$client->state}}</span>--}}
                            {{--<span class="label label-info">{{$client->country}}</span>--}}
                        {{--</p>--}}

                        {{--<hr>--}}

                        <strong><i class="fa fa-file-text-o margin-r-5"></i> Tool Functions</strong>
                        <p>{{$cstool->tool_function}}</p>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection