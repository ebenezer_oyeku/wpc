@extends('layouts.default')

@section('title')
    Our Expertise.
@stop



@section('banner')
    <div class="page-banner" style="padding:40px 0; background: url(images/slide-02-bg.jpg) center #f9f9f9;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Services</h2>
                    <p>We Are Professional</p>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="{{URL::to('index')}}">Home</a></li>
                        <li>Services</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @stop

@section('content')
    <div id="content">
        <div class="container">
            <div class="page-content">


                <div class="row">

                    <!-- Start Image Service Box 1 -->
                    <div class="col-md-4 image-service-box">
                        <img class="img-thumbnail" src="images/database.jpg" alt="" />
                        <h4>Data-Center Support</h4>
                        <p>Database Analysis and Design, Database Access Services, Data Mining and Analyzing Solutions, Data Warehouse/Business Intelligence Development</p>
                    </div>
                    <!-- End Image Service Box 1 -->

                    <!-- Start Image Service Box 2 -->
                    <div class="col-md-4 image-service-box">
                        <img class="img-thumbnail" src="images/global.jpg" alt="" />
                        <h4>Globalise Freelance Projects Agency </h4>
                        <p>Working all Type of Freelance projects such as Accounting, Designing, Adminstrative Support, IT Support, Property Sales and So on.</p>
                    </div>
                    <!-- End Image Service Box 2 -->

                    <!-- Start Image Service Box 3 -->
                    <div class="col-md-4 image-service-box">
                        <img class="img-thumbnail" src="images/softdev.jpg" alt="" />
                        <h4>Software/Hardware Engineering and Development</h4>
                        <p>Software Information System, Software Analysis and Design, Hardware Installations, Maintainance and Configurations</p>
                    </div>
                    <!-- End Image Service Box 3 -->
                    <div class="col-md-4 image-service-box">
                        <img class="img-thumbnail" src="images/consult.jpg" alt="" />
                        <h4>Business Management and Consultancy</h4>
                        <p>IT Business Management Application Specification,Management and Support. IT Procurement Management Strategy – Development and Review, Project Management,Business Continuity Planning</p>
                    </div>

                    <div class="col-md-4 image-service-box">
                        <img class="img-thumbnail" src="images/web.jpg" alt="" />
                        <h4>Application Development (Web/Mobile/Standalone)</h4>
                        <p>Web Design & Ecommerce with ASP.Net, PHP, Drupal, Joomal, HTML, HTML5 with Technolgy like AJAX, JQuery. And Graphic designing in Photoshop and Gimp</p>
                    </div>

                    <div class="col-md-4 image-service-box">
                        <img class="img-thumbnail" src="images/legal.jpg" alt="" />
                        <h4>Legal Department</h4>
                        <p>Perfection of Legal Documents, Sales of Properties, Business Registration Processes and Processing of C 0f O or Governor Consent.</p>
                    </div>

                </div>

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:45px;"></div>

                <div class="row">

                    <div class="col-md-4">

                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>We Aare Awesome</span></h4>

                        <!-- Accordion -->
                        <div class="panel-group" id="accordion">

                            <!-- Start Accordion 1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-one">
                                            <i class="icon-down-open-1 control-icon"></i>
                                            <i class="icon-laptop-1"></i> Great Customer Service
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse-one" class="panel-collapse collapse in">
                                    <div class="panel-body">Our Clients are very important to us at WPC. That is why we put their Satisfaction on Our priority List in Our Firm. Our goal is to always make our CLIENTS smile due to the fact that "Their Success is Our Success too!"</div>
                                </div>
                            </div>
                            <!-- End Accordion 1 -->

                            <!-- Start Accordion 2 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-tow" class="collapsed">
                                            <i class="icon-down-open-1 control-icon"></i>
                                            <i class="icon-gift-1"></i> Dedicated Team of Experts
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse-tow" class="panel-collapse collapse">
                                    <div class="panel-body">At WPC we believe in what our employees are good at doing, not what they have an idea about. These has helped our productivity by employing Professionals that has strong commitment in ICT Industry and are always ready to learn New Technology to enhance our Clients Productivity.</div>
                                </div>
                            </div>
                            <!-- End Accordion 2 -->

                        </div>
                        <!-- End Accordion -->

                    </div>

                    <div class="col-md-8">

                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>Our Features</span></h4>

                        <div class="row">

                            <!-- Start Service Icon 1 -->
                            <div class="col-md-6 service-box service-icon-left-more">
                                <div class="service-icon">
                                    <i class="icon-paper-plane icon-medium"></i>
                                </div>
                                <div class="service-content">
                                    <h4>Computing Programming Education</h4>
                                    <p>We are passionate about giving <strong class="accent-color">values</strong> to every child with the knowlegde of Computer Science.</p>
                                </div>
                            </div>
                            <!-- End Service Icon 1 -->

                            <!-- Start Service Icon 2 -->
                            <div class="col-md-6 service-box service-icon-left-more">
                                <div class="service-icon">
                                    <i class="icon-basket-1 icon-medium"></i>
                                </div>
                                <div class="service-content">
                                    <h4>Business Application</h4>
                                    <p>We Provide Business application <strong class="accent-color">softwares</strong> for businesses to run effectively as desired.</p>
                                </div>
                            </div>
                            <!-- End Service Icon 2 -->

                            <!-- Start Service Icon 3 -->
                            <div class="col-md-6 service-box service-icon-left-more">
                                <div class="service-icon">
                                    <i class="icon-globe-1 icon-medium"></i>
                                </div>
                                <div class="service-content">
                                    <h4>Web Development</h4>
                                    <p>We develop your <strong class="accent-color">ideas, businesses and inovations</strong> on the world wide web.</p>
                                </div>
                            </div>
                            <!-- End Service Icon 3 -->

                            <!-- Start Service Icon 4 -->
                            <div class="col-md-6 service-box service-icon-left-more">
                                <div class="service-icon">
                                    <i class="icon-camera-alt icon-medium"></i>
                                </div>
                                <div class="service-content">
                                    <h4>Graphic Design</h4>
                                    <p>We design your <strong class="accent-color">Wholesome Graphics</strong> for your needs.</p>
                                </div>
                            </div>
                            <!-- End Service Icon 4 -->

                            <!-- Start Service Icon 5 -->
                            <div class="col-md-6 service-box service-icon-left-more">
                                <div class="service-icon">
                                    <i class="icon-feather icon-medium"></i>
                                </div>
                                <div class="service-content">
                                    <h4>Software Design</h4>
                                    <p>We design your <strong class="accent-color">Dream Application</strong> with our understanding of your ideas.</p>
                                </div>
                            </div>
                            <!-- End Service Icon 5 -->

                            <!-- Start Service Icon 6 -->
                            <div class="col-md-6 service-box service-icon-left-more">
                                <div class="service-icon">
                                    <i class="icon-tools icon-medium"></i>
                                </div>
                                <div class="service-content">
                                    <h4>Administrative Support</h4>
                                    <p>We are here <strong class="accent-color">24/7</strong> for your business support at everytime you call on Us.</p>
                                </div>
                            </div>
                            <!-- End Service Icon 6 -->

                        </div>
                    </div>

                </div>

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:25px;"></div>

                <div class="row">

                    <div class="col-md-8">

                        <!-- Start Clients Carousel -->
                        <div class="our-clients">

                            <!-- Classic Heading -->
                            <h4 class="classic-title"><span>Our Clients & Partners</span></h4>

                            <div class="clients-carousel custom-carousel touch-carousel" data-appeared-items="4">
                            @foreach($clients as $client)
                                <!-- Client 1 -->
                                <div class="client-item item">
                                    <a href="{{$client->weblink}}"><img src="{{$client->image}}" alt="" /></a>
                                </div>
                            @endforeach
                            </div>
                        </div>
                        <!-- End Clients Carousel -->

                    </div>

                    <div class="col-md-4">

                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>Client Testimonials</span></h4>

                        <!-- Start Testimonials Carousel -->
                        <div class="custom-carousel show-one-slide touch-carousel" data-appeared-items="1">
                            <!-- Testimonial 1 -->
                            <div class="classic-testimonials item">
                                <div class="testimonial-content">
                                    <p>They are Young Enterprenuer that are passionate about investing great values to individuals, businesses and Organization.</p>
                                </div>
                                <div class="testimonial-author"><span>Johnson</span> - Customer</div>
                            </div>
                            <!-- Testimonial 2 -->
                            <div class="classic-testimonials item">
                                <div class="testimonial-content">
                                    <p>They Make their Customer Success their Joy..</p>
                                </div>
                                <div class="testimonial-author"><span>Mark Davids</span> - Customer</div>
                            </div>
                            <!-- Testimonial 3 -->
                            <div class="classic-testimonials item">
                                <div class="testimonial-content">
                                    <p>They are innovators with great ideas to grow every buiness or Organization to their desired Status in the World Economy.</p>
                                </div>
                                <div class="testimonial-author"><span>Sunday Ajanaku</span> - Customer</div>
                            </div>
                        </div>
                        <!-- End Testimonials Carousel -->

                    </div>

                </div>


            </div>
        </div>
    </div>
    @stop

