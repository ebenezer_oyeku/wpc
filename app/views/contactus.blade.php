@extends('layouts.default')

@section('title')
	Contact Us
@stop


@section('map')
	<div id="map">
		<iframe src="https://www.google.com/maps/d/embed?mid=zUmZdj13oErE.kt1oQIfJBSsc" width="100%" height="400"></iframe>
	</div>
	@stop

@section('content')

	<div id="content">
		<div class="container">
			<div class="page-content">
				@if(Session::has('message'))
					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif

				<div class="row">

				<div class="col-md-8">

					<!-- Classic Heading -->
					<h4 class="classic-title"><span>Contact Us</span></h4>
					<div id="note"></div>
					<div id="fields">
						<!-- Start Contact Form -->
						{{Form::open(array('action' => 'ContactusController@getContactUsForm'))}}

						<div class="form-group">
							{{ Form::label('first_name', 'Your First Name') }}
							{{ Form::text('first_name', null, array('class' => 'form-control', 'placeholder' => 'Your First Name')) }}
							@if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
						</div>

						<div class="form-group">
							{{Form::label('last_name', 'Your Last Name')}}
							{{Form::text('last_name', null, array('class' => 'form-control', 'placeholder' => 'Your Last Name'))}}
							@if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
						</div>

						<div class="form-group">
							{{Form::label('phone_number', 'Your Phone Number')}}
							{{Form::text('phone_number', null, array('class' => 'form-control', 'placeholder' => '+767680754545'))}}
							@if ($errors->has('phone_number')) <p class="help-block">{{ $errors->first('phone_number') }}</p> @endif
						</div>

						<div class="form-group">
							{{ Form::label('email', 'Your Email') }}
							{{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Your Email Address')) }}
							@if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
						</div>

						<div class="form-group">
							{{ Form::label ('subject', 'Subject') }}
							{{ Form::select ('subject', array(
                            '1' => 'Make Enquiry About Services',
                            '2' => 'Clients Complaints on Services or Products',
                            '3' => 'Appreciation or Testimony about Services Rendered',
                            '4' => 'Request Information About the Kids-HUB'), '1' ) }}
							@if ($errors->has('subject')) <p class="help-block">{{ $errors->first('subject') }}</p> @endif
						</div>

						<div class="form-group">
							{{ Form::label('message', 'Your Message') }}
							{{ Form::textarea('message', null, array('class' => 'form-control', 'placeholder' => 'Your Message for Us')) }}
							@if ($errors->has('message')) <p class="help-block">{{ $errors->first('message') }}</p> @endif
						</div>
						{{Form::submit('Send Message', array('class' => 'btn btn-default'))}}
						{{Form::reset('Clear', array('class' => 'btn btn-default'))}}

						{{Form:: close()}}
						<!-- End Contact Form -->
					</div>
				</div>

				<div class="col-md-4">

					<!-- Classic Heading -->
					<h4 class="classic-title"><span>Information</span></h4>
					<!-- Some Info -->
					<p>We are here to serve you to your Satisfaction</p>

					<!-- Divider -->
					<div class="hr1" style="margin-bottom:10px;"></div>

					<!-- Info - Icons List -->
					<ul class="icons-list">
						<li><i class="icon-location-1"></i> <strong>Lagos Address:</strong> F89 Ikota Shopping Complex, VGC, Ajah, Lagos State, Nigeria..</li>
						<li><i class="icon-location-1"></i> <strong>Ibadan Address:</strong> 15 Ike-Oluwa Plaza, Elewure Junction, Akala Express Road New Garage, Ibadan, Oyo State, Nigeria..</li>
						<li><i class="icon-mail-1"></i> <strong>Email:</strong> info@webplanetcon.com</li>
						<li><i class="icon-mobile-1"></i> <strong>Phone:</strong> +234 081 6869 4999</li>
					</ul>

					<!-- Divider -->
					<div class="hr1" style="margin-bottom:15px;"></div>

					<!-- Classic Heading -->
					<h4 class="classic-title"><span>Working Hours</span></h4>

					<!-- Info - List -->
					<ul class="list-unstyled">
						<li><i class="icon icon-time"></i> <strong>Monday - Friday</strong> - 8am to 5pm</li>
						<li><i class="icon icon-time"></i> <strong>Saturday</strong> - 8am to 2pm</li>
						<li><i class="icon icon-time"></i> <strong>Sunday</strong> - Closed</li>
					</ul>

				</div>

			</div>
			</div>
		</div>
	</div>

	@stop
