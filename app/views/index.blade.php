@extends('layouts.default')

@section('title')
    Welcome to WPC!
    @stop


@section('slider')

    <div class="fullwidthbanner-container">
        <div class="fullwidthbanner" >
            <ul>

                <!-- SLIDE 1 -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                    <!-- MAIN IMAGE -->
                    <img src="images/slide-02-bg.jpg" data-fullwidthcentering="on" alt="slidebg2"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption uppercase big_font_size boldest_font_weight dark_font_color sft start"
                         data-x="540"
                         data-y="125"

                         data-speed="300"
                         data-start="1600"
                         data-easing="easeOutExpo"><span class="accent-color">Professional Consulting</span><br>that you need!
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption medium_font_size regular_font_weight dark_font_color sfl start"
                         data-x="540"
                         data-y="208"

                         data-speed="300"
                         data-start="1900"
                         data-easing="easeOutExpo">The best Consulting Group that to get you started
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption mini_font_size light_font_weight gray_font_color sfr start"
                         data-x="540"
                         data-y="250"

                         data-speed="300"
                         data-start="2200"
                         data-easing="easeOutExpo">"Our Success is Our Satisfied Clients".
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption sfb start"
                         data-x="540"
                         data-y="314"

                         data-speed="300"
                         data-start="2500"
                         data-easing="easeOutExpo"><a href="{{URL::to('about')}}" class="tp-caption btn-system btn-large border-btn btn-gray">Why WPC</a>
                    </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption sfl start"
                         data-x="180"
                         data-y="bottom"

                         data-speed="1000"
                         data-start="1000"
                         data-easing="Power1.easeOut"><img src="images/slide-02-image-01.png" alt="" />
                    </div>

                </li>

                <!-- SLIDE 2  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                    <!-- MAIN IMAGE -->
                    <img src="images/slide-03-bg.jpg" data-fullwidthcentering="on" alt="slidebg3"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption mini_font_size bold_font_weight dark_font_color gray_bg sfl start"
                         data-x="left"
                         data-y="110"

                         data-speed="300"
                         data-start="1600"
                         data-easing="easeInOutExpo">Database Design & Implementation
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption mini_font_size bold_font_weight dark_font_color gray_bg sfr start"
                         data-x="left"
                         data-y="150"

                         data-speed="300"
                         data-start="1900"
                         data-easing="easeInOutExpo">Software Analysis & Engineering
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption mini_font_size bold_font_weight dark_font_color gray_bg sfl start"
                         data-x="left"
                         data-y="190"

                         data-speed="300"
                         data-start="2200"
                         data-easing="easeInOutExpo">Web Design and Apps
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption  mini_font_size bold_font_weight dark_font_color gray_bg sfr start"
                         data-x="left"
                         data-y="230"

                         data-speed="300"
                         data-start="2500"
                         data-easing="easeInOutExpo">Software Development & BIT Consultancy
                    </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption mini_font_size bold_font_weight dark_font_color gray_bg sfl start"
                         data-x="left"
                         data-y="270"

                         data-speed="300"
                         data-start="2800"
                         data-easing="easeInOutExpo">Graphics and Logo Designing
                    </div>

                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption mini_font_size bold_font_weight dark_font_color gray_bg sfr start"
                         data-x="left"
                         data-y="310"

                         data-speed="300"
                         data-start="3100"
                         data-easing="easeInOutExpo">ICT Education for Schools
                    </div>

                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption sfr start"
                         data-x="center"
                         data-y="60"
                         data-hoffset="100"

                         data-speed="600"
                         data-start="1000"
                         data-easing="easeOutBack"
                         style="z-index: 2"><img src="images/slide-03-image-01.png" alt="" />
                    </div>

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption sfl start"
                         data-x="right"
                         data-y="70"
                         data-hoffset="-100"

                         data-speed="600"
                         data-start="1300"
                         data-easing="easeOutBack"
                         style="z-index: 1"><img src="images/slide-03-image-02.png" alt="" />
                    </div>

                </li>

                <!-- SLIDE 3  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                    <!-- MAIN IMAGE -->
                    <img src="images/slide-01-bg.jpg" data-fullwidthcentering="on" alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption uppercase big_font_size boldest_font_weight dark_font_color sft start"
                         data-x="center"
                         data-y="140"

                         data-speed="300"
                         data-start="1000"
                         data-easing="easeOutExpo">Team of Change Maker
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption medium_font_size regular_font_weight dark_font_color sfb start"
                         data-x="center"
                         data-y="182"

                         data-speed="300"
                         data-start="1300"
                         data-easing="easeOutExpo">The Best IT Consultancy that Get You Started
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption small_font_size light_font_weight gray_font_color text-center sfb start"
                         data-x="center"
                         data-y="220"

                         data-speed="300"
                         data-start="1600"
                         data-easing="easeOutExpo">Changing the Scope of ICT and Computer Programming among Kids </br> in Schools and Individual that loves innovative ideas.
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption sfr start"
                         data-x="center"
                         data-y="292"
                         data-hoffset="84"

                         data-speed="600"
                         data-start="2000"
                         data-easing="easeOutExpo"><a href="{{URL::to('contactus')}}" class="btn-system btn-medium">Contact Us Now</a>
                    </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption sfl start"
                         data-x="center"
                         data-y="292"
                         data-hoffset="-84"

                         data-speed="600"
                         data-start="2000"
                         data-easing="easeOutExpo"><a href="{{URL::to('services')}}" class="btn-system btn-medium btn-gray">See More Features</a>
                    </div>

                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption customin start"
                         data-x="880"
                         data-y="30"

                         data-speed="1600"
                         data-start="2100"
                         data-easing="Power1.easeOut"
                         data-customin="x:300;y:-40;z:-400;rotationX:0;rotationY:0;rotationZ:-20;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;">
                        <img src="images/slide-01-image-02.png" alt="" style="width:193px; height: 231px;" />
                    </div>

                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption customin start"
                         data-x="140"
                         data-y="120"

                         data-speed="1400"
                         data-start="2600"
                         data-easing="Power1.easeOut"
                         data-customin="x:-200;y:20;z:-500;rotationX:0;rotationY:0;rotationZ:10;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;">
                        <img src="images/slide-01-image-01.png" alt="" style="width:80px; height: 101px;" />
                    </div>

                </li>

            </ul>
            <div class="tp-bannertimer" style="visibility:hidden;"></div>
        </div>
    </div>

@stop

@section('content')

    <!-- Start Full Width Section 1 -->
    <div class="section" style="padding-top:60px; padding-bottom:30px; border-top:0; border-bottom:0; background:#fff">
        <div class="container">

            <!-- Start Services Icons -->
            <div class="row">

                <!-- Start Service Icon 1 -->
                <div class="col-md-3 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
                    <div class="service-icon">
                        <i class="icon-leaf-1 icon-medium"></i>
                    </div>
                    <div class="service-content">
                        <h4>Database Design</h4>
                        <p>Database Analysis and Design, Database Consulting,Database Development Methodology <a title="Get More Info" href="{{URL::to('services')}}" class="sh-tooltip" data-placement="left">Read More</a></p>

                    </div>
                </div>
                <!-- End Service Icon 1 -->

                <!-- Start Service Icon 2 -->
                <div class="col-md-3 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
                    <div class="service-icon">
                        <i class="icon-desktop-2 icon-medium"></i>
                    </div>
                    <div class="service-content">
                        <h4>IT Globalization</h4>
                        <p>Creation of Information Technology ideas and tools to meet up our End user needs. <a title="Get More Info" href="{{URL::to('services')}}" class="sh-tooltip" data-placement="left">Read More</a></p>
                    </div>
                </div>
                <!-- End Service Icon 2 -->

                <!-- Start Service Icon 3 -->
                <div class="col-md-3 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
                    <div class="service-icon">
                        <i class="icon-droplet icon-medium"></i>
                    </div>
                    <div class="service-content">
                        <h4>Software Development</h4>
                        <p>Software Information System, Software Analysis and Design, Software Programming in Various Languages  <a title="Get More Info" href="{{URL::to('services')}}" class="sh-tooltip" data-placement="left">Read More</a></p>
                    </div>
                </div>
                <!-- End Service Icon 3 -->

                <!-- Start Service Icon 4 -->
                <div class="col-md-3 service-box service-center" data-animation="fadeIn" data-animation-delay="04">
                    <div class="service-icon">
                        <i class="icon-brush icon-medium"></i>
                    </div>
                    <div class="service-content">
                        <h4>Graphic Designing</h4>
                        <p>Graphic Design, Company Logo Creation, Image Fine-tuning, Project Presentation Design
                            <a title="Get More Info" href="{{URL::to('services')}}" class="sh-tooltip" data-placement="left">Read More</a></p>
                    </div>
                </div>
                <!-- End Service Icon 4 -->

                <!-- Start Service Icon 5 -->
                <div class="col-md-3 service-box service-center" data-animation="fadeIn" data-animation-delay="05">
                    <div class="service-icon">
                        <i class="icon-paper-plane icon-medium"></i>
                    </div>
                    <div class="service-content">
                        <h4>IT Consultancy</h4>
                        <p>IT Application Specification,IT Management and Support,IT Procurement Management,Management and Operation of IT Systems<a title="Simple Tooltip" href="{{URL::to('services')}}" class="sh-tooltip" data-placement="left">Read More</a></p>
                    </div>
                </div>
                <!-- End Service Icon 5 -->

                <!-- Start Service Icon 6 -->
                <div class="col-md-3 service-box service-center" data-animation="fadeIn" data-animation-delay="06">
                    <div class="service-icon">
                        <i class="icon-css3 icon-medium"></i>
                    </div>
                    <div class="service-content">
                        <h4>Web Development</h4>
                        <p>Web Design & Implementation,Web Design & Ecommerce with ASP.Net (Shopping Cart)
                            Web Design Course <a title="Simple Tooltip" href="{{URL::to('services')}}" class="sh-tooltip" data-placement="left">Read More</a></p>
                    </div>
                </div>
                <!-- End Service Icon 6 -->

                <!-- Start Service Icon 7 -->
                <div class="col-md-3 service-box service-center" data-animation="fadeIn" data-animation-delay="07">
                    <div class="service-icon">
                        <i class="icon-download-1 icon-medium"></i>
                    </div>
                    <div class="service-content">
                        <h4>Legal Services</h4>
                        <p>Our Corporate Development and Litigation include Company Registration, Business Name Search, Legal Adviser and Litigation <a title="Simple Tooltip" href="{{URL::to('services')}}" class="sh-tooltip" data-placement="left">Read More</a></p>
                    </div>
                </div>
                <!-- End Service Icon 7 -->

                <!-- Start Service Icon 8 -->
                <div class="col-md-3 service-box service-center" data-animation="fadeIn" data-animation-delay="08">
                    <div class="service-icon">
                        <i class="icon-lifebuoy icon-medium"></i>
                    </div>
                    <div class="service-content">
                        <h4>Help & Support</h4>
                        <p>Business Softwares like Accounting Softwares, Loan Softwares, Hospital Management Softwares, School Management Softwares<a title="Simple Tooltip" href="{{URL::to('services')}}" class="sh-tooltip" data-placement="left">Read More</a></p>
                    </div>
                </div>
                <!-- End Service Icon 8 -->

            </div>
            <!-- End Services Icons -->

        </div>
    </div>
    <!-- End Full Width Section 1 -->


    <!-- Start Full Width Section 2 -->
    <div class="section" style="padding-top:60px; padding-bottom:0px; border-top:1px solid #eee; border-bottom:1px solid #eee; background: url(images/browser-section.jpg) #f9f9f9;">
        <div class="container">

            <!-- WPC Logo Image -->
            <div class="text-center"><img data-animation="fadeInDown" data-animation-delay="01" alt="" src="images/vella-pic.png" /></div>

            <!-- WPC Browser Image -->
            <div class="text-center"><iframe src="http://www.youtube.com/embed/playlist?list=PLzdnOPI1iJNe1WmdkMG-Ca8cLQpdEAL7Q"
                                             width="1118" height="495" frameborder="0"></iframe></div>
        </div>
    </div>
    <!-- End Full Width Section 2 -->


    <!-- Start Full Width Section 3 -->
    <div class="section" style="padding-top:50px; padding-bottom:25px; border-top:0; border-bottom:0; background:#fff;">
        <div class="container">

            <!-- Start Milestone Block 1 -->
            <div class="milestone-block" data-animation="fadeIn" data-animation-delay="01">
                <div class="milestone-icon"><i class="icon-box-1"></i></div>
                <div class="milestone-right">
                    <div class="milestone-number">82</div>
                    <div class="milestone-text">Our Products</div>
                </div>
            </div>
            <!-- End Milestone Block 1 -->

            <!-- Start Milestone Block 2 -->
            <div class="milestone-block" data-animation="fadeIn" data-animation-delay="02">
                <div class="milestone-icon"><i class="icon-users-1"></i></div>
                <div class="milestone-right">
                    <div class="milestone-number">64</div>
                    <div class="milestone-text">Our Customers</div>
                </div>
            </div>
            <!-- End Milestone Block 2 -->

            <!-- Start Milestone Block 3 -->
            <div class="milestone-block" data-animation="fadeIn" data-animation-delay="03">
                <div class="milestone-icon"><i class="icon-brush"></i></div>
                <div class="milestone-right">
                    <div class="milestone-number">13</div>
                    <div class="milestone-text">Wordpress Web</div>
                </div>
            </div>
            <!-- End Milestone Block 3 -->

            <!-- Start Milestone Block 4 -->
            <div class="milestone-block" data-animation="fadeIn" data-animation-delay="04">
                <div class="milestone-icon"><i class="icon-download-1"></i></div>
                <div class="milestone-right">
                    <div class="milestone-number">14</div>
                    <div class="milestone-text">New Updates</div>
                </div>
            </div>
            <!-- End Milestone Block 4 -->

            <!-- Start Milestone Block 5 -->
            <div class="milestone-block" data-animation="fadeIn" data-animation-delay="05">
                <div class="milestone-icon"><i class="icon-twitter-2"></i></div>
                <div class="milestone-right">
                    <div class="milestone-number">4000</div>
                    <div class="milestone-text">Twitter Followers</div>
                </div>
            </div>
            <!-- End Milestone Block 5 -->

        </div>
    </div>
    <!-- End Full Width Section 3 -->


    <!-- Start Full Width Section 4 -->
    <div class="section" style="padding-top:120px; padding-bottom:120px; border-top:1px solid #eee; border-bottom:1px solid #eee; background:#fff;">
        <div class="container">

            <!-- Start Video Section Content -->
            <div class="section-video-content text-center">

                <!-- Start Animations Text -->
                <h1 class="fittext wite-text uppercase tlt">
                           <span class="texts">
                           	   <span>Modern</span>
                               <span>Clean</span>
                               <span>Awesome</span>
                               <span>Cool</span>
                               <span>Great</span>
                           </span>
                    Webplanet Consulting & Services is Ready for <br/>Businesses, Agencies, Organizations <strong>or</strong> Creative Portfolios
                </h1>
                <!-- End Animations Text -->

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:32px;"></div>

                <!-- Start Buttons -->
                <a href="{{URL::to('services')}}" class="btn-system btn-large border-btn btn-wite"><i class="icon-feather"></i> Check Our Clients</a>
                <a href="{{URL::to('contactus')}}" class="btn-system btn-large btn-wite"><i class="icon-download-3"></i> ContactUs Now</a>

            </div>
            <!-- End Section Content -->

            <!-- Start Video -->
            <video class="section-video" poster="images/video/poster.jpg" autoplay loop preload="none">
                <!-- MP4 source must come first for iOS -->
                <source type="video/mp4" src="images/video/video.mp4" />
                <!-- WebM for Firefox 4 and Opera -->
                <source type="video/webm" src="images/video/video.webm" />
                <!-- OGG for Firefox 3 -->
                <source type="video/ogg" src="images/video/video.ogv" />
                <!-- Fallback flash player for no-HTML5 browsers with JavaScript turned off -->
                <object type="application/x-shockwave-flash" data="images/video/flashmediaelement.swf">
                    <param name="movie" value="images/video/flashmediaelement.swf" />
                    <param name="flashvars" value="controls=false&amp;file=images/video/flashmediaelement.mp4" />
                    <!-- Image fall back for non-HTML5 browser with JavaScript turned off and no Flash player installed -->
                    <img src="images/video/poster.jpg" alt="No Video Image" title="No video playback capabilities" />
                </object>
            </video>
            <script>$('.section-video').mediaelementplayer({ loop:true });</script>
            <!-- End Video -->

            <!-- Start Video Section overlay -->
            <div class="section-overlay"></div>

        </div>
    </div>
    <!-- End Full Width Section 4 -->


    <!-- Start Full Width Section 6 -->
    <div class="section bg-parallax light-section" style="padding-top:80px; padding-bottom:80px; border-top:0; border-bottom:0; background: url(images/testimonials-bg.jpg);">
        <div class="container">

            <!-- Start Testimonials Carousel -->
            <div class="testimonials-carousel">
                <!-- Testimonial 1 -->
                <div class="testimonials item">
                    <div class="testimonial-content">
                        <p>They Make their Customer Success their Joy..</p>
                    </div>
                    <div class="testimonial-author"><span>Mark Davids</span> - Customer</div>
                </div>
                <!-- Testimonial 2 -->
                <div class="testimonials item">
                    <div class="testimonial-content">
                        <p>They are Young Enterprenuer that are passionate about investing great values to individuals, businesses and Organization.</p>
                    </div>
                    <div class="testimonial-author"><span>Johnson</span> - Customer</div>
                </div>
                <!-- Testimonial 3 -->
                <div class="testimonials item">
                    <div class="testimonial-content">
                        <p>They are innovators with great ideas to grow every buiness or Organization to their desired Status in the World Economy.</p>
                    </div>
                    <div class="testimonial-author"><span>Sunday Ajanaku</span> - Customer</div>
                </div>
            </div>
            <!-- End Testimonials Carousel -->

        </div>
    </div>
    <!-- End Full Width Section 5 -->


    <!-- Start Full Width Section 7 -->
    <div class="section" style="padding-top:60px; padding-bottom:40px; border-top:1px solid #eee; border-bottom:1px solid #eee; background:#fff;">
        <div class="container">

            <!-- Start Big Heading -->
            <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                <p class="title-desc">We Make Your Smile</p>
                <h1>Our Great <strong>Team</strong></h1>
            </div>
            <!-- End Big Heading -->

            <!-- Some Text -->
            <p class="text-center">Our Team are fully dedicated member that are always ready to put a smile on our client faces whenever they are in need of a solution to a persisting probem in their Organisation.</p>

            <!-- Divider -->
            <div class="hr1" style="margin-bottom:35px;"></div>

            <!-- Start Team Members -->
            <div class="row">

                <!-- Start Memebr 1 -->
                <div class="col-md-3" data-animation="fadeIn" data-animation-delay="03">
                    <div class="team-member modern">
                        <!-- Memebr Photo, Name & Position -->
                        <div class="member-photo">
                            <img alt="" src="images/member-01.png" />
                            <div class="member-name">Oyeku Ebenezer<span>Developer</span></div>
                        </div>
                        <!-- Memebr Words -->
                        <div class="member-info">
                            <p>Work  as Network Administrator, Business Developer Staff, Database Developer, Designer, System Analyst, Project Manager and Software Engineer for Over 3 years at Different Companies.</p>
                        </div>
                        <!-- Start Progress Bar 1 -->
                        <div class="progress-label">Graphics Designer</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="96%" data-appear-animation-delay="400">
                                <span class="percentage">96%</span>
                            </div>
                        </div>
                        <!-- Start Progress Bar 2 -->
                        <div class="progress-label">Software Analyst</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="98%" data-appear-animation-delay="800">
                                <span class="percentage">98%</span>
                            </div>
                        </div>
                        <!-- Start Progress Bar 3 -->
                        <div class="progress-label">Database Administration</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="100%" data-appear-animation-delay="1200">
                                <span class="percentage">100%</span>
                            </div>
                        </div>
                        <!-- Memebr Social Links -->
                        <div class="member-socail">
                            <a class="twitter" href="https://twitter.com/unstinted"><i class="icon-twitter-2"></i></a>
                            <a class="gplus" href="https://plus.google.com/u/0/113737353160487799790"><i class="icon-gplus"></i></a>
                            <a class="linkedin" href="https://www.linkedin.com/in/ebenezer-oyeku-7a540125?trk=nav_responsive_tab_profile"><i class="icon-linkedin"></i></a>
                            <a class="facebook" href="https://www.facebook.com/odammy"><i class="icon-facebook"></i></a>
                            <a class="mail" href="mailto:ebenezeroyeku@webplanetcon.com"><i class="icon-mail-2"></i></a>
                        </div>
                    </div>
                </div>
                <!-- End Memebr 1 -->

                <!-- Start Memebr 2 -->
                <div class="col-md-3" data-animation="fadeIn" data-animation-delay="04">
                    <div class="team-member modern">
                        <!-- Memebr Photo, Name & Position -->
                        <div class="member-photo">
                            <img alt="" src="images/member-02.png" />
                            <div class="member-name">Tran Bach <span>Developer</span></div>
                        </div>
                        <!-- Memebr Words -->
                        <div class="member-info">
                            <p>Work as a Project Developer, Analyst and Designer.</p>
                        </div>
                        <!-- Start Progress Bar 1 -->
                        <div class="progress-label">Project Analyst</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="78%" data-appear-animation-delay="400">
                                <span class="percentage">78%</span>
                            </div>
                        </div>
                        <!-- Start Progress Bar 2 -->
                        <div class="progress-label">Application Developer</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="92%" data-appear-animation-delay="800">
                                <span class="percentage">92%</span>
                            </div>
                        </div>
                        <!-- Start Progress Bar 3 -->
                        <div class="progress-label">Project Manager</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="80%" data-appear-animation-delay="1200">
                                <span class="percentage">80%</span>
                            </div>
                        </div>
                        <!-- Memebr Social Links -->
                        <div class="member-socail">
                            <a class="facebook" href="https://www.facebook.com/tranbach93?fref=ts"><i class="icon-facebook"></i></a>
                            <a class="gplus" href="https://plus.google.com/u/0/+B%C3%A1chTr%E1%BA%A7n93/posts"><i class="icon-gplus"></i></a>
                        </div>
                    </div>
                </div>
                <!-- End Memebr 2 -->

                <!-- Start Memebr 3 -->
                <div class="col-md-3" data-animation="fadeIn" data-animation-delay="05">
                    <div class="team-member modern">
                        <!-- Memebr Photo, Name & Position -->
                        <div class="member-photo">
                            <img alt="" src="images/member-03.png" />
                            <div class="member-name">Opeyemi Aderibigbe<span>Company Secretary</span></div>
                        </div>
                        <!-- Memebr Words -->
                        <div class="member-info">
                            <p>Worked at Various Establishment with Proven Experience.</p>
                        </div>
                        <!-- Start Progress Bar 1 -->
                        <div class="progress-label">Legal Counsel</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="96%" data-appear-animation-delay="400">
                                <span class="percentage">96%</span>
                            </div>
                        </div>
                        <!-- Start Progress Bar 2 -->
                        <div class="progress-label">Business Developer</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="94%" data-appear-animation-delay="800">
                                <span class="percentage">94%</span>
                            </div>
                        </div>
                        <!-- Start Progress Bar 3 -->
                        <div class="progress-label">Human Resources</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="90%" data-appear-animation-delay="1200">
                                <span class="percentage">90%</span>
                            </div>
                        </div>
                        <!-- Memebr Social Links -->
                        <div class="member-socail">
                            <a class="facebook" href="https://www.facebook.com/opeyemi.oyeku.7?fref=ts"><i class="icon-facebook"></i></a>
                            <a class="twitter" href="https://twitter.com/opnaty2009"><i class="icon-twitter-2"></i></a>
                            <a class="linkedin" href="https://www.linkedin.com/in/opeyemi-oyeku-31b07194"><i class="icon-linkedin"></i></a>
                            <a class="mail" href="mailto:opeyemi.aderibigbe@webplanetcon.com"><i class="icon-mail-2"></i></a>
                        </div>
                    </div>
                </div>
                <!-- End Memebr 3 -->

                <!-- Start Memebr 4 -->
                <div class="col-md-3" data-animation="fadeIn" data-animation-delay="06">
                    <div class="team-member modern">
                        <!-- Memebr Photo, Name & Position -->
                        <div class="member-photo">
                            <img alt="" src="images/member-04.png" />
                            <div class="member-name"> Nguyen Duc Anh <span>Developer</span></div>
                        </div>
                        <!-- Memebr Words -->
                        <div class="member-info">
                            <p>Worked as a Project Manager, Project Leader, Software Developer, Analyst and Database designer on Various projects for Various Company.</p>
                        </div>
                        <!-- Start Progress Bar 1 -->
                        <div class="progress-label">Project Manager</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="96%" data-appear-animation-delay="400">
                                <span class="percentage">96%</span>
                            </div>
                        </div>
                        <!-- Start Progress Bar 2 -->
                        <div class="progress-label">Database Designer</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="85%" data-appear-animation-delay="800">
                                <span class="percentage">85%</span>
                            </div>
                        </div>
                        <!-- Start Progress Bar 3 -->
                        <div class="progress-label">Software Developer</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-progress-animation="100%" data-appear-animation-delay="1200">
                                <span class="percentage">100%</span>
                            </div>
                        </div>
                        <!-- Memebr Social Links -->
                        <div class="member-socail">
                            <a class="facebook" href="https://www.facebook.com/nguyenduc.anh.906?fref=ts"><i class="icon-facebook"></i></a>
                            <a class="gplus" href="https://plus.google.com/u/0/118353886690704267032/posts"><i class="icon-gplus-1"></i></a>
                        </div>
                    </div>
                </div>
                <!-- End Memebr 4 -->

            </div>
            <!-- End Team Members -->

            <!-- Divider -->
            <div class="hr1" style="margin-bottom:40px;"></div>

            <div class="text-center">
                <div class="pieChart" data-bar-color="#00afd1" data-bar-width="150" data-percent="95">
                    <span>Web Design</span>
                    <i class="icon-brush"></i>
                </div>
                <div class="pieChart" data-bar-color="#00afd1" data-bar-width="150" data-percent="92">
                    <span>Web Developing</span>
                    <i class="icon-laptop-1"></i>
                </div>
                <div class="pieChart" data-bar-color="#00afd1" data-bar-width="150" data-percent="85">
                    <span>ICT Education</span>
                    <i class="icon-briefcase-1"></i>
                </div>
                <div class="pieChart" data-bar-color="#00afd1" data-bar-width="150" data-percent="94">
                    <span>Brand Design</span>
                    <i class="icon-leaf-2"></i>
                </div>
                <div class="pieChart" data-bar-color="#00afd1" data-bar-width="150" data-percent="96">
                    <span>Software Support</span>
                    <i class="icon-traffic-cone"></i>
                </div>
            </div>

        </div>
    </div>
    <!-- End Full Width Section 6 -->

@stop

@section('loader')
    <div class="spinner">
        <div class="dot1"></div>
        <div class="dot2"></div>
    </div>
    @stop










