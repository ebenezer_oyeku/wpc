@extends('layouts.default')

@section('title')
	Application for Job
@stop


@section('content')
	<div id="content">
		<div class="container">

			<div class="page-content">
				{{HTML::image('images/join.jpg')}}
				@if(Session::has('message'))
					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif

				<div class="sbh"></div>
				<div class="idz">&nbsp;</div>
				<div class="row">


					{{ Form::open(array('action'=>'ApplicationController@postJobApp', 'files' => true)) }}
					<div class="row">
						<div class="col-md-6">
							<div id="output"></div>

							<h4>Image Upload</h4>
							<div class="form-group">
								{{Form::label('img_up', 'Upload Your Picture e.g yourname-picture.jpg or yourname-picture.png')}}
								{{Form::file('img_up', null, array('class' => 'file_up form-control'))}}
								@if ($errors->has('img_up')) <p class="help-block">{{ $errors->first('img_up') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('job_list', 'Job Opening')}}
								{{Form::select('job_list', $job_lists, null, ['class' => 'form-control'])}}
								@if ($errors->has('job_list')) <p class="help-block">{{ $errors->first('job_list') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('title', 'Title')}}
								{{Form::select('title', array('Ch' => 'Choose','Ms' => 'Ms', 'Miss' => 'Miss', 'Mr' => 'Mr','Mrs' => 'Mrs' ),'Ch', ['class' => 'form-control'])}}
								@if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('firstname', 'First Name')}}
								{{Form::text('firstname', null, array('class' => 'form-control', 'placeholder' => 'Your First Name'))}}
								@if ($errors->has('firstname')) <p class="help-block">{{ $errors->first('firstname') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('middlename', 'Middle Name')}}
								{{Form::text('middlename', null, array('class' => 'form-control', 'placeholder' => 'Your Middle Name'))}}
								@if ($errors->has('middlename')) <p class="help-block">{{ $errors->first('middlename') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('lastname', 'Last Name')}}
								{{Form::text('lastname', null, array('class' => 'form-control', 'placeholder' => 'Your Last Name'))}}
								@if ($errors->has('lastname')) <p class="help-block">{{ $errors->first('lastname') }}</p> @endif
							</div>


							<div class="form-group">
								{{Form::label('gender', 'Gender')}}
								{{Form::select('gender', array('Ch' => 'Choose','male' => 'Male', 'female' => 'Female' ),'Ch', ['class' => 'form-control'])}}
								@if ($errors->has('gender')) <p class="help-block">{{ $errors->first('gender') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('dob', 'Date of Birth')}}
								{{Form::text('dob', null, array('id' => 'datepicker', 'class' => 'form-control', 'placeholder' => 'dd-mm-yyyy'))}}
								@if ($errors->has('dob')) <p class="help-block">{{ $errors->first('dob') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('phone_no', 'Phone Number')}}
								{{Form::text('phone_no', null, array('class' => 'form-control', 'placeholder' => '+2340812345678'))}}
								@if ($errors->has('phone_no')) <p class="help-block">{{ $errors->first('phone_no') }}</p> @endif
							</div>



							<div class="form-group">
								{{Form::label('phone_no2', 'Phone Number 2')}}
								{{Form::text('phone_no2', null, array('class' => 'form-control', 'placeholder' => '+2340812345678'))}}
								@if ($errors->has('phone_no2')) <p class="help-block">{{ $errors->first('phone_no2') }}</p> @endif
							</div>



						</div>

						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('contact_add', 'Contact Address')}}
								{{Form::text('contact_add', null, array('class' => 'form-control', 'placeholder' => 'Your Postal or Residential Address'))}}
								@if ($errors->has('contact_add')) <p class="help-block">{{ $errors->first('contact_add') }}</p> @endif
							</div>
							<div class="form-group">
								{{Form::label('state_lists', 'State of Residence')}}
								{{Form::select('state_lists', $state_lists, null, ['class' => 'form-control'])}}
								@if ($errors->has('state_lists')) <p class="help-block">{{ $errors->first('state_lists') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('e_level', 'Educational Level')}}
								{{Form::select('e_level',
                                array('select' => 'Select',
                                'uni' => 'University',
                                'poly' => 'Polytechnic',
                                'col' => 'College of Education',
                                'v_col' => 'Vocational Institutes',
                                 'sec' => 'Secondary School',
                                 'pry' => 'Primary School',),
                                 'select', ['class' => 'form-control'])}}
								@if ($errors->has('e_level')) <p class="help-block">{{ $errors->first('e_level') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('i_name', 'Institution Name')}}
								{{Form::text('i_name', null, array('class' => 'form-control', 'placeholder' => 'Institution Attended'))}}
								@if ($errors->has('i_name')) <p class="help-block">{{ $errors->first('i_name') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('c_study', 'Course Studied')}}
								{{Form::text('c_study', null, array('class' => 'form-control', 'placeholder' => 'Not Applicable to Secondary and Primary school holders'))}}
								@if ($errors->has('c_study')) <p class="help-block">{{ $errors->first('c_study') }}</p> @endif
							</div>


							<div class="form-group">
								{{Form::label('email', 'Email Address')}}
								{{Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'yourmail@mail.com'))}}
								@if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
							</div>

							<div class="form-group">
								{{Form::label('cv_up', 'Upload Your Resume e.g yourname-CV.doc or yourname-CV.docx or yourname-CV.pdf')}}
								{{Form::file('cv_up', null, array('class' => 'file_up form-control'))}}
								@if ($errors->has('cv_up')) <p class="help-block">{{ $errors->first('cv_up') }}</p> @endif
							</div>

							<div class="form-group">
								{{ Form::submit('Apply', array('class'=>'btn btn-large btn-success btn-block'))}}
								{{ Form::reset('Clear Form', array('class'=>'btn btn-large btn-primary btn-block'))}}
							</div>
						</div>
					</div>
					{{ Form::close() }}

				</div>
			</div>
		</div>
	</div>
	@stop





