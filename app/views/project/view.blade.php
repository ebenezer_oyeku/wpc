@extends('layouts.default')

@section('title')
    Project Details.
@stop

@section('banner')
    <div class="page-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Project Details</h2>
                    <p>{{ $project->pname }}</p>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumbs">
                        <li><a href="/project">Projects</a></li>
                        <li><a href="#">Project Detail</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div id="content">
        <div class="container">
            <div class="project-page row">

                <!-- Start Single Project Slider -->
                <div class="project-media col-md-8">
                    <div class="touch-slider project-slider">
                        <div class="item">
                            <a class="lightbox" title="This is an image title" href="{{ $project->image }}" data-lightbox-gallery="gallery2">
                                <div class="thumb-overlay"><i class="icon-resize-full"></i></div>
                                <img alt="" src="{{ $project->image }}">
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End Single Project Slider -->

                <!-- Start Project Content -->
                <div class="project-content col-md-4">
                    <h4><span>Project Description</span></h4>
                    <p>{{ $project->details }}</p>
                    <h4><span>{{ $project->pname }}</span></h4>
                    <ul>
                        <li><strong>Client:</strong> {{$project->client->name}}</li>
                        <li><strong>Status:</strong> {{Status::display($project->projState)}}</li>
                        <li><strong>Category:</strong> {{$project->category->name}}</li>
                    </ul>

                    <div class="post-share">
                        <span class='st_sharethis_large' displayText='ShareThis'></span>
                        <span class='st_facebook_large' displayText='Facebook'></span>
                        <span class='st_twitter_large' displayText='Tweet'></span>
                        <span class='st_linkedin_large' displayText='LinkedIn'></span>
                        <span class='st_pinterest_large' displayText='Pinterest'></span>
                        <span class='st_email_large' displayText='Email'></span>
                    </div>

                </div>
                <!-- End Project Content -->

            </div>


        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({publisher: "09ea60ab-288b-44e6-9fc2-3a5df9056b54", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
@endsection