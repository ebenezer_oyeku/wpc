@extends('layouts.default')

@section('title')
    Projects Category.
@stop

@section('content')
    <div id="content">
        <div class="container">
            <div class="row portfolio-page">

                <!-- Start Portfolio Filter -->
                <div class="call-action call-action-boxed call-action-style3 clearfix">
                    <!-- Call Action Button -->
                    <div class="button-side" style="margin-top:10px;"><a href="https://www.webplanetcon.com/kid_techhub/" class="btn-system border-btn btn-medium btn-wite"><i class="icon-folder-open"></i> Join Our TechHub</a> <a href="https://www.webplanetcon.com/Freelance_Office/" class="btn-system border-btn btn-medium"><i class="icon-upload-cloud"></i> Post your Projects</a></div>
                    <!-- Call Action Text -->
                    <h2 class="primary"><strong>{{$category->name}} Projects</strong> at Webplanet Consulting</h2>
                    <div class="btn-group col-md-12">
                        <button type="button" class="btn btn-danger">Browse Project Categories</button>
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            @foreach($catnav as $cat)
                                <li>{{HTML::link('/project/category/'.$cat->id, $cat->name)}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <hr>

                <!-- Start Portfolio Items -->
                <div id="portfolio" class="portfolio-3">
                    @foreach($projects as $project)
                        <!-- Start Portfolio Item -->
                        <div class="portfolio-item logo graphic col-md-4">
                            <div class="portfolio-border">
                                <!-- Start Portfolio Item Thumb -->
                                <div class="portfolio-thumb">
                                    <a class="lightbox" title="{{$project->details}}" href="/project/view/{{$project->id}}">
                                        <div class="thumb-overlay"><i class="icon-resize-full"></i></div>
                                        <img alt="" src="{{$project->image}}" />
                                    </a>
                                </div>
                                <!-- End Portfolio Item Thumb -->
                                <!-- Start Portfolio Item Details -->
                                <div class="portfolio-details">
                                    <a href="#">
                                        <h4>{{$project->pname}}</h4>
                                        <span>{{$project->category->name}}</span>
                                    </a>
                                    <a href="#" class="like-link"><span class="{{Status::displayClass($project->ProjState)}}">Status: {{Status::display($project->projState)}}</span></a>
                                </div>
                                <!-- End Portfolio Item Details -->
                            </div>
                        </div>
                        <!-- End Portfolio Item -->
                    @endforeach
                </div>
                <!-- End Portfolio Item -->

            </div>
            <!-- End Portfolio Items -->

            @stop
            @section('pagination')
                <!-- Start Pagination -->
                <div id="pagination">
                    <span class="all-pages">Page 1 of 3</span>
                    <span class="current page-num">1</span>
                    <a class="page-num" href="#">2</a>
                    <a class="page-num" href="#">3</a>
                    <a class="next-page" href="#">Next</a>
                </div>
                <!-- End Pagination -->
        </div>
    </div>
    @endsection
