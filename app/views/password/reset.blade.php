<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <title>WPC Registration </title>

    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLE CSS -->
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <link href="{{asset('css/signup_style.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <style>
        .alert {
            padding: 8px 35px 8px 14px;
            margin-bottom: 18px;
            text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
            background-color: #00d4b4;
            border: 1px solid #fbeed5;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            color: #c09853;
        }
    </style>

</head>
<body>

<div class="container">
    @if(Session::has('message'))
        <p class="alert">{{ Session::get('message') }}</p>
    @endif
    <div class="row text-center pad-top ">
        <div class="col-md-12">
            <img src="/img/wpc.png" />
            <h2>WPC Reset Password</h2>
        </div>
    </div>

    <div class="row  pad-top">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="form-signup-heading">Please Reset Your Password</h2>
                </div>
                <div class="panel-body">
                    {{ Form::open(array('action'=>'RemindersController@postReset', 'class'=>'form-signup')) }}

                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>

                    <div class="form-group">
                        <input class="form-control" type="hidden" name="token" value="{{ $token }}"/>
                    </div>

                    <div class="form-group">
                        {{ Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
                    </div>

                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder ="Password">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password_confirmation" class="form-control" placeholder ="Confirm Password">
                    </div>

                    <p><small>All Field are Required</small></p>

                    {{ Form::submit('Reset Password', array('class'=>'btn btn-large btn-primary btn-block'))}}
                    <hr />
                    Already Registered ?  <a href="{{URL::to('login')}}" >Login here</a>
                    {{ Form::close() }}
                </div>

            </div>
        </div>


    </div>
</div>


<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
<!-- CORE JQUERY  -->
<script src="js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS  -->
<script src="js/bootstrap.js"></script>

</body>
</html>
