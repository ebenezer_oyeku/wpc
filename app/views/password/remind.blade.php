<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SignIn - To WPC</title>
    <link rel="stylesheet" href="{{asset('css/lib/bootstrap.min.css')}}">
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/signin.css')}}" rel="stylesheet">
    <!--<link href="css/style.css" rel="stylesheet">-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <style>
        .alert {
            padding: 8px 35px 8px 14px;
            margin-bottom: 18px;
            text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
            background-color: #00d4b4;
            border: 1px solid #fbeed5;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            color: #c09853;
        }
    </style>

</head>

<body>
<div class="container">
    @if(Session::has('message'))
        <p class="alert">{{ Session::get('message') }}</p>
    @endif
    <div class="text-center"><img src="img/wpc.png" /></div>
    <h1 class="heading text-center">Webplanetcon.com</h1>
    <p class="heading-desc text-center">Password Reminder</p>

    {{ Form::open(array('action' => 'RemindersController@postRemind', 'class'=>'form-signin')) }}
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>

    {{ Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
    <hr/>
    {{ Form::submit('Send Reminder', array('class'=>'btn btn-large btn-primary btn-block'))}}

    {{ Form::close() }}

</div>

</body>
</html>


