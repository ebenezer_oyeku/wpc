
<!-- Start Header -->
		<div class="hidden-header"></div>
		<header class="clearfix">
            
            <!-- Start Top Bar -->
			<div class="top-bar">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
                            <!-- Start Contact Info -->
							<ul class="contact-details">
                            	<li><a href="#"><i class="icon-mobile-2"></i> +234 815 747 8047</a></li>
                                <li><a href="#"><i class="icon-mail-2"></i> info@webplanetcon.com</a></li>
                    	<a href="{{URL::to('login')}}" class="btn-system border-btn btn-medium">Login/Register</a>
                            </ul>
                            <!-- End Contact Info -->
						</div>
						<div class="col-md-6">
                            <!-- Start Social Links -->
							<ul class="social-list">
								<li>
									<a class="facebook sh-tooltip" data-placement="bottom" title="Facebook" href="https://www.facebook.com/WebplanetCon"><i class="icon-facebook-2"></i></a>
								</li>
								<li>
									<a class="twitter sh-tooltip" data-placement="bottom" title="Twitter" href="https://twitter.com/WebplanetC"><i class="icon-twitter-2"></i></a>
								</li>
								<li>
									<a class="google sh-tooltip" data-placement="bottom" title="Google Plus" href="https://plus.google.com/u/0/111669135886597931127"><i class="icon-gplus"></i></a>
								</li>
                                <li>
									<a class="linkdin sh-tooltip" data-placement="bottom" title="Linkedin" href="https://www.linkedin.com/company/webplanet-consulting-and-services"><i class="icon-linkedin"></i></a>
								</li>
                                <li>
									<a class="instgram sh-tooltip" data-placement="bottom" title="Instagram" href="http://instagram.com/webplanetconsulting?ref=badge"><i class="icon-instagramm"></i></a>
								</li>
								<li>
									<a class="skype sh-tooltip" data-placement="bottom" title="Skype" href="skype:webplanet.consulting?call"><i class="icon-skype-2"></i></a>
								</li>
							</ul>
                            <!-- End Social Links -->
						</div>
					</div>
				</div>
			</div>
            <!-- End Top Bar -->
          
			<!-- Start Header ( Logo & Naviagtion ) -->
			<div class="navbar navbar-default navbar-top">
				<div class="container">
					<div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<i class="icon-menu-1"></i>
						</button>
                        <!-- End Toggle Nav Link For Mobiles -->
						<a class="navbar-brand" href="{{URL::to('/index')}}">{{HTML::image('images/vella.png', 'WPC Logo')}}</a>
					</div>
						<div class="navbar-collapse collapse">
                        <!-- Stat Search -->
                    	<div class="search-side">
                            <a href="#" class="show-search"><i class="icon-search-1"></i></a>
                            <div class="search-form">
                                <form autocomplete="off" role="search" method="get" class="searchform" action="#">
                                    <input type="text" value="" name="s" id="s" placeholder="Search the site...">
                                </form>
                            </div>
                        </div>
                        <!-- End Search -->
                        <!-- Start Navigation List -->
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a class="active" href="{{URL::to('index')}}">Home</a>
							</li>
							<li>
								<a href="{{URL::to('about')}}">Why US</a>
							</li>
                            <li>
								<a href="{{URL::to('services')}}">Services</a>
							</li>
							<li>
								<a href="{{ URL::to('project') }}">Projects</a>
							</li>
                            <li>
								<a href="{{URL::to('childict')}}">Child CS</a>
							</li>
                            <li>
								<a href="http://www.blog.webplanetcon.com" target="_blank">Blog</a>
							</li>
							<li><a href="{{URL::to('contactus')}}">Contact</a></li>
						</ul>
                        <!-- End Navigation List -->
					</div>
				</div>
			</div>
            <!-- End Header ( Logo & Naviagtion ) -->
          
		</header>
        
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55339003-1', 'auto');
  ga('send', 'pageview');

</script>