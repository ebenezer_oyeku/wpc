
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

    {{HTML::style('css/bootstrap.css', ['media' => 'screen'])}}


    <!-- Revolution Banner CSS -->
    {{HTML::style('css/settings.css', ['media' => 'screen'])}}

    <!-- Vella CSS Styles  -->
    {{HTML::style('css/style.css', ['media' => 'screen'])}}

    <!-- Responsive CSS Styles  -->
    {{HTML::style('css/responsive.css', ['media' => 'screen'])}}

    <!-- Css3 Transitions Styles  -->
    {{HTML::style('css/animate.css', ['media' => 'screen'])}}

    {{HTML::style('css/datepicker.css', ['media' => 'screen'])}}

    <!-- Color CSS Styles  -->
    {{HTML::style('css/colors/blue.css', ['media' => 'screen', 'title' => 'blue'])}}
    {{HTML::style('css/colors/cyan.css', ['media' => 'screen', 'title' => 'cyan'])}}
    {{HTML::style('css/colors/green.css', ['media' => 'screen', 'title' => 'green'])}}
    {{HTML::style('css/colors/jade.css', ['media' => 'screen', 'title' => 'jade'])}}
    {{HTML::style('css/colors/orange.css', ['media' => 'screen', 'title' => 'orange'])}}
    {{HTML::style('css/colors/peach.css', ['media' => 'screen', 'title' => 'peach'])}}
    {{HTML::style('css/colors/pink.css', ['media' => 'screen', 'title' => 'pink'])}}
    {{HTML::style('css/colors/purple.css', ['media' => 'screen', 'title' => 'purple'])}}
    {{HTML::style('css/colors/red.css', ['media' => 'screen', 'title' => 'red'])}}
    {{HTML::style('css/colors/sky-blue.css', ['media' => 'screen', 'title' => 'sky-blue'])}}
    {{HTML::style('css/colors/yellow.css', ['media' => 'screen', 'title' => 'yellow'])}}


    <!-- Fontello Icons CSS Styles  -->
    {{HTML::style('css/fontello.css', ['media' => 'screen'])}}
    {{HTML::style('css/fontello-ie7.css', ['media' => 'screen'])}}

    <!-- Vella JS  -->
    {{HTML::script('js/jquery.min.js')}}
    {{HTML::script('js/jquery.migrate.js')}}
    {{HTML::script('js/modernizrr.js')}}
    {{HTML::script('js/bootstrap.js')}}
    {{HTML::script('js/jquery.fitvids.js')}}
    {{HTML::script('js/owl.carousel.min.js')}}
    {{HTML::script('js/nivo-lightbox.min.js')}}
    {{HTML::script('js/jquery.isotope.min.js')}}
    {{HTML::script('js/jquery.appear.js')}}
    {{HTML::script('js/count-to.js')}}
    {{HTML::script('js/jquery.textillate.js')}}
    {{HTML::script('js/jquery.lettering.js')}}
    {{HTML::script('js/jquery.easypiechart.min.js')}}
    {{HTML::script('js/jquery.nicescroll.min.js')}}
    {{HTML::script('js/jquery.parallax.js')}}
    {{HTML::script('js/mediaelement-and-player.js')}}
    {{HTML::script('js/jquery.themepunch.plugins.min.js')}}
    {{HTML::script('js/jquery.themepunch.revolution.min.js')}}
    {{HTML::script('js/script.js')}}
    {{HTML::script('js/jquery.parallax.js')}}

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="https://apis.google.com/js/platform.js" async defer></script>



    <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

