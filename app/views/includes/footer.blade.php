<?php
$startYear = 2012;
$thisYear = date('Y');
if ($thisYear > $startYear) {
    $thisYear - date('y');
    $copyright = "$startYear&ndash;$thisYear";
} else {
    $copyright = $startYear;
}
?>
<footer>
			<div class="container">
				<div class="row footer-widgets">
                  
                    <!-- Start Contact Widget -->
					<div class="col-md-3">
                        <div class="footer-widget contact-widget">
							<h4>Contact info<span class="head-line"></span></h4>
							<ul>
								<li><span>Lagos Address:</span> F89 Ikota Shopping Complex, VGC, Ajah, Lagos State, Nigeria.</li>
								<li><span>Ibadan Address:</span> 15 Ike-Oluwa Plaza, Elewure Junction, Akala Express Road New Garage, Ibadan, Oyo State, Nigeria</li>
								<li><span>Phone Number:</span> +234 081 6869 4999</li>
                                <li><span>Fax Number:</span> +234 081 6869 4999</li>
								<li><span>Email:</span> info@webplanetcon.com</li>
                                <li><span>Website:</span> www.webplanetcon.com</li>
							</ul>
						</div>
					</div>
                    <!-- End Contact Widget -->
                  
                    <!-- Start Facebook Widget -->  
                    <div class="col-md-3">
						<div class="footer-widget flickr-widget">
							<h4>Facebook Feed<span class="head-line"></span></h4>
                      <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=1428285717423998&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-like-box" data-href="https://www.facebook.com/WebplanetCon" data-width="250" data-height="250" data-colorscheme="dark" data-show-faces="true" data-header="true"></div>
						</div>
					</div>
                    <!-- End Flickr Widget -->
                    
                    <!-- Start Twitter Widget -->
					<div class="col-md-3">
						<div class="footer-widget twitter-widget">
							<h4>Twitter Feed<span class="head-line"></span></h4>
							<a class="twitter-timeline"  href="https://twitter.com/WebplanetC"  data-widget-id="502742942566461440">Tweets by @WebplanetC</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
						</div>
					</div>
                    <!-- End Twitter Widget -->
                    
					<!-- Start Subscribe & Social Links Widget -->
                    <div class="col-md-3">
						<div class="footer-widget mail-subscribe-widget">
							<h4>Get in touch<span class="head-line"></span></h4>
							<p>Join our mailing list to stay up to date and get notices about our new releases!</p>
                            <form class="subscribe" form action="//webplanetcon.us9.list-manage.com/subscribe/post?u=e063d9e43456481f799d43d9a&amp;id=956a2a40e3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								<input type="text"id="mce_Email" placeholder="email address">
								<input type="submit" class="main-button" value="Send" name="subscribe" id="mc-embedded-subscribe" >
							</form>


						</div>
                        <div class="footer-widget social-widget">
							<h4>Follow Us<span class="head-line"></span></h4>
                            <ul class="social-icons">
								<li>
									<a class="facebook" href="https://www.facebook.com/WebplanetCon"><i class="icon-facebook-2"></i></a>
								</li>
								<li>
									<a class="twitter" href="https://twitter.com/WebplanetC"><i class="icon-twitter-2"></i></a>
								</li>
								<li>
									<a class="google" href="https://plus.google.com/u/0/111669135886597931127"><i class="icon-gplus-1"></i></a>
								</li>
                                <li>
									<a class="linkdin" href="https://www.linkedin.com/company/webplanet-consulting-and-services"><i class="icon-linkedin-1"></i></a>
								</li>
                                <li>
									<a class="instgram" href="http://instagram.com/webplanetcon?ref=badge"><i class="icon-instagramm"></i></a>
								</li>
								<li>
									<a class="skype" href="skype:webplanet.consulting?call"><i class="icon-skype-2"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
                <!-- End Subscribe & Social Links Widget -->
              
                <!-- Start Copyright -->
				<div class="copyright-section">
					<div class="row">
						<div class="col-md-6">
							<p><?php echo $copyright; ?> - All Rights Reserved</p>
						</div>
						<div class="col-md-6">
							<ul class="footer-nav">
                            	<li><a href="{{URL::to('careers')}}">Career</a></li>
                                <li><a href="{{URL::to('Faq')}}">FAQ</a></li>
								<li><a href="{{URL::to('privacy')}}">Policy</a></li>
								<li>{{ HTML::link('admin/users/teamlogin', 'Sign In') }}</li>
							</ul>
						</div>						
					</div>
				</div>
                <!-- End Copyright -->
                
			</div>
		</footer>