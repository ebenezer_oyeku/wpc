<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="keywords"  name="Webplanet Consulting" content="Webplanet, WPC, WPC + Code, WPC + Code.org, Webplanet Consulting, Webplanet Consulting and Services, Child ICT, Kids Programming, African young minds, ChangeMaker, Innovator, Webplanet Consulting and Services provide supports, services, management and developments in ICT, Administrative  supports and Graphics Design. We also offer Transcribing services for audio and videos">
<meta name="Webplanet Consulting - Child ICT and Progrmming " content="Creating a better future for the young minds">
<meta name="description" name="Ebenezer Oyeku" content="ChangeMaker!"/>
<meta name="viewport" content="initial-scale-1.0, width=device-width, height=device-height, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

<meta property="place:location:latitude" content="13.062616"/>
<meta property="place:location:longitude" content="80.229508"/>
<meta property="business:contact_data:street_address" content="6 Sanni Aro street, Onireke, Ilaje, Ajah"/>
<meta property="business:contact_data:locality" content="Lagos State"/>
<meta property="business:contact_data:postal_code" content="23401"/>
<meta property="business:contact_data:country_name" content="Nigeria"/>
<meta property="business:contact_data:email" content="info@webplanetcon.com"/>
<meta property="business:contact_data:phone_number" content="+23408168694999"/>
<meta property="business:contact_data:website" content="http://www.webplanetcon.com"/>

<meta name="twitter:card" content="summary"/>  <!-- Card type -->
<meta name="twitter:site" content="WPC"/>
<meta name="twitter:title" content="Webplanet Consulting and Services">
<meta name="twitter:description" content="Innovator of Child ICT and Programming for African Kids"/>
<meta name="twitter:creator" content="WPC"/>
<meta name="twitter:image:src" content="https://webplanetcon.com/images/vella.png"/>
<meta name="twitter:domain" content="webplanetcon.com"/>

<meta itemprop="name" content="Webplanet Consulting and Services"/>
<meta itemprop="description" content="ICT for all"/>
<meta itemprop="image" content="https://www.webplanetcon.com/images/vella.png"/>

<meta property="og:type" content="odammy"/>
<meta property="profile:first_name" content="Ebenezer"/>
<meta property="profile:last_name" content="Oyeku"/>
<meta property="profile:username" content="odammy"/>
<meta property="og:title" content="Webplanet Consulting and Services"/>
<meta property="og:description" content="Creating a better future for the young minds"/>
<meta property="og:image" content="https://www.webplanetcon.com/images/vella.png"/>
<meta property="og:url" content="http://www.webplanetcon.com"/>
<meta property="og:site_name" content="Webplanet Consulting and Services"/>
<meta property="og:see_also" content="http://www.webplanetcon.com"/>
<meta property="fb:admins" content="https://www.facebook.com/WebplanetCon"/>
<meta name="google-site-verification" content="goRlooHoQkqwARXyzX1G33akPYXYSWubXGG02gMFX6M" />