@extends('layouts.admin')

@section('style')
    {{HTML::script('js/jquery-1.8.0.min.js')}}
    {{HTML::style('css/datepicker.css')}}
    @endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Employee
                <small>Control Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Create Employee Form</a></li>
            </ol>
        </section>

        <section class="content">
            @if(Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <!-- USERS LIST -->
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Latest Employees</h3>
                            <div class="box-tools pull-right">
                                <span class="label label-danger">Employee</span>
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body no-padding">
                            <ul class="users-list clearfix">
                                @foreach($users as $user)
                                    <li>
                                        {{HTML::image($user->image, 'User Image', array('width'=>'50'))}}
                                        <a class="users-list-name" href="#">{{$user->pname}}</a>
                                        <span class="users-list-date">{{$user->created_at}}</span>
                                        {{Form::open(array('url' =>'admin/users/destroy', 'class'=>'form-inline')) }}
                                        {{Form::hidden('id', $user->id) }}
                                        {{Form::submit('Delete Client', array('class' => 'btn btn-block btn-danger btn-xs')) }}
                                        {{Form::close() }}
                                    </li>
                                @endforeach
                            </ul><!-- /.users-list -->
                            {{$users->links()}}
                        </div><!-- /.box-body -->
                        <div class="box-footer text-center">
                            <a href="javascript::" class="uppercase">View All Users</a>
                        </div><!-- /.box-footer -->
                    </div><!--/.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- Input addon -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create New Employee</h3>
                        </div>
                        {{Form::open(array('url'=>'admin/users/create', 'files' => true))}}
                        <div class="box-body">
                            <div class="input-group">
                                <span class="input-group-addon">First Name</span>
                                {{Form::text('firstname', null, array('class' => 'form-control', 'placeholder' => 'Employee Firstname'))}}
                            </div>
                            @if ($errors->has('firstname')) <p class="help-block">{{ $errors->first('firstname') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Last Name</span>
                                {{Form::text('lastname', null, array('class' => 'form-control', 'placeholder' => 'Employee Lastname'))}}
                            </div>
                            @if ($errors->has('lastname')) <p class="help-block">{{ $errors->first('lastname') }}</p> @endif
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <div id="imagePreview" class="img-thumbnail"></div>
                                        <div>
                                            <input id="uploadFile" type="file" name="image" class="img" />
                                            <label for="img">Post Image Upload</label>
                                            <p>"Include Snapshot of Your Project Here"</p>
                                            @if ($errors->has('image')) <p class="help-block">{{ $errors->first('image') }}</p> @endif
                                        </div>
                                    </div>
                                </div><!-- /.col-lg-6 -->

                            </div><!-- /.row -->
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Username</span>
                                {{Form::text('Username', null, array('class' => 'form-control', 'placeholder' => 'Desired Username'))}}
                            </div>
                            @if ($errors->has('Username')) <p class="help-block">{{ $errors->first('Username') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Desired Password</span>
                                {{Form::text('password', null, array('class' => 'form-control', 'placeholder' => '*************'))}}
                            </div>
                            @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Confirm Password</span>
                                {{Form::text('password_confirmation', null, array('class' => 'form-control', 'placeholder' => '*************'))}}
                            </div>
                            @if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                {{Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Your Email Address'))}}
                            </div>
                            @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Contact Address</span>
                                {{Form::text('Address', null, array('class' => 'form-control', 'placeholder' => 'Employee Contact Address'))}}
                            </div>
                            @if ($errors->has('Address')) <p class="help-block">{{ $errors->first('Address') }}</p> @endif
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                {{Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Your Contact Number'))}}
                            </div>
                            @if ($errors->has('phone')) <p class="help-block">{{ $errors->first('phone') }}</p> @endif
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                        <span class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </span>
                                        {{Form::text('dob', null, array('class' => 'form-control', 'id' => 'example1','placeholder' => 'Employee Date of Birth'))}}
                                    </div><!-- /input-group -->
                                    @if ($errors->has('dob')) <p class="help-block">{{ $errors->first('dob') }}</p> @endif
                                </div><!-- /.col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">Employee An Admin</span>
                                        {{Form::select('Administrator', array('0'=>'No', '1'=>'Yes' ), 'default', array('class' => 'form-control'))}}
                                    </div><!-- /input-group -->
                                    @if ($errors->has('Administrator')) <p class="help-block">{{ $errors->first('Administrator') }}</p> @endif
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->
                            <br>
                            {{Form::submit('Create Employee', array('class'=>'btn btn-block btn-success btn-flat'))}}
                            {{Form::close() }}
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!--/.col (left) -->
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('script')
    <style>
        #imagePreview {
            width: 180px;
            height: 180px;
            background-position: center center;
            background-size: cover;
            -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
            display: inline-block;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $("#uploadFile").on("change", function()
            {
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file

                    reader.onloadend = function(){ // set image data as background of div
                        $("#imagePreview").css("background-image", "url("+this.result+")");
                    }
                }
            });
        });
    </script>
    {{HTML::script('js/jquery-1.9.1.min.js')}}
    {{HTML::script('js/bootstrap-datepicker.js')}}

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            $('#example1').datepicker({
                format: "mm/dd/yyyy"
            });

        });
    </script>
    @endsection
