
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=1,initial-scale=1,user-scalable=1" />
    <title>Staff Login</title>

    <link href="http://fonts.googleapis.com/css?family=Lato:100italic,100,300italic,300,400italic,400,700italic,700,900italic,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../../css/loginstyles.css" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="../../images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../images/favicon.ico" type="image/x-icon">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<section class="container login-form">
    <section>
        @if(Session::has('message'))
            <div class="alert alert-success">{{ Session::get('message') }}</div>
        @endif

        {{ Form::open(array('url' => 'admin/users/teamlogin', 'class'=>'form-signin')) }}
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
            {{HTML::image('/images/logo.png', 'Login Logo', array('class'=>'img-responsive'))}}
        {{--<img src="../images/logo.png" alt="" class="img-responsive" />--}}

        {{ Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
        <hr/>
            <input type="password" name="password"  class="form-control" placeholder="*************">
        {{--{{ Form::password('password', null, array('name'=> 'password', 'class' => 'form-control',  'placeholder' => '*********')) }}--}}
        <hr/>
        {{ Form::submit('SignIn', array('class'=>'btn btn-large btn-primary btn-block'))}}
        {{Form::close()}}
        <hr>
        <label>
            <input type="checkbox" value="remember-me" id="remember"> Remember me
        </label>

        {{--<a href="{{URL::to('remind')}}" class="pull-right help">Forgot Password? </a><span class="clearfix"></span>--}}
        {{ Form::close() }}
        {{--<a href="{{URL::to('signup')}}" class="text-center account-creation">Create an account </a>--}}
    </section>
</section>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>

<script>
    <!--
    $( document ).ready(function() {
        $('#tooltip').tooltip();
    });
    -->
</script>

</body>
</html>